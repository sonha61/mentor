<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        //Load CI In
        parent::__construct();

        //Preparing common data that use in all methods
        $data_head = array(
            'base_url' => $this->config->base_url(),
            'og_url' => current_url(),
            'style' => array(),
            'scripts' => array(),
            'meta_keywords' => 'heykorean,이민,유학,구인,구직,미국,부동산,렌트,민박,비자,job,fulltime,parttime,사고팔기,업소록,생활정보,여행,미주,한인,커뮤니티,동호회,클럽,뉴스,KSANY,newyork,newjersey,음식점,이사,상담,변호사,서블렛,홈스테이,무빙세일,매매,영주권,트랜스퍼,쿠폰,쇼핑,미술관,뮤지컬,박물관',
            'meta_title' => 'HeyKorean 전 세계 NO.1 한인 커뮤니티 – 이민 유학 구인구직 미국 뉴욕 뉴저지 부동산 렌트 비자 민박취업 인턴 사고팔기 중고 업소록 생활정보 여행',
            'meta_description' => '한인들을 위한 최대 한인 포탈 사이트입니다. heykorean 서비스는 mobile app으로도 만나볼 수 있습니다.',
            'og_image' => 'http://s3.amazonaws.com/heykorean.image.public/common/SNS/2016/02/20/2OvOvtYwEK0.png',
        );

        $this->load->helper("url");
        $this->load->model('Qa_Model');
        $this->load->model('Sharing_Model');
        $this->load->model('Common_Model');
        $this->load->model('Category_Model');
        $this->load->model('Reply_Sharing_Model');
        $this->load->model('Reply_Qa_Model');
        $this->load->model('User_Model');
        $this->load->helper('breadcrumb_helper');
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->data_head = $data_head;
    }

    public function getMenuInfo($data) {
        $data["us_life"]  = $this->Qa_Model->get_menu('01', 50);
        $data["advice"]  = $this->Qa_Model->get_menu('02', 50);
        $data["local_info"]  = $this->Qa_Model->get_menu('03', 50);
        $data["live_debate"]  = $this->Qa_Model->get_menu('04', 50);
        $data["sharing"]  = $this->Qa_Model->get_menu('05', 50);
        return $data;
    }

    public function myMentor1($memberId = null)
    {
        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        $data['title'] = 'My Mentor';
        $data = array_merge($data, $this->getMenuInfo($data));
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('user/my_mentor',$data);
        $this->load->view('layout/footer', $this->data_head);
    }

    function jsonp_decode($jsonp, $assoc = false) { // PHP 5.3 adds depth as third parameter to json_decode
    if($jsonp[0] !== '[' && $jsonp[0] !== '{') { // we have JSONP
       $jsonp = substr($jsonp, strpos($jsonp, '('));
    }
    return json_decode(trim($jsonp,'();'), $assoc);
    }

    public function myMentor($memberId = null)
    {
        // $this->data_head['style'] = array();
        // $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        // $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        if($_COOKIE['heykorean'] != '') {
            // header('content-type: application/json; charset=utf-8');
            // header('content-type: text/html; charset=utf-8');
            // $url = 'http://ap.heykorean.com/v2/heykorean/member/memberinfo?callback=member';
            // $user_info = file_get_contents($url);
            // var_dump($user_info);die;
            // var_dump($this->jsonp_decode($user_info));die;

            // $json_url = "http://ap.heykorean.com/v2/heykorean/member/memberinfo?callback=member";
            // $json = file_get_contents($json_url);
            // var_dump($json);die;
            // $json=str_replace('},]',"}]",$json);
            // $data = json_decode($json);
            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            // die();

            $user_info = $this->User_Model->get_user_by_id($memberId);
            $data['title'] = 'My Mentor';
            $data['user_info'] = $user_info;
            $data['my_answers'] = $this->Reply_Qa_Model->get_reply_qa_by_member_id($memberId, 5);
            $data['my_questions'] = $this->Qa_Model->get_qa_by_member_id($memberId, 5);
            $data = array_merge($data, $this->getMenuInfo($data));
            $this->load->view('layout/head', $this->data_head);
            $this->load->view('user/my_mentor1',$data);
        } else {
            header("Location: http://heykorean.com/help/login_proc.asp?ret=".$this->data_head['base_url'].'user/myMentor');
        }
    }

    public function goodPeople() {
        $this->data_haed['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "/assets/js/jquery.bxslider.min.js");
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('user/good_people');
        $this->load->view('layout/footer', $this->data_head);
    }
}
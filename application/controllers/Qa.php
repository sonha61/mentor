<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qa extends CI_Controller
{
    public function __construct()
    {
        //Load CI In
        parent::__construct();

        //Preparing common data that use in all methods
        $data_head = array(
            'base_url' => $this->config->base_url(),
            'og_url' => current_url(),
            'style' => array(),
            'scripts' => array(),
            'meta_keywords' => 'heykorean,이민,유학,구인,구직,미국,부동산,렌트,민박,비자,job,fulltime,parttime,사고팔기,업소록,생활정보,여행,미주,한인,커뮤니티,동호회,클럽,뉴스,KSANY,newyork,newjersey,음식점,이사,상담,변호사,서블렛,홈스테이,무빙세일,매매,영주권,트랜스퍼,쿠폰,쇼핑,미술관,뮤지컬,박물관',
            'meta_title' => 'HeyKorean 전 세계 NO.1 한인 커뮤니티 – 이민 유학 구인구직 미국 뉴욕 뉴저지 부동산 렌트 비자 민박취업 인턴 사고팔기 중고 업소록 생활정보 여행',
            'meta_description' => '한인들을 위한 최대 한인 포탈 사이트입니다. heykorean 서비스는 mobile app으로도 만나볼 수 있습니다.',
            'og_image' => 'http://s3.amazonaws.com/heykorean.image.public/common/SNS/2016/02/20/2OvOvtYwEK0.png',
        );
        $this->load->helper("url");
        $this->load->model('Qa_Model');
        $this->load->model('Sharing_Model');
        $this->load->model("Cipagination_Model");
        $this->load->model('Common_Model');
        $this->load->model('Category_Model');
        $this->load->model('Reply_Qa_Model');
        $this->load->model('User_Model');
        $this->load->helper('breadcrumb_helper');
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->data_head = $data_head;
    }

    public function clean()
    {
        $this->cache->memcached->clean();
        echo 'Cache Cleaned';die;
    }

    public function test() {
        $data['title'] = 'New Version';
        $data['base_url'] = $this->config->base_url();
        $this->load->view('qa/qa_test',$data);
    }

    public function index()
    {
        $data['all_search_field'] = array(
            '' => '--Option--',
            'fTitle' => 'Title',
            'fContent' => 'Content',
            'fContent' => 'Tag',
            'fSeq' => 'ID'
        );
        $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
        $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;

        $data["fCatSeq"] = ($this->input->get("fCatSeq"))? $this->input->get("fCatSeq") : null;
        $data["fTab"] = ($this->input->get("fTab"))? $this->input->get("fTab") : null;
        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        $this->load->helper('form');
        $config = $this->Common_Model->getPaging(25);
        $config["base_url"] = base_url() . "/qa/index";

        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        }
        $config["total_rows"] = $this->Qa_Model->record_count($data["fCatSeq"], $type_search, $keyword);
        $data["newly_posted"] = $this->Qa_Model->record_count_last_month();
        $data["answered"] = $this->Reply_Qa_Model->record_count_last_month();
        $data['all_category'] = $this->Category_Model->get_all_category();
        $data["best_recommended_qa"] = $this->Qa_Model->best_recommended_qa(5);
        $data["best_sharing"]  = $this->Sharing_Model->best_sharing(2);

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if (empty($page)) $page = 1;
        $offset = ($page - 1) * $config['per_page'];
        $data['page'] = (int)$this->uri->segment(3);
        $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
        $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

        $data = array_merge($data, $this->getMenuInfo($data));
        // $data["result"] = $this->Qa_Model->get_voting_qa($config["per_page"], $offset, $data["fCatSeq"], $type_search, $keyword);
        $data["result"] = $this->Qa_Model->fetch_qa(10, $offset, $data["fCatSeq"], $data["fTab"], $type_search, $keyword);

        // var_dump($data["result"] );die;
        $data["best_mentor"]  = $this->User_Model->get_best_mentor(5);
        $data["best_leader"]  = $this->User_Model->get_best_leader(5);
        $data["best_qa"]  = $this->Qa_Model->best_qa(10);
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);

        $data["links"] = $this->pagination->create_links();
        $data['base_url'] = $this->config->base_url();
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('qa/qa_test', $data);
//        $this->load->view('layout/footer', $this->data_head);
    }

    public function index1()
    {
        $data['all_search_field'] = array(
            '' => '--Option--',
            'fTitle' => 'Title',
            'fContent' => 'Content',
            'fContent' => 'Tag',
            'fSeq' => 'ID'
        );
        $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
        $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;

        $data["fCatSeq"] = ($this->input->get("fCatSeq"))? $this->input->get("fCatSeq") : null;
        $data["fTab"] = ($this->input->get("fTab"))? $this->input->get("fTab") : null;
        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        $this->load->helper('form');
        $config = $this->Common_Model->getPaging(25);
        $config["base_url"] = base_url() . "/qa/index";

        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        }
        $config["total_rows"] = $this->Qa_Model->record_count($data["fCatSeq"], $type_search, $keyword);
        $data["newly_posted"] = $this->Qa_Model->record_count_last_month();
        $data["answered"] = $this->Reply_Qa_Model->record_count_last_month();
        $data['all_category'] = $this->Category_Model->get_all_category();
        $data["best_recommended_qa"] = $this->Qa_Model->best_recommended_qa(5);
        $data["best_sharing"]  = $this->Sharing_Model->best_sharing(2);

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if (empty($page)) $page = 1;
        $offset = ($page - 1) * $config['per_page'];
        $data['page'] = (int)$this->uri->segment(3);
        $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
        $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

        $data = array_merge($data, $this->getMenuInfo($data));

        if(isset($data['fTab'])) {
            if($data["fTab"] == 'voting') {
                $data["opening"] = null;
                $data["voting"] = $this->Qa_Model->get_voting_qa($config["per_page"], $offset, $data["fCatSeq"], $type_search, $keyword);
                $data["resolved"] = null;
            } elseif ($data["fTab"] == 'resolved') {
                $data["opening"] = null;
                $data["voting"] = null;
                $data["resolved"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $data["fCatSeq"], $data["fTab"], $type_search, $keyword);
            } elseif($data["fTab"] == 'open') {
                $data["opening"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $data["fCatSeq"], $data["fTab"], $type_search, $keyword);
                $data["voting"] = null;
                $data["resolved"] = null;
            }
        } else {
            $data["opening"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $data["fCatSeq"], $data["fTab"], $type_search, $keyword);
            $data["voting"] = null;
            $data["resolved"] = null;
        }

        $data["best_mentor"]  = $this->User_Model->get_best_mentor(5);
        $data["best_leader"]  = $this->User_Model->get_best_leader(5);
        $data["best_qa"]  = $this->Qa_Model->best_qa(10);
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);

        $data["links"] = $this->pagination->create_links();
        $data['base_url'] = $this->config->base_url();
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('qa/qa_default', $data);
        $this->load->view('layout/footer', $this->data_head);
    }

    public function getMenuInfo($data) {
        $data["us_life"]  = $this->Qa_Model->get_menu('01', 50);
        $data["advice"]  = $this->Qa_Model->get_menu('02', 50);
        $data["local_info"]  = $this->Qa_Model->get_menu('03', 50);
        $data["live_debate"]  = $this->Qa_Model->get_menu('04', 50);
        $data["sharing"]  = $this->Qa_Model->get_menu('05', 50);
        return $data;
    }

    public function ajax()
    {
        $data["fCatSeq"] = ($this->input->get("fCatSeq"))? $this->input->get("fCatSeq") : null;
        //Custom style and scripts

        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array();
        $this->load->helper('form');
        $config = $this->Common_Model->getPaging(25,3);
        $config["base_url"] = base_url() . "/qa/ajax";

        $config["total_rows"] = $this->Qa_Model->record_count();
        $data['all_category'] = $this->Category_Model->get_all_category();

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if (empty($page)) $page = 1;
        $offset = ($page - 1) * $config['per_page'];
        $data['page'] = (int)$this->uri->segment(3);
        $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
        $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

        $data["results"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $data["fCatSeq"], null, null);
        $data["results2"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $data["fCatSeq"], null, null);
        $data["results3"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $data["fCatSeq"], null, null);

        $data["best_mentor"]  = $this->User_Model->get_best_mentor(5);
        $data["best_leader"]  = $this->User_Model->get_best_leader(5);
        $data["best_qa"]  = $this->Qa_Model->best_qa(10);
        $data["links"] = $this->pagination->create_links();
        $data['base_url'] = $this->config->base_url();

        $this->load->view('layout/head', $this->data_head);
        if($this->input->post('ajax')) {
            $this->load->view('qa/qa_paging',$data);
        }
        else {
            $this->load->view('qa/qa_default', $data);
        }
        $this->load->view('layout/footer', $this->data_head);
    }

    public function category($catId)
    {
        $data['all_search_field'] = array(
            '' => '--Option--',
            'fTitle' => 'Title',
            'fContent' => 'Content',
            'fContent' => 'Tag',
            'fSeq' => 'ID'
        );
        $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
        $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
        //Custom style and scripts
        $this->data_head['style'] = array();
        // $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        // $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        $this->load->helper('form');
        $config = $this->Common_Model->getPaging(25,4);
        $config["base_url"] = base_url() . "/qa/category/$catId";
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        }

        $config["total_rows"] = $this->Qa_Model->record_count($catId);
        $data['all_category'] = $this->Category_Model->get_all_category();
        $data["fTab"] = ($this->input->get("fTab"))? $this->input->get("fTab") : null;
        $data["catId"] = $catId;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        if (empty($page)) $page = 1;
        $offset = ($page - 1) * $config['per_page'];
        $data['page'] = (int)$this->uri->segment(4);
        $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
        $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

        $data = array_merge($data, $this->getMenuInfo($data));
        $data["navi_category"] =  $this->Qa_Model->get_menu($catId, 50);
        $data["cat_name"] =  $this->Category_Model->get_category_name_by_code($catId);
        $data["newly_posted"] = $this->Qa_Model->record_count_last_month($catId);
        $data["answered"] = $this->Reply_Qa_Model->record_count_last_month($catId);
        $data["best_recommended_qa"] = $this->Qa_Model->best_recommended_qa(5);
        $data["us_life_best_reply"]  = $this->Qa_Model->us_life_best_reply(5);

        if(isset($data['fTab'])) {
            if($data["fTab"] == 'voting') {
                $data["opening"] = null;
                $data["voting"] = $this->Qa_Model->get_voting_qa($config["per_page"], $offset,$data["catId"], $data["fTab"], $type_search, $keyword);
                $data["resolved"] = null;
            } elseif ($data["fTab"] == 'resolved') {
                $data["opening"] = null;
                $data["voting"] = null;
                $data["resolved"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $catId, $data["fTab"], $type_search, $keyword);
            } elseif($data["fTab"] == 'open') {
                $data["opening"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $catId, $data["fTab"], $type_search, $keyword);
                $data["voting"] = null;
                $data["resolved"] = null;
            }
        } else {
            $data["fTab"] == 'open';
            $data["opening"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $catId, $data["fTab"], $type_search, $keyword);
            $data["voting"] = null;
            $data["resolved"] = null;
        }
        $data["best_mentor"]  = $this->User_Model->get_best_mentor(5);
        $data["best_leader"]  = $this->User_Model->get_best_leader(5);
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);
        $data["best_qa"]  = $this->Qa_Model->best_qa(10);
        $data["links"] = $this->pagination->create_links();

       // var_dump($this->data_head);die;
        $data['base_url'] = $this->config->base_url();
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('qa/qa_category1', $data);
        $this->load->view('layout/footer', $this->data_head);
    }

    public function category1($catId)
    {
        $data['all_search_field'] = array(
            '' => '--Option--',
            'fTitle' => 'Title',
            'fContent' => 'Content',
            'fContent' => 'Tag',
            'fSeq' => 'ID'
        );
        $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
        $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;
        //Custom style and scripts
        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        $this->load->helper('form');
        $config = $this->Common_Model->getPaging(25,4);
        $config["base_url"] = base_url() . "/qa/category/$catId";
        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        }

        $config["total_rows"] = $this->Qa_Model->record_count($catId);
        $data['all_category'] = $this->Category_Model->get_all_category();
        $data["fTab"] = ($this->input->get("fTab"))? $this->input->get("fTab") : null;
        $data["catId"] = $catId;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        if (empty($page)) $page = 1;
        $offset = ($page - 1) * $config['per_page'];
        $data['page'] = (int)$this->uri->segment(4);
        $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
        $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

        $data = array_merge($data, $this->getMenuInfo($data));
        $data["navi_category"] =  $this->Qa_Model->get_menu($catId, 50);
        $data["cat_name"] =  $this->Category_Model->get_category_name_by_code($catId);
        $data["newly_posted"] = $this->Qa_Model->record_count_last_month($catId);
        $data["answered"] = $this->Reply_Qa_Model->record_count_last_month($catId);
        $data["best_recommended_qa"] = $this->Qa_Model->best_recommended_qa(5);
        $data["us_life_best_reply"]  = $this->Qa_Model->us_life_best_reply(3);

        if(isset($data['fTab'])) {
            if($data["fTab"] == 'voting') {
                $data["opening"] = null;
                $data["voting"] = $this->Qa_Model->get_voting_qa($config["per_page"], $offset,$data["catId"], $data["fTab"], $type_search, $keyword);
                $data["resolved"] = null;
            } elseif ($data["fTab"] == 'resolved') {
                $data["opening"] = null;
                $data["voting"] = null;
                $data["resolved"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $catId, $data["fTab"], $type_search, $keyword);
            } elseif($data["fTab"] == 'open') {
                $data["opening"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $catId, $data["fTab"], $type_search, $keyword);
                $data["voting"] = null;
                $data["resolved"] = null;
            }
        } else {
            $data["fTab"] == 'open';
            $data["opening"] = $this->Qa_Model->fetch_qa($config["per_page"], $offset, $catId, $data["fTab"], $type_search, $keyword);
            $data["voting"] = null;
            $data["resolved"] = null;
        }

        $data["best_mentor"]  = $this->User_Model->get_best_mentor(5);
        $data["best_leader"]  = $this->User_Model->get_best_leader(5);
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);
        $data["best_qa"]  = $this->Qa_Model->best_qa(10);
        $data["links"] = $this->pagination->create_links();

//        var_dump($data["links"] );die;
        $data['base_url'] = $this->config->base_url();
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('qa/qa_category', $data);
        $this->load->view('layout/footer', $this->data_head);
    }

    public function view1($id)
    {
        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js");
        $data['editor'] =   $this->Common_Model->tinymce('fContent');
        $data['qa_info'] = $this->Qa_Model->get_qa($id);

        $this->data_head['meta_keywords'] = $data['qa_info']->fTitle;
        $this->data_head['meta_title'] = $data['qa_info']->fTitle;

        $this->Qa_Model->qa_hit_increase($id);
        $data['all_reply'] = $this->Reply_Qa_Model->get_reply_qa($id);
        $data['count_reply'] = count($data['all_reply']);
        $pattern = '~(http.*\.)(jpe?g|png|[tg]iff?|svg)~i';
        $m = preg_match_all($pattern, $data['qa_info']->fContent,$matches);
        $this->data_head['og_image'] = isset($matches[0][0]) ? $matches[0][0] : $this->data_head['og_image'];
        //remove image from content
        $this->data_head['meta_description'] = strip_tags(preg_replace("/<img[^>]+\>/i", "image", $data['qa_info']->fContent));

        $this->load->view('layout/head', $this->data_head);


        $data["most_answered"]  = $this->Qa_Model->best_qa(5);
        $data["most_recommended_sharing"] = $this->Sharing_Model->most_recommended_sharing(5);
        $data["today_best_advice"] = $this->Qa_Model->get_today_best_advice(3);
        $data["most_popular_local"]  = $this->Qa_Model->most_popular_local(5, 0, null, null);
        $data["unanswered_questions"]  = $this->Qa_Model->get_unanswered_questions( $data['qa_info']->fCatSeq, 3);
        $data["resolved_questions"]  = $this->Qa_Model->get_resolved_questions( $data['qa_info']->fCatSeq, 3);
        $data['base_url'] = $this->config->base_url();
        $data = array_merge($data, $this->getMenuInfo($data));
        if(isset($_POST['submit'])) {
            $this->form_validation->set_rules('fContent', 'Name', 'required');
            $this->form_validation->set_rules('fTitle', 'Title', 'required');
//            $this->form_validation->set_rules('choosearea', 'Location', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('qa/qa_view', $data);
                $this->load->view('layout/footer', $this->data_head);
            } else {
                $this->Reply_Qa_Model->add_reply($id);
                redirect('/qa/view/'.$id, 'refresh');
            }
        }

        if(isset($_POST['replyComment'])) {
            $this->Reply_Qa_Model->add_reply_comment($id);
            redirect('/qa/view/'.$id, 'refresh');
        }
        $this->load->view('qa/qa_view', $data);
        $this->load->view('layout/footer', $this->data_head);
    }

    public function view($id)
    {

        $this->data_head['style'] = array();
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js");
        $data['editor'] =   $this->Common_Model->tinymce('fContent');
        $data['qa_info'] = $this->Qa_Model->get_qa($id);

        $this->data_head['meta_keywords'] = $data['qa_info']->fTitle;
        $this->data_head['meta_title'] = $data['qa_info']->fTitle;

        $this->data_head['scripts'] = array(base_url() . "assets/js/mentor_new.js");

        $this->Qa_Model->qa_hit_increase($id);
        $data['all_reply'] = $this->Reply_Qa_Model->get_reply_qa($id);
        $data['count_reply'] = count($data['all_reply']);
        $pattern = '~(http.*\.)(jpe?g|png|[tg]iff?|svg)~i';
        $m = preg_match_all($pattern, $data['qa_info']->fContent,$matches);
        $this->data_head['og_image'] = isset($matches[0][0]) ? $matches[0][0] : $this->data_head['og_image'];
        //remove image from content
        $this->data_head['meta_description'] = strip_tags(preg_replace("/<img[^>]+\>/i", "image", $data['qa_info']->fContent));

        $this->load->view('layout/head', $this->data_head);
        $data["most_answered"]  = $this->Qa_Model->best_qa(5);
        $data["most_recommended_sharing"] = $this->Sharing_Model->most_recommended_sharing(5);
        $data["today_best_advice"] = $this->Qa_Model->get_today_best_advice(3);
        $data["most_popular_local"]  = $this->Qa_Model->most_popular_local(5, 0, null, null);
        $data["unanswered_questions"]  = $this->Qa_Model->get_unanswered_questions( $data['qa_info']->fCatSeq, 3);
        $data["resolved_questions"]  = $this->Qa_Model->get_resolved_questions( $data['qa_info']->fCatSeq, 3);
        $data['base_url'] = $this->config->base_url();
        $data = array_merge($data, $this->getMenuInfo($data));
        if(isset($_POST['submit'])) {
            $this->form_validation->set_rules('fContent', 'Name', 'required');
            // $this->form_validation->set_rules('fTitle', 'Title', 'required');
//            $this->form_validation->set_rules('choosearea', 'Location', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('qa/qa_view2', $data);
            } else {
                $this->Reply_Qa_Model->add_reply($id);
                redirect('/qa/view/'.$id, 'refresh');
            }
        }

        if(isset($_POST['replyComment'])) {
            $this->Reply_Qa_Model->add_reply_comment($id);
            redirect('/qa/view/'.$id, 'refresh');
        }
        $this->load->view('qa/qa_view2', $data);
        // $this->load->view('layout/footer', $this->data_head);
    }

    public function getQaByCat() {
        return true;
    }

    public function addVote() {
        $id = $this->input->post('id');
        $action = $this->input->post('action');
        $type = $this->input->post('type');
        if($type == 'qa') {
            if($action == 'report') {
                $result = $this->Qa_Model->add_qa_report($id);
            } else {
                $result = $this->Qa_Model->add_qa_vote($id);
            }
            echo json_encode($result);
            exit();
        } else {
            if($action == 'report') {
                $result = $this->Sharing_Model->add_sharing_report($id);
            } else {
                $result = $this->Sharing_Model->add_sharing_vote($id);
            }
            echo json_encode($result);
            exit();
        }

    }

    public function addVoteReply() {
        $qa_id = $this->input->post('qa_id');
        $reply_id = $this->input->post('reply_id');
        $action = $this->input->post('action');
        $type = $this->input->post('type');
        if($type == 'qa') {
            if($action == 'report') {
                $result = $this->Qa_Model->add_qa_reply_report($reply_id, $qa_id);
            } else {
                $result = $this->Qa_Model->add_qa_reply_vote($reply_id, $qa_id);
            }
            echo json_encode($result);
            exit();
        } else {
            if($action == 'report') {
                $result = $this->Sharing_Model->add_sharing_reply_report($reply_id, $qa_id);
            } else {
                $result = $this->Sharing_Model->add_sharing_reply_vote($reply_id, $qa_id);
            }
            echo json_encode($result);
            exit();
        }

    }

    public function getSubCagegory() {
        $code = $this->input->post('category_code');
        $data =  $this->Category_Model->get_sub_category_by_code($code);
        echo json_encode($data);
        exit();
    }

    public function write1() {
        if($_COOKIE['heykorean'] != '') {
            $this->data_head['style'] = array();
            $this->data_head['scripts'] = array(base_url() . "assets/js/qa_write.js");
            $data['all_category'] = $this->Category_Model->get_all_category();
            $data['editor'] =   $this->Common_Model->tinymce('fWrite', '886');
            $data = array_merge($data, $this->getMenuInfo($data));

            $data['title'] = 'Create a QA';
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fTitle', 'Name', 'required');
            $this->form_validation->set_rules('fWrite', 'Content', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('layout/head', $this->data_head);
                $this->load->view('qa/qa_write', $data);
                $this->load->view('layout/footer', $this->data_head);
            } else {
                $insert_id = $this->Qa_Model->set_qa();
                $this->session->set_flashdata('qa_add', 'Add QA Successful');
                redirect('/qa/view/'.$insert_id , 'refresh');
            }
        } else {
            header("Location: http://heykorean.com/help/login_proc.asp?ret=".$this->data_head['base_url'].'/qa/write');
        }
        }

    public function write() {
        // if($_COOKIE['heykorean'] != '') {
            $this->data_head['style'] = array();
            $this->data_head['scripts'] = array(base_url() . "assets/js/mentor_new.js");
            $data['all_category'] = $this->Category_Model->get_all_category();
            $data['editor'] =   $this->Common_Model->tinymce('fWrite', '886');
            $data = array_merge($data, $this->getMenuInfo($data));

            $data['title'] = 'Create a QA';
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fTitle', 'Name', 'required');
            $this->form_validation->set_rules('fWrite', 'Content', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('layout/head', $this->data_head);
                $this->load->view('qa/qa_write2', $data);
                // $this->load->view('layout/footer', $this->data_head);
            } else {
                $insert_id = $this->Qa_Model->set_qa();
                $this->session->set_flashdata('qa_add', 'Add QA Successful');
                redirect('/qa/view/'.$insert_id , 'refresh');
            }
        // } else {
        //     header("Location: http://heykorean.com/help/login_proc.asp?ret=".$this->data_head['base_url'].'/qa/write');
        // }
    }

    public function getCity() {
        $country = $this->input->post('country');
        if(!$country) {
            return false;
        }
        $cities = array(
            0 => array('Kathmandu','Bhaktapur','Patan','Pokhara','Lumbini'),
            1 => array('Kathmandu','Bhaktapur','Patan','Pokhara','Lumbini'),
            2 => array('Delhi','Mumbai','Kolkata','Bangalore','Hyderabad','Pune','Chennai','Jaipur','Goa'),
            3 => array('Beijing','Chengdu','Lhasa','Macau','Shanghai')
        );

        $currentCities = $cities[$country];
        $result = "<option value=''>Please Select</option>";
        foreach ($currentCities as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        echo $result;
    }

    public function best() {
        $data['all_search_field'] = array(
            '' => '--Option--',
            'fTitle' => 'Title',
            'fContent' => 'Content',
            'fContent' => 'Tag',
            'fSeq' => 'ID'
        );
        $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
        $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;

        $data['title'] = 'Best List';
        $data['base_url'] = $this->config->base_url();
        $this->data_head['style'] = array();
//        $this->data_head['scripts'] = array(base_url() . "assets/js/news_slider.js");
        $this->data_head['scripts'] = array(base_url() . "assets/js/sharing_list.js");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js");
        $this->load->view('layout/head', $this->data_head);
        $config = $this->Common_Model->getPaging(25,3);
        $config["base_url"] = base_url() . "qa/best";

        $config["total_rows"] = $this->Qa_Model->record_count(null,null, null);

        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        }

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["results"]  = $this->Qa_Model->most_popular_local($config["per_page"], $page, $type_search, $keyword);
        $data = array_merge($data, $this->getMenuInfo($data));
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);
        $data["best_recommended_qa"] = $this->Qa_Model->best_recommended_qa(5);
        $data["us_life_best_reply"]  = $this->Qa_Model->us_life_best_reply(3);
        $data["links"] = $this->pagination->create_links();

        if($this->input->post('ajax')) {
            $this->load->view('qa_ciajaxpagination',$data);
        }
        else {
            $this->load->view('qa_cipagination',$data);
        }
        $this->load->view('layout/footer', $this->data_head);
    }
}
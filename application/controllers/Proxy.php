<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proxy extends CI_Controller {
    public function index()
    {
        $url = $_GET['url'];
        echo file_get_contents($url);   // get html content using CURL
    }

    /**
     * @todo : Get AreaGroup from areaidx
     * @author : Simon
     */
    public function GetAreaGroup() {
        header('content-type: application/json; charset=utf-8');
        if(empty($_COOKIE['areaidx']) || $_COOKIE['areaidx'] == null) {
            setcookie('areaidx', 4672, time() + 86400 * 365);
        }
        $areaidx = isset($_COOKIE['areaidx']) ? $_COOKIE['areaidx'] : 4672;
        $url = 'http://ap.heykorean.com/v3/Heykorean/Common/GetAreaGroup?callback=areagroup&areaidx='.$areaidx;
        $data =file_get_contents($url);
        echo $data;
    }

    /**
     * @todo : Get Area from areaidx
     * @author : Simon
     */
    public function getArea() {
        header('content-type: application/json; charset=utf-8');
        $url = 'http://ap.heykorean.com/v3/Heykorean/Common/GetArea?callback=area&areaidx='.$_COOKIE['areaidx'];
        $data = file_get_contents($url);
        if($data && strlen($data) > 12 ) {
            echo $data;
        } else {
            echo 'area(null)';
        }
    }

    /**
     * @todo : Get Member Inf
     * @author : Simon
     */
    public function getMemberInfo() {
        header('content-type: application/json; charset=utf-8');
        $url = 'http://ap.heykorean.com/v2/heykorean/member/memberinfo?callback=member';
        $data = file_get_contents($url);
        echo $data;
    }
}

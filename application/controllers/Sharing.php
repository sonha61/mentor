<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sharing extends CI_Controller
{
    public function __construct()
    {
        //Load CI In
        parent::__construct();

        //Preparing common data that use in all methods
        $data_head = array(
            'base_url' => $this->config->base_url(),
            'og_url' => current_url(),
            'style' => array(),
            'scripts' => array(),
            'meta_keywords' => 'heykorean,이민,유학,구인,구직,미국,부동산,렌트,민박,비자,job,fulltime,parttime,사고팔기,업소록,생활정보,여행,미주,한인,커뮤니티,동호회,클럽,뉴스,KSANY,newyork,newjersey,음식점,이사,상담,변호사,서블렛,홈스테이,무빙세일,매매,영주권,트랜스퍼,쿠폰,쇼핑,미술관,뮤지컬,박물관',
            'meta_title' => 'HeyKorean 전 세계 NO.1 한인 커뮤니티 – 이민 유학 구인구직 미국 뉴욕 뉴저지 부동산 렌트 비자 민박취업 인턴 사고팔기 중고 업소록 생활정보 여행',
            'meta_description' => '한인들을 위한 최대 한인 포탈 사이트입니다. heykorean 서비스는 mobile app으로도 만나볼 수 있습니다.',
//            'og_image' => 'http://www.mobipicker.com/wp-content/uploads/2016/06/ATT-IPHONE.png',
            'og_image' => 'http://s3.amazonaws.com/heykorean.image.public/common/SNS/2016/02/20/2OvOvtYwEK0.png',
        );
        $this->load->helper("url");
        $this->load->model('Qa_Model');
        $this->load->model('Sharing_Model');
        $this->load->model('Common_Model');
        $this->load->model('Category_Model');
        $this->load->model('Reply_Sharing_Model');
        $this->load->model('User_Model');
        $this->load->helper('breadcrumb_helper');
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->data_head = $data_head;
    }

    public function getMenuInfo($data) {
        $data["us_life"]  = $this->Qa_Model->get_menu('01', 50);
        $data["advice"]  = $this->Qa_Model->get_menu('02', 50);
        $data["local_info"]  = $this->Qa_Model->get_menu('03', 50);
        $data["live_debate"]  = $this->Qa_Model->get_menu('04', 50);
        $data["sharing"]  = $this->Qa_Model->get_menu('05', 50);
        return $data;
    }

    public function index()
    {
        $data['all_search_field'] = array(
            '' => '--Option--',
            'fTitle' => 'Title',
            'fContent' => 'Content',
            'fContent' => 'Tag',
            'fSeq' => 'ID'
        );
        $keyword = ($this->input->post("search_name"))? $this->input->post("search_name") : null;
        $type_search = ($this->input->post("type_search"))? $this->input->post("type_search") : null;

        $data["fCatSeq"] = ($this->input->get("fCatSeq"))? $this->input->get("fCatSeq") : null;
        $data["fTab"] = ($this->input->get("fTab"))? $this->input->get("fTab") : null;
        // $this->data_head['style'] = array(base_url() . "assets/css/mentor.css");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/news_slider.js");
        $this->load->helper('form');
        $config = $this->Common_Model->getPaging(25);
        $config["base_url"] = base_url() . "/sharing/index";

        if (count($_GET) > 0) {
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
            $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
        }
        $config["total_rows"] = $this->Sharing_Model->record_count(null, $type_search, $keyword);
        $data['all_category'] = $this->Category_Model->get_all_category();
        $data['sharing_category'] = $this->Category_Model->get_sharing_category('05');

        foreach($data['sharing_category'] as $key => $value) {
            $new_data[$key]['code'] = $value->code;
            $new_data[$key]['en_name'] = $value->en_name;
            $new_data[$key]['name'] = $value->name;
            $new_data[$key]['sub_category'] = $this->Category_Model->get_sharing_category($value->code);
        }
        $data['category'] = $new_data;


        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if (empty($page)) $page = 1;
        $offset = ($page - 1) * $config['per_page'];
        $data['page'] = (int)$this->uri->segment(3);
        $order_seg = $this->uri->segment(5, "asc"); // if the 5th segment not present,it will return asc. default value.
        $data["order"] = ($order_seg == "asc") ? 'desc' : 'asc';

        $data = array_merge($data, $this->getMenuInfo($data));

        $data["result"] = $this->Sharing_Model->fetch_sharing(10, $offset, $data["fCatSeq"], $data["fTab"], $type_search, $keyword);

        // $data["most_answered"]  = $this->Sharing_Model->best_sharing(5);
        $data["best_recommended_qa"] = $this->Qa_Model->best_recommended_qa(5);
        $data["us_life_best_reply"]  = $this->Qa_Model->us_life_best_reply(5);
        // $data["best_sharing"]  = $this->Sharing_Model->best_sharing(3);
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);
        $data["links"] = $this->pagination->create_links();
        $data['base_url'] = $this->config->base_url();
        $this->load->view('layout/head', $this->data_head);
        $this->load->view('sharing/sharing_list1', $data);
        // $this->load->view('layout/footer', $this->data_head);
    }

     public function view($id)
    {
        $this->data_haed['style'] = array("url1", "url2");
        $this->data_head['scripts'] = array(base_url() . "assets/js/mentor_new.js");
        // $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/sharing_view.js");
        $this->load->view('layout/head', $this->data_head);
        // $this->load->view('layout/partial_header', $this->data_head);
        $data['editor'] =   $this->Common_Model->tinymce('fContent');
        $data['sharing_info'] = $this->Sharing_Model->get_sharing($id);
        $this->Sharing_Model->sharing_hit_increase($id);
        $data['all_reply'] = $this->Reply_Sharing_Model->get_reply_sharing($id);
        $data["best_sharing"]  = $this->Sharing_Model->best_sharing(3);
        // echo "<pre>";
        // var_dump($data['all_reply']);die;
        $data['catId'] = $data['sharing_info']->fCatSeq;

        $data["most_answered"]  = $this->Qa_Model->best_qa(5);
        $data["best_recommended_qa"]  = $this->Qa_Model->best_recommended_qa(5);
        $data["most_recommended_sharing"] = $this->Sharing_Model->most_recommended_sharing(5);
        $data["today_best_advice"] = $this->Qa_Model->get_today_best_advice(3);
        $data["most_popular_local"]  = $this->Qa_Model->most_popular_local(5, 0, null, null);
        $data["most_popular_keywords"]  = $this->Category_Model->most_popular_keywords(10);
        // echo "<pre>";
        // var_dump($data["most_popular_keywords"]);die;
        $data['base_url'] = $this->config->base_url();
        $data = array_merge($data, $this->getMenuInfo($data));
        if(isset($_POST['submit'])) {
            $this->form_validation->set_rules('fContent', 'Content', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('sharing/sharing_view', $data);
                $this->load->view('layout/footer', $this->data_head);
            } else {
                $this->Reply_Sharing_Model->add_reply($id);
                redirect('/sharing/view/'.$id, 'refresh');
            }
        }
        
        if(isset($_POST['replyComment'])) {
            $this->Reply_Sharing_Model->add_reply_comment($id);
            redirect('/sharing/view/'.$id, 'refresh');
        }

        $this->load->view('sharing/sharing_view2', $data);
        // $this->load->view('layout/partial_footer', $this->data_head);
        // $this->load->view('layout/partial_js', $this->data_head);
    }

    public function view1($id)
    {
        $this->data_haed['style'] = array("url1", "url2");
        $this->data_head['scripts'] = array(base_url() . "assets/js/market.js", base_url() . "assets/js/sharing_view.js");
        $this->load->view('layout/head', $this->data_head);
        $data['editor'] =   $this->Common_Model->tinymce('fContent');
        $data['sharing_info'] = $this->Sharing_Model->get_sharing($id);
        $this->Sharing_Model->sharing_hit_increase($id);
        $data['all_reply'] = $this->Reply_Sharing_Model->get_reply_sharing($id);
        $data["best_sharing"]  = $this->Sharing_Model->best_sharing(3);

        $data['catId'] = $data['sharing_info']->fCatSeq;

        $data["most_answered"]  = $this->Qa_Model->best_qa(5);
        $data["most_recommended_sharing"] = $this->Sharing_Model->most_recommended_sharing(5);
        $data["today_best_advice"] = $this->Qa_Model->get_today_best_advice(3);
        $data["most_popular_local"]  = $this->Qa_Model->most_popular_local(5, 0, null, null);
        $data['base_url'] = $this->config->base_url();
        $data = array_merge($data, $this->getMenuInfo($data));
        if(isset($_POST['submit'])) {
            $this->form_validation->set_rules('fContent', 'Content', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('sharing/sharing_view', $data);
                $this->load->view('layout/footer', $this->data_head);
            } else {
                $this->Reply_Sharing_Model->add_reply($id);
                redirect('/sharing/view/'.$id, 'refresh');
            }
        }
        $this->load->view('sharing/sharing_view', $data);
        $this->load->view('layout/footer', $this->data_head);
    }

    public function write1() {
        if($_COOKIE['heykorean'] != '') {
            $this->data_head['style'] = array("url1", "url2");
            $this->data_head['scripts'] = array(base_url() . "assets/js/qa_write.js");
            $data['all_category'] = $this->Category_Model->get_all_category();
            $data['editor'] =   $this->Common_Model->tinymce('fWrite', '886');
            $data = array_merge($data, $this->getMenuInfo($data));

            $data['title'] = 'Create a Sharing';
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fTitle', 'Name', 'required');
            $this->form_validation->set_rules('fWrite', 'Content', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('layout/head', $this->data_head);
                $this->load->view('sharing/sharing_write', $data);
                $this->load->view('layout/footer', $this->data_head);
            } else {
                $insert_id = $this->Sharing_Model->set_sharing();
                $this->session->set_flashdata('qa_add', 'Add Sharing Successful');
                redirect('/sharing/view/'.$insert_id , 'refresh');
            }
        } else {
            header("Location: http://heykorean.com/help/login_proc.asp?ret=".$this->data_head['base_url'].'/sharing/write');
        }
    }

    public function write() {
        // if($_COOKIE['heykorean'] != '') {
            $this->data_head['style'] = array("url1", "url2");
            $this->data_head['scripts'] = array(base_url() . "assets/js/qa_write.js");
            $data['all_category'] = $this->Category_Model->get_all_category();
            $data['editor'] =   $this->Common_Model->tinymce('fWrite', '886');
            $data = array_merge($data, $this->getMenuInfo($data));

            $data['title'] = 'Create a Sharing';
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fTitle', 'Name', 'required');
            $this->form_validation->set_rules('fWrite', 'Content', 'required');

            if ($this->form_validation->run() === FALSE) {
                $this->load->view('layout/head', $this->data_head);
                $this->load->view('sharing/sharing_write2', $data);
                // $this->load->view('layout/footer', $this->data_head);
            } else {
                $insert_id = $this->Sharing_Model->set_sharing();
                $this->session->set_flashdata('qa_add', 'Add Sharing Successful');
                redirect('/sharing/view/'.$insert_id , 'refresh');
            }
        // } else {
        //     header("Location: http://heykorean.com/help/login_proc.asp?ret=".$this->data_head['base_url'].'/sharing/write');
        // }
    }
}
<?php

/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 5/11/2016
 * Time: 10:41 AM
 */
class User_Model extends CI_Model
{
    
    function __construct()
    {
        $this->load->driver('cache');
        return parent::__construct();
    }

    public function record_count($type = null, $keyword = null)
    {
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('tbl_mentor_user_basic');
            return $this->db->count_all_results();
        } else {
            return $this->db->count_all("tbl_mentor_user_basic");
        }
    }


    function get_best_mentor($limit)
    {
        $cache = $this->cache->memcached->get('get_best_mentor');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('strUserID, n4CurrentlyLevelRank,n4CurrentlyLevelPoint');
            $this->db->from('tbl_mentor_user_basic');
            $this->db->order_by('n4CurrentlyLevelPoint', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_best_mentor', $data, 10000);
                return $data;
            }
            return false;
        }

    }

    function get_best_leader($limit)
    {
        $cache = $this->cache->memcached->get('get_best_leader');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('strUserID, n4CurrentlyLevelRank,n4CurrentlyLevelPoint');
            $this->db->from('tbl_mentor_user_basic');
            $this->db->order_by('n4CurrentlyLevelPoint', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_best_leader', $data, 10000);
                return $data;
            }
            return false;
        }
    }

     function get_user_by_id($id)
    {
        $cache = $this->cache->memcached->get('get_user_by_id' . $id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('tbl_mentor_user_basic');
            $this->db->where('strUserID', $id);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_user_by_id' . $id, $data, 10000);
                return $data;
            }
        }
    }
}

?>

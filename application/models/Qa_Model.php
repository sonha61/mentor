<?php

class Qa_Model extends CI_Model
{
    function __construct()
    {
        $this->load->driver('cache');
        return parent::__construct();
    }

    public function record_count_last_month($catId = null)
    {
        $cache = $this->cache->memcached->get('record_count_last_month' . $catId);
        if ($cache) {
            return $cache;
        } else {
            $sql = "SELECT COUNT(fSeq) FROM `htb_mentor_board_qa` WHERE `fFirstRegDT` >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)";
            if($catId) {
                $sql .= " AND fCatSeq LIKE ('$catId%')";
            }
            $query = $this->db->query($sql);
            $data = $query->row_array();
            $this->cache->memcached->save('record_count_last_month' , $data["COUNT(fSeq)"], 10000);
            return $data["COUNT(fSeq)"];
        }
    }

    public function record_count($catId = null, $type = null, $keyword = null)
    {
        $cache = $this->cache->memcached->get('qa_record_count' . $catId . $type . $keyword);
        if ($cache) {
            return $cache;
        } else {
            $sql = "SELECT COUNT(fSeq) FROM htb_mentor_board_qa WHERE 1 = 1 ";
            if (!empty($type) && !empty($keyword)) {
                $sql .= " AND $type LIKE '%$keyword%'";
            }
            if (!empty($catId)) {
//                $sql .= " AND fCatSeq LIKE '$catId%'";
                $sql .= " AND fCatSeq = '$catId'";
            }
//            var_dump($sql);die;
            $query = $this->db->query($sql);
            $data = $query->row_array();
            $this->cache->memcached->save('qa_record_count' . $catId . $type . $keyword, $data["COUNT(fSeq)"], 10000);
            return $data["COUNT(fSeq)"];
        }
    }

    public function set_qa()
    {
        $data = array(
            'fTitle' => $this->input->post('fTitle'),
            'fAreaCode' => $this->input->post('fTitle'),
            'fAreaDepth' => $this->input->post('fTitle'),
            'fIP' => $this->input->post('fTitle'),
            'fHit' => 0,
            'fRecommend' => 0,
            'fNotRecommend' => 0,
            'fUserID' => isset($_COOKIE['USERID']) ? $_COOKIE['USERID'] : 'simon',
            'fUser_Key' => isset($_COOKIE['USERKEY']) ? $_COOKIE['USERKEY'] : 'simon',
            'fReplyCount' => 0,
            'fHideID' => 1,
            // 'AREA_IDX' => $this->input->post('choosearea'),
            'AREA_IDX' => 11111,
            'fHideIDMy' => 1,
            'fCatSeq' => $this->input->post('categoryName'),
            'fMB' => $this->input->post('fMB'),
            'fContent' => $this->input->post('fWrite'),
        );

        $this->db->insert('htb_mentor_board_qa', $data);

        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function get_voting_qa($limit, $start, $fCatSeq = null)
    {
        $cache = $this->cache->memcached->get('get_voting_qa'.$limit.$start.$fCatSeq);
        if ($cache) {
            return $cache;
        } else {

            //fMB, fSeq, fTitle, fCatSeq,fRegDT, fAreaCode
            $this->db->select('reply.fBSeq,qa.fMB, qa.fSeq, qa.fTitle as title, qa.fCatSeq, qa.fFirstRegDT, qa.fRegDT, qa.fAreaCode, COUNT(reply.fBSeq) as total, reply.fTitle');
            $this->db->from('htb_mentor_reply_qa as reply');
            $this->db->join('htb_mentor_board_qa as qa', 'reply.fBSeq = qa.fSeq');
            if (!empty($fCatSeq)) {
                $this->db->like('qa.fCatSeq', $fCatSeq, 'after');
            }
            $this->db->group_by("fBSeq");

//            $this->db->where('qa.fFirstRegDT >', 'DATE_SUB(CURDATE(), INTERVAL 1 MONTH)');
//            $sql = "SELECT COUNT(fSeq) FROM `htb_mentor_board_qa` WHERE `fFirstRegDT` >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)";

            $this->db->order_by('qa.fFirstRegDT', 'desc');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_voting_qa'.$limit.$start.$fCatSeq, $data, 10000);
                return $data;
            }
            return false;
        }
    }


    public function fetch_qa($limit, $start, $fCatSeq = null, $fTab = null, $type = null, $keyword = null)
    {
        if (!empty($type) && !empty($keyword)) {
            $cache = $this->cache->memcached->get('fetch_qa' . $limit . $start . $fCatSeq . $fTab . $type . $keyword);
        } else {
            $cache = $this->cache->memcached->get('fetch_qa' . $limit . $start . $fCatSeq . $fTab);
        }
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fMB, fSeq, fTitle, fCatSeq,fRegDT, fAreaCode, fContent');
            $this->db->limit($limit, $start);
            if (!empty($type) && !empty($keyword)) {
                $this->db->like($type, $keyword);
            }
            if (!empty($fCatSeq)) {
                $this->db->like('fCatSeq', $fCatSeq, 'after');
            }

            $this->db->order_by("fRegDT","desc");
            $query = $this->db->get("htb_mentor_board_qa");

            if ($query->num_rows() > 0) {
                foreach ($query->result() as $key => $row) {
                    $data[$key] = $row;
                    $this->load->model('Category_Model', 'Category_Model');
//                    $data[$key]->fCatSeq = $this->Category_Model->get_category_name_by_code($row->fCatSeq);
                }

                if (!empty($type) && !empty($keyword)) {
                    $this->cache->memcached->save('fetch_qa' . $limit . $start . $fCatSeq . $fTab . $type . $keyword, $data, 10000);
                } else {
                    $this->cache->memcached->save('fetch_qa' . $limit . $start . $fCatSeq . $fTab, $data, 10000);
                }

                $this->cache->memcached->save('fetch_qa' . $limit . $start . $fCatSeq . $fTab . $type . $keyword, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    function get_qa_by_member_id($memberId, $limit = 5)
    {
        $cache = $this->cache->memcached->get('get_qa_by_member_id'.$memberId);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_board_qa');
            $this->db->where('fUserID', $memberId);
            $this->db->limit($limit);
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save('get_qa_by_member_id'.$memberId , $data, 10000);
            return $data;
        }
    }

    public function get_menu($parent_code, $limit)
    {
        $cache = $this->cache->memcached->get('get_us_life' . $parent_code);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('code, name, en_name');
            $this->db->limit($limit);
            $this->db->where('depth', 0);
            $this->db->like('code', $parent_code, 'after');
            $query = $this->db->get("htb_mentor_category");
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $key => $row) {
                    $data[$key] = $row;
                }
                $this->cache->memcached->save('get_us_life' . $parent_code, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    function get_qa($id)
    {
        $cache = $this->cache->memcached->get('get_qa' . $id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_board_qa');
            $this->db->where('fSeq', $id);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_qa' . $id, $data, 10000);
                return $data;
            }
        }
    }

    // function get_hit_by_id($id)
    // {
    //     $cache = $this->cache->memcached->get('get_hit_by_qa' . $id);
    //     if ($cache) {
    //         return $cache;
    //     } else {
    //         $this->db->select('fHit');
    //         $this->db->from('htb_mentor_board_qa');
    //         $this->db->where('fSeq', $id);
    //         $query = $this->db->get();
    //         $data = $query->first_row()->fHit;
    //         if ($data) {
    //             $this->cache->memcached->save('get_hit_by_qa' . $id, $data, 10000);
    //             return $data;
    //         }
    //     }
    // }

    function get_reply_qa_by_id($id)
    {
        $cache = $this->cache->memcached->get('get_reply_qa_by_id' . $id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fSeq', $id);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_reply_qa_by_id' . $id, $data, 10000);
                return $data;
            }
        }
    }

    function most_answered($limit = 10)
    {
        $cache = $this->cache->memcached->get('get_most_answered');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('tbl_mentor_user_basic');
            $this->db->order_by('n4CurrentlyLevelPoint', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_most_answered', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    /**
     * Indexed
     * Created by Ha Anh Son
     * @param $fSeq
     * @return mixed
     */
    function get_total_reply($fSeq = null)
    {
        $cache = $this->cache->memcached->get('get_total_reply' . $fSeq);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fBSeq');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fBSeq', $fSeq);
            $data = $this->db->count_all_results();
            $this->cache->memcached->save('get_total_reply' . $fSeq, $data, 10000);
            return $data;
        }
        return false;
    }

    function get_total_reply_comment($fSeq = null)
    {
        $cache = $this->cache->memcached->get('get_total_reply_comment' . $fSeq);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fPSeq');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fPSeq', $fSeq);
            $this->db->where('fDepth is NOT NULL', NULL, FALSE);
            $data = $this->db->count_all_results();
            $this->cache->memcached->save('get_total_reply_comment' . $fSeq, $data, 10000);
            return $data;
        }
        return false;
    }

    public function best_qa($limit = 10)
    {
        $cache = $this->cache->memcached->get('get_best_qa'.$limit);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('reply.fBSeq, qa.fSeq, qa.fTitle as title, qa.fCatSeq, qa.fFirstRegDT, COUNT(reply.fBSeq) as total, reply.fTitle');
            $this->db->from('htb_mentor_reply_qa as reply');
            $this->db->join('htb_mentor_board_qa as qa', 'reply.fBSeq = qa.fSeq');
            $this->db->group_by("fBSeq");
            $this->db->order_by('total', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_best_qa'.$limit, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function get_today_best_advice($limit = 10)
    {
        $cache = $this->cache->memcached->get('get_today_best_advice');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fSeq,fTitle, fRecommend');
            $this->db->from('htb_mentor_board_qa');
            $this->db->order_by('fRecommend', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_today_best_advice', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function most_popular_local($limit = 10, $start = 0, $type= null, $keyword = null)
    {
        $cache = $this->cache->memcached->get('most_popular_local'.$limit.$start.$type.$keyword);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('reply.fBSeq,COUNT(reply.fBSeq) as total, qa.fTitle, qa.fUserID, qa.fFirstRegDT, qa.fContent, qa.fCatSeq, qa.fSeq');
            $this->db->from('htb_mentor_reply_qa as reply');
            $this->db->join('htb_mentor_board_qa as qa', 'reply.fBSeq = qa.fSeq');
            $this->db->where('fAreaCode', 13310);
            if (!empty($type) && !empty($keyword)) {
                $this->db->like('qa.'.$type, $keyword);
            }
            $this->db->group_by("fBSeq");
            $this->db->order_by('total', 'desc');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
//            $data = $query->result();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $key => $row) {
                    $data[$key] = $row;
                    $this->load->model('Category_Model', 'Category_Model');
                    $data[$key]->fCatSeq = $this->Category_Model->get_category_name_by_code($row->fCatSeq);
                }
            }
            if ($data) {
                $this->cache->memcached->save('most_popular_local'.$limit.$start.$type.$keyword, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function get_resolved_questions($fCatSeq = null, $limit = null)
    {
        $cache = $this->cache->memcached->get('resolved_questions' . $fCatSeq);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('reply.fBSeq,COUNT(reply.fBSeq) as total, qa.fTitle,  qa.fFirstRegDT');
            $this->db->from('htb_mentor_reply_qa as reply');
            $this->db->join('htb_mentor_board_qa as qa', 'reply.fBSeq = qa.fSeq');
            $this->db->where('fCatSeq', $fCatSeq);
            $this->db->group_by("fBSeq");
            $this->db->order_by('qa.fFirstRegDT', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('resolved_questions' . $fCatSeq, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function get_unanswered_questions($fCatSeq = null, $limit = 10)
    {
        $cache = $this->cache->memcached->get('get_unanswered_questions' . $fCatSeq . $limit);
        if ($cache) {
            return $cache;
        } else {
            $sql = "SELECT fSeq, fTitle,fFirstRegDT FROM `htb_mentor_board_qa` WHERE fCatSeq = '' AND (fSeq NOT IN (SELECT DISTINCT fBSeq FROM `htb_mentor_reply_qa`)) ORDER BY fFirstRegDT DESC LIMIT $limit";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $data[] = $row;
                }
            }
            if ($data) {
                $this->cache->memcached->save('get_unanswered_questions' . $fCatSeq . $limit, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function qa_hit_increase($id = null)
    {
        $qa_info = $this->get_qa($id);
        if ($qa_info) {
            $this->cache->memcached->delete('get_qa' . $id);
            $data = array(
                'fHit' => $qa_info->fHit + 1,
            );
            $this->db->where('fSeq', $id);
            return $this->db->update('htb_mentor_board_qa', $data);
        }
        return false;
    }


    public function us_life_best_reply($limit = 10)
    {
        $cache = $this->cache->memcached->get('us_life_best_reply');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('reply.fBSeq, qa.fSeq, qa.fTitle as title, qa.fCatSeq, qa.fFirstRegDT, COUNT(reply.fBSeq) as total, reply.fTitle');
            $this->db->from('htb_mentor_reply_qa as reply');
            $this->db->join('htb_mentor_board_qa as qa', 'reply.fBSeq = qa.fSeq');
            $this->db->group_by("fBSeq");
            $this->db->order_by('total', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('us_life_best_reply', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function best_recommended_qa($limit = 10)
    {
        $cache = $this->cache->memcached->get('best_recommended_qa');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fSeq,fTitle, fRecommend');
            $this->db->from('htb_mentor_board_qa');
            $this->db->order_by('fRecommend', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('best_recommended_qa', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function add_qa_report($id = null)
    {
        $qa_info = $this->get_qa($id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($id, $_COOKIE['USERID'],2);
        if ($qa_info) {
            if(!$vote) {
                $this->cache->memcached->delete('get_qa' . $id);
                $data_vote = array(
                    'fType' => 2,
                    'fBSeq' => $id,
                    'fNotRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fNotRecommend' => $qa_info->fNotRecommend + 1,
                );
                $this->db->where('fSeq', $id);
                return $this->db->update('htb_mentor_board_qa', $data);
            } else {
                return 'duplicated';
            }
        }
        return false;
    }

    public function add_qa_reply_report($reply_id = null, $qa_id = null)
    {
        $qa_info = $this->get_reply_qa_by_id($reply_id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($reply_id, $_COOKIE['USERID'],2);
        if ($qa_info) {
            if(!$vote) {
                $this->cache->memcached->delete('get_reply_qa_by_id' . $reply_id);
                $this->cache->memcached->delete('get_reply_qa' . $qa_id);
                $data_vote = array(
                    'fType' => 2,
                    'fBSeq' => $reply_id,
                    'fNotRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fNotRecommend' => $qa_info->fNotRecommend + 1,
                );
                $this->db->where('fSeq', $reply_id);
                return $this->db->update('htb_mentor_reply_qa', $data);
            } else {
                return 'duplicated';
            }

        }
        return false;
    }

    public function add_qa_vote($id = null)
    {
        $qa_info = $this->get_qa($id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($id, $_COOKIE['USERID'],1);
        if ($qa_info) {
            $this->cache->memcached->delete('get_qa' . $id);
            if(!$vote) {
                $data_vote = array(
                    'fType' => 1,
                    'fBSeq' => $id,
                    'fRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fRecommend' => $qa_info->fRecommend + 1,
                );
                $this->db->where('fSeq', $id);
                return $this->db->update('htb_mentor_board_qa', $data);
            } else {
                return 'duplicated';
            }
        }
        return false;
    }

    public function add_qa_reply_vote($reply_id = null, $qa_id = null)
    {
        $qa_info = $this->get_reply_qa_by_id($reply_id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($reply_id, $_COOKIE['USERID'],1);
        if ($qa_info) {
            if(!$vote) {
                $this->cache->memcached->delete('get_reply_qa_by_id' . $reply_id);
                $this->cache->memcached->delete('get_reply_qa' . $qa_id);
                $data_vote = array(
                    'fType' => 1,
                    'fBSeq' => $reply_id,
                    'fRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fRecommend' => $qa_info->fRecommend + 1,
                );
                $this->db->where('fSeq', $reply_id);
                return $this->db->update('htb_mentor_reply_qa', $data);
            } else {
                return 'duplicated';
            }

        }
        return false;
    }

    public function count_qa_by_user_id($id) {
        $cache = $this->cache->memcached->get('count_qa_by_user_id' . $id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('COUNT(*)');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fUserID', $id);
            $data = $this->db->count_all_results();
            $this->cache->memcached->save('count_qa_by_user_id' . $id, $data, 10000);
            return $data;
        }
        return false;
    }
}

?>

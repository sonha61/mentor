<?php
/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 30/4/2016
 * Time: 2:01 PM
 */
class Common_Model extends CI_Model {

    /**
     * @todo : get status
     * Created by Ha Anh Son
     * @param null $status
     * @return mixed|string
     */
    public function getStatus($status = null)
    {
        $base = array(
            STATUS_ACTIVE => 'Y',
            STATUS_UNACTIVE => 'N'
        );
        return !empty($base[$status]) ? $base[$status] : 'Undefine';
    }

    /**
     * paging
     * Created by Ha Anh Son
     * @param int $perPage
     * @return array
     */
    public function getPaging($perPage = 10, $uri_segment = 3){
        $config = array();
        $config["per_page"] = $perPage;
        $config['use_page_numbers'] = TRUE;
        $config["uri_segment"] = $uri_segment;
       // $config['next_link'] = 'Next';
       // $config['prev_link'] = 'Previous';
        // $config['full_tag_open'] = '<div class="paging-wrap clearfix"><ul>';
        // $config['full_tag_close'] = '</ul></div>';
        // $config['first_link'] = false;
        // $config['last_link'] = false;
        // // $config['prev_link'] = '&laquo';
        // // $config['next_link'] = '&raquo';
        // $config['cur_tag_open'] = '<li class="paging"><a class="on" href="#">';
        // $config['cur_tag_close'] = '</a><li>';


$config['full_tag_open'] = '<div class="pagination"><ul>';
$config['full_tag_close'] = '</ul></div><!--pagination-->';
$config['first_link'] = '&laquo; First';
$config['first_tag_open'] = '<li class="prev page">';
$config['first_tag_close'] = '</li>';
$config['last_link'] = 'Last &raquo;';
$config['last_tag_open'] = '<li class="next page">';
$config['last_tag_close'] = '</li>';
$config['next_link'] = 'Next &rarr;';
$config['next_tag_open'] = '<li class="next page">';
$config['next_tag_close'] = '</li>';
$config['prev_link'] = '&larr; Previous';
$config['prev_tag_open'] = '<li class="prev page">';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a href="">';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li class="page">';
$config['num_tag_close'] = '</li>';
// $config['display_pages'] = FALSE;
// 
$config['anchor_class'] = 'follow_link';
// 

        return $config;
    }

    /**
     * @todo : get Color
     * Created by Ha Anh Son
     * @param $number
     * @return string
     */
    public function getWaterColor($number) {
        if($number < 50) {
            $color = 'water-blue.png';
        } elseif ($number > 50 && $number < 100) {
            $color = 'water-gray.png';
        } else {
            $color = 'water-orange.png';
        }
        return $color;
    }

    /**
     * @Todo: Get breadcrumbs
     * Created by Ha Anh Son
     * @param $cat_id
     * @param int $level
     * @param string $sep
     */
    public function breadcrumbs($cat_id, $level=0, $sep='&gt;') //&raquo;
    {
        $query = $this->db->query("SELECT * FROM `htb_mentor_category` WHERE `code` = ".$this->db->escape($cat_id)."");
        $row = $query->row();

        if(isset($row)) {
            if ($row->parent_code != '') {
                $this->breadcrumbs($row->parent_code, $level+1, $sep);
            }
            echo ($row->parent_code == '') ? '<a href="'.$row->code.'">'.$row->en_name.'</a>' : '<a href="'.$row->parent_code.'">'."$sep ".$row->en_name.'</a>';
        } else {
            echo '';
        }
    }

    function createPath($id, $except = null) {

        $query = $this->db->query("SELECT * FROM `htb_mentor_category` WHERE `code` = ".$this->db->escape($id)."");
        $row = $query->row();
        if($row->parent_code == null) {
            $name = $row->en_name;
            return "<li><a href='".base_url()."'>Home</a></li><li><a href='".base_url()."qa?/category/$id'>".$name."</a></li>";
        } else {
            $name = $row->en_name;
            if(!empty($except) && $except == $name)
                return $this->createPath($row['parent_id'], $except)." ".$name;
        }
        return $this->createPath($row->parent_code, $except). "<li><a href='".base_url()."qa/category/$id'>".$name."</a></li>";
    }


    public function tinymce($name, $width= '817')
    {
        $this->load->helper('url');
        $editor = '<!-- place in header of your html document -->
            <script type="text/javascript" src="'.  base_url().'assets/tinymce/js/tinymce/tinymce.min.js"></script>
            <script>
            tinymce.init({
                selector: "textarea#'.$name.'",
                entity_encoding : "raw",
                theme: "modern",
                width: '.$width.',
                height: 200,
                relative_urls : false,
                remove_script_host: false,
                plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                     "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                     "save table contextmenu directionality emoticons template paste textcolor"
               ],
               content_css: "css/content.css",
               style_formats: [
                    {title: "Bold text", inline: "b"},
                    {title: "Red text", inline: "span", styles: {color: "#ff0000"}},
                    {title: "Red header", block: "h1", styles: {color: "#ff0000"}},
                    {title: "Example 1", inline: "span", classes: "example1"},
                    {title: "Example 2", inline: "span", classes: "example2"},
                    {title: "Table styles"},
                    {title: "Table row 1", selector: "tr", classes: "tablerow1"}
                ]
             }); 
            </script>'
        ;
        return $editor;
    }

    function truncate($string,$length=100,$append="&hellip;") {
        $short_string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length));

        return $short_string.'...';
    }

//    function truncate($string,$length=100,$append="&hellip;") {
//        $string = trim($string);
//
//        if(strlen($string) > $length) {
//            $string = wordwrap($string, $length);
//            $string = explode("\n", $string, 2);
//            $string = $string[0] . $append;
//        }
//
//        return $string;
//    }
}
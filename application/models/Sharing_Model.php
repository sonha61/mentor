<?php

class Sharing_Model extends CI_Model
{
    function __construct()
    {
        $this->load->driver('cache');
        return parent::__construct();
    }

    public function record_count($type = null, $keyword = null)
    {
        $cache = $this->cache->memcached->get('sharing_record_count');
        if ($cache) {
            return $cache;
        } else {
            $query = $this->db->query("SELECT COUNT(fSeq) FROM htb_mentor_board_sharing");
            $data = $query->row_array();
            $this->cache->memcached->save('sharing_record_count', $data["COUNT(fSeq)"], 10000);
            return $data["COUNT(fSeq)"];
        }
    }

    function get_total_reply_comment($fSeq = null)
    {
        $cache = $this->cache->memcached->get('get_total_reply_comment_sharing' . $fSeq);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fPSeq');
            $this->db->from('htb_mentor_reply_sharing');
            $this->db->where('fPSeq', $fSeq);
            $data = $this->db->count_all_results();
            $this->cache->memcached->save('get_total_reply_comment_sharing' . $fSeq, $data, 10000);
            return $data;
        }
        return false;
    }

//    public function fetch_sharing($limit, $start, $fCatSeq = null, $fTab = null, $type = null, $keyword = null)
//    {
//        $cache = $this->cache->memcached->get('fetch_sharing' . $limit . $start);
//        if ($cache) {
//            return $cache;
//        } else {
//            $this->db->select('fMB, fSeq, fTitle, fCatSeq,fRegDT, fUserID');
//            $this->db->limit($limit, $start);
//            if (!empty($type) && !empty($keyword)) {
//                $this->db->like($type, $keyword);
//            }
//            $this->db->order_by("$sortfield", "$order");
//            $query = $this->db->get("htb_mentor_board_sharing");
//
//            if ($query->num_rows() > 0) {
//                foreach ($query->result() as $key => $row) {
//                    $data[$key] = $row;
//                    $this->load->model('Category_Model', 'Category_Model');
//                    $data[$key]->fCatSeq = $this->Category_Model->get_category_name_by_code($row->fCatSeq);
//                }
//                $this->cache->memcached->save('fetch_sharing' . $limit . $start, $data, 10000);
//                return $data;
//            }
//            return false;
//        }
//    }

    public function fetch_sharing($limit, $start, $fCatSeq = null, $fTab = null, $type = null, $keyword = null)
    {
        if (!empty($type) && !empty($keyword)) {
            $cache = $this->cache->memcached->get('fetch_sharing' . $limit . $start . $fCatSeq . $fTab . $type . $keyword);
        } else {
            $cache = $this->cache->memcached->get('fetch_sharing' . $limit . $start . $fCatSeq . $fTab);
        }
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fMB, fSeq, fTitle, fCatSeq, fRegDT, fUserID, fContent');
            $this->db->limit($limit, $start);
            if (!empty($type) && !empty($keyword)) {
                $this->db->like($type, $keyword);
            }
            if (!empty($fCatSeq)) {
                $this->db->like('fCatSeq', $fCatSeq, 'after');
            }
            $this->db->order_by("fRegDT","desc");
            $query = $this->db->get("htb_mentor_board_sharing");

            if ($query->num_rows() > 0) {
                foreach ($query->result() as $key => $row) {
                    $data[$key] = $row;
                    $this->load->model('Category_Model', 'Category_Model');
                    // $data[$key]->fCatSeq = $this->Category_Model->get_category_name_by_code($row->fCatSeq);
                }

                if (!empty($type) && !empty($keyword)) {
                    $this->cache->memcached->save('fetch_sharing' . $limit . $start . $fCatSeq . $fTab . $type . $keyword, $data, 10000);
                } else {
                    $this->cache->memcached->save('fetch_sharing' . $limit . $start . $fCatSeq . $fTab, $data, 10000);
                }

                $this->cache->memcached->save('fetch_sharing' . $limit . $start . $fCatSeq . $fTab . $type . $keyword, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function get_best_reply_sharing($limit, $start, $fCatSeq = null)
    {
        $cache = $this->cache->memcached->get('get_best_reply_sharing'.$limit.$start);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('reply.fBSeq, sharing.fSeq, sharing.fTitle, sharing.fCatSeq,sharing.fRegDT,sharing.fUserID, sharing.fFirstRegDT, COUNT(reply.fBSeq) as total');
            $this->db->from('htb_mentor_reply_sharing as reply');
            $this->db->join('htb_mentor_board_sharing as sharing', 'reply.fBSeq = sharing.fSeq');
            $this->db->group_by("fBSeq");
            $this->db->order_by('total', 'desc');
            $this->db->limit($limit, $start);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_best_reply_sharing'.$limit.$start, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function fetch_best_hit_sharing($limit, $start, $fCatSeq = null, $fTab = null, $type = null, $keyword = null)
    {
        if (!empty($type) && !empty($keyword)) {
            $cache = $this->cache->memcached->get('fetch_best_hit_sharing' . $limit . $start . $fCatSeq . $fTab . $type . $keyword);
        } else {
            $cache = $this->cache->memcached->get('fetch_best_hit_sharing' . $limit . $start . $fCatSeq . $fTab);
        }
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fMB, fSeq, fTitle, fCatSeq,fRegDT, fUserID');
            $this->db->limit($limit, $start);
            if (!empty($type) && !empty($keyword)) {
                $this->db->like($type, $keyword);
            }
            if (!empty($fCatSeq)) {
                $this->db->like('fCatSeq', $fCatSeq, 'after');
            }
            $this->db->order_by('fHit', 'desc');
            $query = $this->db->get("htb_mentor_board_sharing");

            if ($query->num_rows() > 0) {
                foreach ($query->result() as $key => $row) {
                    $data[$key] = $row;
                    $this->load->model('Category_Model', 'Category_Model');
                    $data[$key]->fCatSeq = $this->Category_Model->get_category_name_by_code($row->fCatSeq);
                }

                if (!empty($type) && !empty($keyword)) {
                    $this->cache->memcached->save('fetch_best_hit_sharing' . $limit . $start . $fCatSeq . $fTab . $type . $keyword, $data, 10000);
                } else {
                    $this->cache->memcached->save('fetch_best_hit_sharing' . $limit . $start . $fCatSeq . $fTab, $data, 10000);
                }

                $this->cache->memcached->save('fetch_best_hit_sharing' . $limit . $start . $fCatSeq . $fTab . $type . $keyword, $data, 10000);
                return $data;
            }
            return false;
        }
    }

    function get_sharing($id)
    {
        $cache = $this->cache->memcached->get('get_sharing' . $id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_board_sharing');
            $this->db->where('fSeq', $id);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_sharing' . $id, $data, 10000);
                return $data;
            }
        }
    }

    public function best_sharing($limit = 10)
    {
        $cache = $this->cache->memcached->get('best_sharing');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('reply.fBSeq, sharing.fSeq, sharing.fTitle as title, sharing.fCatSeq, sharing.fFirstRegDT, COUNT(reply.fBSeq) as total, reply.fTitle');
            $this->db->from('htb_mentor_reply_sharing as reply');
            $this->db->join('htb_mentor_board_sharing as sharing', 'reply.fBSeq = sharing.fSeq');
            $this->db->group_by("fBSeq");
            $this->db->order_by('total', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('best_sharing', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    public function set_sharing()
    {
        $data = array(
            'fTitle' => $this->input->post('fTitle'),
            'fAreaCode' => $this->input->post('fTitle'),
            'fAreaDepth' => $this->input->post('fTitle'),
            'fIP' => $this->input->post('fTitle'),
            'fHit' => 0,
            'fRecommend' => 0,
            'fNotRecommend' => 0,
            'fReplyCount' => 0,
            'fHideID' => 1,
            // 'AREA_IDX' => $this->input->post('choosearea'),
            'AREA_IDX' => 11111,
            'fHideIDMy' => 1,
            'fCatSeq' => $this->input->post('categoryName'),
            'fMB' => $this->input->post('fMB'),
            'fContent' => $this->input->post('fWrite'),
        );

        $this->db->insert('htb_mentor_board_sharing', $data);

        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function get_best_sharing($limit = 10)
    {
        $cache = $this->cache->memcached->get('get_best_sharing');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('tbl_mentor_user_basic');
            $this->db->order_by('n4CurrentlyLevelPoint', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('get_best_sharing', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    /**
     * Indexed
     * Created by Ha Anh Son
     * @param $fSeq
     * @return mixed
     */
    function get_total_reply($fSeq = null)
    {
        $cache = $this->cache->memcached->get('get_total_reply_sharing' . $fSeq);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fBSeq');
            $this->db->from('htb_mentor_reply_sharing');
            $this->db->where('fBSeq', $fSeq);
            $data = $this->db->count_all_results();
            $this->cache->memcached->save('get_total_reply_sharing' . $fSeq, $data, 10000);
            return $data;
        }
        return false;
    }


    public function most_recommended_sharing($limit = 10)
    {
        $cache = $this->cache->memcached->get('most_recommended_sharing');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('fSeq,fTitle, fRecommend');
            $this->db->from('htb_mentor_board_sharing');
            $this->db->order_by('fRecommend', 'desc');
            $this->db->limit($limit, 0);
            $query = $this->db->get();
            $data = $query->result();
            if ($data) {
                $this->cache->memcached->save('most_recommended_sharing', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    function get_reply_sharing_by_id($id)
    {
        $cache = $this->cache->memcached->get('get_reply_sharing_by_id' . $id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_sharing');
            $this->db->where('fSeq', $id);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_reply_sharing_by_id' . $id, $data, 10000);
                return $data;
            }
        }
    }

    public function sharing_hit_increase($id=null)
    {
        $qa_info = $this->get_sharing($id);
        if($qa_info) {
            $this->cache->memcached->delete('get_sharing'.$id);
            $data = array(
                'fHit' => $qa_info->fHit + 1,
            );
            $this->db->where('fSeq',$id);
            return $this->db->update('htb_mentor_board_sharing',$data);
        }
        return false;
    }

    public function add_sharing_report($id = null)
    {
        $qa_info = $this->get_sharing($id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($id, $_COOKIE['USERID'],4);
        if ($qa_info) {
            if(!$vote) {
                $this->cache->memcached->delete('get_sharing' . $id);
                $data_vote = array(
                    'fType' => 4,
                    'fBSeq' => $id,
                    'fNotRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fNotRecommend' => $qa_info->fNotRecommend + 1,
                );
                $this->db->where('fSeq', $id);
                return $this->db->update('htb_mentor_board_sharing', $data);
            } else {
                return 'duplicated';
            }
        }
        return false;
    }

    public function add_sharing_reply_report($reply_id = null, $qa_id = null)
    {
        $qa_info = $this->get_reply_sharing_by_id($reply_id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($reply_id, $_COOKIE['USERID'],4);
        if ($qa_info) {
            if(!$vote) {
                $this->cache->memcached->delete('get_reply_sharing_by_id' . $reply_id);
                $this->cache->memcached->delete('get_reply_sharing' . $qa_id);
                $data_vote = array(
                    'fType' => 4,
                    'fBSeq' => $reply_id,
                    'fNotRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fNotRecommend' => $qa_info->fNotRecommend + 1,
                );
                $this->db->where('fSeq', $reply_id);
                return $this->db->update('htb_mentor_reply_sharing', $data);
            } else {
                return 'duplicated';
            }

        }
        return false;
    }

    public function add_sharing_vote($id = null)
    {
        $qa_info = $this->get_sharing($id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($id, $_COOKIE['USERID'],3);
        if ($qa_info) {
            $this->cache->memcached->delete('get_reply_sharing' . $id);
            if(!$vote) {
                $data_vote = array(
                    'fType' => 3,
                    'fBSeq' => $id,
                    'fRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fRecommend' => $qa_info->fRecommend + 1,
                );
                $this->db->where('fSeq', $id);
                return $this->db->update('htb_mentor_board_sharing', $data);
            } else {
                return 'duplicated';
            }
        }
        return false;
    }

    public function add_sharing_reply_vote($reply_id = null, $qa_id = null)
    {
        $qa_info = $this->get_reply_sharing_by_id($reply_id);
        $this->load->model('Vote_Model', 'Vote_Model');
        $vote = $this->Vote_Model->get_vote_by_id($reply_id, $_COOKIE['USERID'],3);
        if ($qa_info) {
            if(!$vote) {
                $this->cache->memcached->delete('get_reply_sharing_by_id' . $reply_id);
                $this->cache->memcached->delete('get_reply_sharing' . $qa_id);
                $data_vote = array(
                    'fType' => 3,
                    'fBSeq' => $reply_id,
                    'fRecommend' => 1,
                    'fUserID' => $_COOKIE['USERID'],
                );
                $this->db->insert('htb_mentor_qa_vote', $data_vote);
                $data = array(
                    'fRecommend' => $qa_info->fRecommend + 1,
                );
                $this->db->where('fSeq', $reply_id);
                return $this->db->update('htb_mentor_reply_sharing', $data);
            } else {
                return 'duplicated';
            }

        }
        return false;
    }
}

?>

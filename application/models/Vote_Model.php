<?php

class Vote_Model extends CI_Model
{
    function __construct()
    {
        $this->load->driver('cache');
        return parent::__construct();
    }

    function get_vote_by_id($fBSeq = null, $userId = null, $type = null)
    {
        $cache = $this->cache->memcached->get('get_vote_by_id' . $fBSeq.$userId.$type);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_qa_vote');
            $this->db->where('fBSeq', $fBSeq);
            $this->db->where('fUserID', $userId);
            $this->db->where('fType', $type);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_vote_by_id' . $fBSeq.$userId.$type, $data, 10000);
                return $data;
            }

        }
        return false;
    }

//    function get_vote_by_user_id($user = null)
//    {
//        $cache = $this->cache->memcached->get('get_vote_by_id' . $fBSeq);
//        if ($cache) {
//            return $cache;
//        } else {
//            $this->db->select('*');
//            $this->db->from('htb_mentor_qa_vote');
//            $this->db->where('fBSeq', $fBSeq);
//            $query = $this->db->get();
//            $data = $query->first_row();
//            if ($data) {
//                $this->cache->memcached->save('get_vote_by_id' . $fBSeq, $data, 10000);
//                return $data;
//            }
//
//        }
//        return false;
//    }
}

?>

<?php

/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 5/11/2016
 * Time: 10:41 AM
 */
class Category_Model extends CI_Model
{

    function __construct()
    {
        $this->load->driver('cache');
        return parent::__construct();
    }

    public function record_count($type = null, $keyword = null)
    {
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
            $this->db->from('htb_mentor_category');
            return $this->db->count_all_results();
        } else {
            return $this->db->count_all("htb_mentor_category");
        }
    }

    /**
     * Indexed
     * Created by Ha Anh Son
     * @return mixed
     */
    public function get_all_category()
    {
        $cache = $this->cache->memcached->get('get_all_category');
        if ($cache) {
            return $cache;
        } else {

            $query = $this->db->get_where('htb_mentor_category', array('parent_code' => '01', 'status' => 2));
            $data = $query->result();
            $this->cache->memcached->save('get_all_category', $data, 10000);
            return $data;
        }
    }

    /**
     * Indexed
     * Created by Ha Anh Son
     * @return mixed
     */
    public function get_sharing_category($parent_category = '')
    {
        $cache = $this->cache->memcached->get('get_sharing_category'.$parent_category);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('code, name, en_name');
            $this->db->where('status', 1);
            $this->db->where('parent_code', $parent_category);
            $this->db->like('code', '05', 'after');
            $query = $this->db->get("htb_mentor_category");
            $data = $query->result();
            $this->cache->memcached->save('get_sharing_category'.$parent_category, $data, 10000);
            return $data;
        }
    }

    public function fetch_category($limit, $start, $sortfield = null, $order = null, $type = null, $keyword = null)
    {
        $this->db->limit($limit, $start);
        if (!empty($type) && !empty($keyword)) {
            $this->db->like($type, $keyword);
        }
        $this->db->order_by("$sortfield", "$order");
        $query = $this->db->get("htb_mentor_category");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function get_category_name_by_code($id)
    {
        $cache = $this->cache->memcached->get('get_category_name_by_code' . $id);
        if ($cache) {
            return $cache->en_name;
        } else {
            $this->db->select('en_name');
            $this->db->from('htb_mentor_category');
            $this->db->where('code', $id);
            $query = $this->db->get();
            $data = $query->first_row();
            if ($data) {
                $this->cache->memcached->save('get_category_name_by_code' . $id, $data, 10000);
                return $data->en_name;
            }
            return false;
        }
    }

    function most_popular_keywords()
    {
        $cache = $this->cache->memcached->get('most_popular_keywords');
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_categorykeyword');
            $this->db->order_by('fCount', 'desc');
            $this->db->limit(10, 0);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $data[] = $row;
                }

                $this->cache->memcached->save('most_popular_keywords', $data, 10000);
                return $data;
            }
            return false;
        }
    }

    function get_sub_category_by_code($code)
    {
        $cache = $this->cache->memcached->get('get_sub_category_by_code' . $code);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_category');
            $this->db->where('parent_code', $code);
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $data[] = $row;
                }

                $this->cache->memcached->save('get_sub_category_by_code'. $code, $data, 10000);
                return $data;
            }

            return false;
        }
    }

}

?>

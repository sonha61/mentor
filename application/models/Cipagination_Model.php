<?php
class Cipagination_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    function record_count()
    {
        return $this->db->count_all('htb_mentor_category');
    }
    public function fetch_employees($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("htb_mentor_category");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function fetch_qa($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("htb_mentor_board_qa");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

}
?>
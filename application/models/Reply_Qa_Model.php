<?php

class Reply_Qa_Model extends CI_Model
{

    function get_reply_qa($id)
    {
        $cache = $this->cache->memcached->get('get_reply_qa'.$id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fBSeq', $id);
            $this->db->where('fSeq = fPSeq');
            // $this->db->or_where("fPSeq IS NULL", null, false);
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save('get_reply_qa'.$id , $data, 10000);
            return $data;
        }
    }

//    function get_reply_qa_by_id($id)
//    {
//        $this->db->select('*');
//        $this->db->from('htb_mentor_reply_qa');
//        $this->db->where('fSeq', $id);
//        $query = $this->db->get();
//        return $query->result();
//    }

    function get_reply_qa_by_member_id($memberId, $limit = 5)
    {
        $cache = $this->cache->memcached->get('get_reply_qa_by_member_id'.$memberId);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fUserID', $memberId);
            $this->db->limit($limit);
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save('get_reply_qa_by_member_id'.$memberId , $data, 10000);
            return $data;
        }
    }

    public function add_reply($id)
    {
        $fHideID = ($this->input->post('fHideID') == 'on') ? 1 : 0;
        $data = array(
            'fTitle' => $this->input->post('fTitle'),
            'fFrom' => $this->input->post('fFrom'),
            'fContent' => $this->input->post('fContent'),
            'fHideID' => $fHideID,
            'fBSeq' => $id,
            'fPSeq' => $id,
            'fUserID' => isset($_COOKIE['USERID']) ? $_COOKIE['USERID'] : 'simon',
            'fUser_Key' => isset($_COOKIE['USERKEY']) ?  $_COOKIE['USERKEY'] : 'simon',
        );
        $this->db->insert('htb_mentor_reply_qa', $data);
        $this->cache->memcached->delete('get_reply_qa' . $id);
        $this->db->set('fPSeq', $this->db->insert_id(), FALSE);
        $this->db->where('fSeq', $this->db->insert_id());
        return $this->db->update('htb_mentor_reply_qa'); 

    }

    public function add_reply_comment($id)
    {
        $fHideID = ($this->input->post('fHideIDReply') == 'on') ? 1 : 0;
        $data = array(
            'fTitle' => '',
            'fFrom' => $this->input->post('fFrom'),
            'fContent' => $this->input->post('fComment'),
            'fHideID' => $fHideID,
            'fPSeq' => $this->input->post('replyID'),
            'fBSeq' => $id,
            'fDepth' => 1,
            'fUserID' => isset($_COOKIE['USERID']) ? $_COOKIE['USERID'] : 'simon',
            'fUser_Key' => isset($_COOKIE['USERKEY']) ? $_COOKIE['USERKEY'] : 'simon',
        );

        $this->cache->memcached->delete('get_reply_qa' . $id);
        $this->cache->memcached->delete('get_reply_comment_qa' . $this->input->post('replyID'));
        $this->cache->memcached->delete('get_total_reply_comment' . $this->input->post('replyID'));
        return $this->db->insert('htb_mentor_reply_qa', $data);
    }

    public function record_count_last_month($catId = null)
    {
        $cache = $this->cache->memcached->get('answered_record_count_last_month'.$catId);
        if ($cache) {
            return $cache;
        } else {
            if($catId) {
                $sql = "SELECT `reply`.`fBSeq`, `qa`.`fSeq`, `qa`.`fCatSeq`  
                    FROM `htb_mentor_reply_qa` as `reply` 
                    JOIN `htb_mentor_board_qa` as `qa` 
                    ON `reply`.`fBSeq` = `qa`.`fSeq` 
                    WHERE `reply`.`fRegDT` >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND fCatSeq LIKE ('$catId%')";

                $query = $this->db->query($sql);
                $data = $query->num_rows();
                $this->cache->memcached->save('answered_record_count_last_month'.$catId , $data, 10000);
                return $data;
            } else {
                $sql = "SELECT COUNT(fSeq) FROM `htb_mentor_reply_qa` WHERE `fRegDT` >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)";
                $query = $this->db->query($sql);
                $data = $query->row_array();
                $this->cache->memcached->save('answered_record_count_last_month'.$catId , $data["COUNT(fSeq)"], 10000);
                return $data["COUNT(fSeq)"];
            }

        }
    }

    function get_reply_comment_qa($id)
    {
        $cache = $this->cache->memcached->get('get_reply_comment_qa'.$id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_qa');
            $this->db->where('fPSeq', $id);
            $this->db->where('fDepth is NOT NULL', NULL, FALSE);
          
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save('get_reply_comment_qa'.$id , $data, 10000);
            return $data;
        }
    }
}

?>

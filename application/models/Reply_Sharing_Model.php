<?php

class Reply_Sharing_Model extends CI_Model
{
    function get_reply_sharing($id)
    {
        $cache = $this->cache->memcached->get('get_reply_sharing'.$id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_sharing');
            $this->db->where('fBSeq', $id);
            $this->db->where('fPSeq IS NULL', null, false);
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save('get_reply_sharing'.$id , $data, 10000);
            return $data;
        }
    }

    function get_reply_comment_sharing($id)
    {
        $cache = $this->cache->memcached->get('get_reply_comment_sharing'.$id);
        if ($cache) {
            return $cache;
        } else {
            $this->db->select('*');
            $this->db->from('htb_mentor_reply_sharing');
            $this->db->where('fPSeq', $id);
            $query = $this->db->get();
            $data = $query->result();
            $this->cache->memcached->save('get_reply_comment_sharing'.$id , $data, 10000);
            return $data;
        }
    }

    public function add_reply($id)
    {
        $fHideID = ($this->input->post('fHideID') == 'on') ? 1 : 0;
        $data = array(
            'fContent' => $this->input->post('fContent'),
            'fHideID' => $fHideID,
            'fBSeq' => $id,
            'fUserID' => 'sonhaanh',
            'fUser_Key' => '1234567',
        );
        $this->cache->memcached->delete('get_reply_sharing' . $id);
        return $this->db->insert('htb_mentor_reply_sharing', $data);
    }


     public function add_reply_comment($id)
    {
        $fHideID = ($this->input->post('replyID') == 'on') ? 1 : 0;
        $data = array(
            'fTitle' => '',
            'fFrom' => $this->input->post('fFrom'),
            'fContent' => $this->input->post('fComment'),
            'fHideID' => $fHideID,
            'fPSeq' => $this->input->post('replyID'),
            'fBSeq' => $id,
            'fDepth' => 1,
            'fUserID' => isset($_COOKIE['USERID']) ? $_COOKIE['USERID'] : 'simon',
            'fUser_Key' => isset($_COOKIE['USERKEY']) ? $_COOKIE['USERKEY'] : 'simon',
        );

        $this->cache->memcached->delete('get_reply_sharing' . $id);
        $this->cache->memcached->delete('get_reply_comment_sharing' . $this->input->post('replyID'));
        $this->cache->memcached->delete('get_total_reply_comment_sharing' . $this->input->post('replyID'));
        return $this->db->insert('htb_mentor_reply_sharing', $data);
    }
}

?>

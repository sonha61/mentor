<?php

namespace Entity;

/**
 * Category Model
 *
 * @Entity
 * @Table(name="asset_category")
 * @author  Joseph Wynn <joseph@wildlyinaccurate.com>
 */
class Category
{

	/**
	 * @Id
	 * @Column(type="integer", nullable=false)
	 * @GeneratedValue(strategy="AUTO")
	 */
	protected $ac_seq;

	/**
	 * @Column(type="string", length=32, nullable=false)
	 */
	protected $ac_code;

	/**
	 * @Column(type="string", length=64, nullable=false)
	 */
	protected $ac_name;

	/**
	 * @Column(type="integer", length=11, nullable=false)
	 */
	protected $ac_qty;

	/**
	 * @Column(type="string", length=255, nullable=false)
	 */
	protected $ac_remark;

	/**
	 * @Column(type="integer", length=11, nullable=false)
	 */
	protected $wdate;

	/**
	 * @Column(type="integer", length=11, nullable=false)
	 */
	protected $mdate;

	/**
	 * @Column(type="integer", length=11, nullable=false)
	 */
	protected $status;

	public function setACCode($ac_code)
	{
		$this->ac_code = $ac_code;
		return $this;
	}

	public function setACName($ac_name)
	{
		$this->ac_name = $ac_name;
		return $this;
	}

	public function setACQty($ac_qty)
	{
		$this->ac_qty = $ac_qty;
		return $this;
	}

	public function setACRemark($ac_remark)
	{
		$this->ac_remark = $ac_remark;
		return $this;
	}


	public function setWdate($wdate)
	{
		$this->wdate = $wdate;
		return $this;
	}

	public function setMdate($mdate)
	{
		$this->mdate = $mdate;
		return $this;
	}

	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	public function getACCode()
	{
		return $this->ac_code;
	}

	public function getACName()
	{
		return $this->ac_name;
	}

	public function getACQty()
	{
		return $this->ac_qty;
	}

	public function getACRemark()
	{
		return $this->ac_remark;
	}

	public function getWdate()
	{
		return $this->wdate;
	}

	public function getMdate()
	{
		return $this->mdate;
	}
}

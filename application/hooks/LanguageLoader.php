<?php
/**
 * Created by Ha Anh Son
 * User: heyso
 * Date: 5/9/2016
 * Time: 10:42 AM
 */
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');
        $ci->lang->load('message','english');
    }
}
<!-- #header -->
<div class="container">
    <div class="header-wrap clearfix">
        <div class="title-page-wrap">
            <a class="logo" href="index.html"><img src="<?php echo $base_url ?>assets/mentor_new/images/hey-korean-logo.png" alt="Hey!Korean"></a>
            <h1 class="webname"><a href="index.html"><img src="<?php echo $base_url ?>assets/mentor_new/images/mentoring.png" alt="Mentoring"></a></h1>
        </div>
        <div class="profile-wrap">
            <!-- Single button -->
            <div class="btn-group">
                <button type="button" class="btn btn-profile dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt="">user name <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Information</a></li>
                    <li><a href="#">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <nav class="menu-wrap clearfix">
        <ul>
            <li class="active"><a href="<?php echo $base_url . "qa/";?>">Home</a></li>
            <li><a href="<?php echo $base_url . "qa/category/01";?>">Us life</a></li>
            <li><a href="<?php echo $base_url . "qa/category/02";?>">Advice</a></li>
            <li><a href="<?php echo $base_url . "qa/category/03";?>">Local info</a></li>
            <li><a href="<?php echo $base_url . "sharing";?>">Sharing</a></li>
            <li><a href="<?php echo $base_url . "qa/best";?>">Best</a></li>
            <li><a href="<?php echo $base_url . "user/myMentor";?>">My mentoring</a></li>
        </ul>
        <div class="search-wrap">
            <input type="text" name="search-box">
        </div>
    </nav>
    <div class="notice-wrap clearfix">
        <h3>Notice</h3>
        <a href="#">헤이코리안 멘토링 페이지를 개편하였습니다.</a>
    </div>
</div>

<!-- common head files -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:type" content="website">
<meta property="og:title" content="">
<meta property="og:site_name" content="">
<meta property="og:url" content="">
<meta property="og:image" content="">
<meta property="og:description" content="">
<link href="<?php echo $base_url ?>assets/mentor_new/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
<!-- <link href="<?php echo $base_url ?>assets/mentor_new/css/common.css" rel="stylesheet"> -->
<link rel="shortcut icon" href="<?php echo $base_url ?>assets/mentor_new/images/heykorean.ico" type="image/x-icon">

<link href="<?php echo $base_url ?>assets/mentor_new/lib/bootstrap/bootstrap-tagsinput.css" rel="stylesheet">
<link href="<?php echo $base_url ?>assets/mentor_new/lib/icheck/blue.css" rel="stylesheet">
<link href="<?php echo $base_url ?>assets/mentor_new/css/common.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- /common head files -->
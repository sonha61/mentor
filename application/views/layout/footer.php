    <div id="footer">
        <h2 class="hidden">heykorean mentoring footer</h2>
        <div class="footer_con">
            <div class="policy">
                <a href="http://www.heykorean.com/aboutus/company/main.html" title="About us" target="_blank">About us</a>
                <a href="http://www.heykorean.com/aboutus/company/proposal.html" title="Advertising · Sponsors" target="_blank">Advertising · Sponsors</a>
                <a href="http://www.heykorean.com/hk_help/privacy.asp" title="Terms" target="_blank">Terms</a>
                <a href="http://www.heykorean.com/hk_help/personal.asp" title="Privacy" target="_blank">Privacy</a>
                <a href="http://www.heykorean.com/hk_help/safe_tip.asp" title="Avoiding Scams &amp; Fraud" target="_blank">Avoiding Scams &amp; Fraud</a>
                <a href="http://www.heykorean.com/hk_help/manager_inquiry.asp" title="Contact us" target="_blank">Contact us</a>
                <a href="http://hr.heykorean.com" title="Recruit" target="_blank">Recruit</a>
                <a href="http://www.heykorean.com/hk_support/site_map.asp" title="Site Map" target="_blank">Site Map</a>
            </div>
            <p>Copyright © <strong> 1998-2016 HeyKorean,</strong> Inc. All Rights Reserved.</p>
        </div>
    </div>
</body>
    <script type="text/javascript">

        loadDirectory('01');
        function checksearchtext() {
            if ($("input[name*='search_name']").val().trim() == "") {
                alert("검색어를 입력해주세요.");
                return false;
            }
            else {
            }
        }

        function loadDirectory(category) {
            var select = $('#lstCategory2');
            //remove item
            for (var i = 0; i <= 5; i++) {
                $('#lstCategory' + i).children().remove().end();
                $('#setCategory' + i).text("");
            }

            $.ajax({
                type: "POST",
                url: "<?php echo base_url()."qa/getSubCagegory";?>",
                data:{category_code:category},
                dataType: "json",
                async: false,
                success: function(data) {
                    $.each(data, function( key, value ){
                        select.append("<li id=\"lstCategory2\" title='"+data[key].en_name+"' onclick=\"Category_Click(0, this, '2', '" + data[key].code +"');LoadCategory('3', '" + data[key].code +"');\"> " + data[key].en_name + "</li>");
                    });
                }
            });

        }
        function Category_Click(nLast, nID, nObj, nCode) {
            $(nID).addClass('select_on');
            //remove class
            $(nID).siblings().removeClass('select_on');
            $(nID).siblings().removeClass('select_on2');
            if ((parseInt(nObj)) > 2 && nLast == 0) {
                $('#lstCategory' + (parseInt(nObj) - 1)).find('.select_on').addClass('select_on2');
            }
        }

        function LoadCategory(nObj, nCode) {
            var select = $('#lstCategory' + nObj);
            for (var i = (parseInt(nObj)); i <= 5; i++) {
                $('#lstCategory' + i).children().remove().end();
//                $('#setCategory' + i).text("");
            }

            name = 'categoryName';
            $('input[name="'+name+'"]').val(nCode);

            $.ajax({
                type: "POST",
                url: "<?php echo base_url()."qa/getSubCagegory";?>",
                data:{category_code:nCode},
                dataType: "json",
                async: false,
                success: function(data) {
                    $.each(data, function( key, value ){
                            select.append("<li id=\"lstCategory" + nObj + "\" title='"+data[key].en_name+"' onclick=\"Category_Click(0, this, '" + nObj + "','2');LoadCategory('" + (parseInt(nObj) + 1) + "', '" + data[key].code +"');\"> " + data[key].en_name + "</li>");
                    });
                }
            });
        }

        $(document).ready(function () {
            $("select[name='sortByCategory']").change(function(){
                var optionValue = $("select[name='sortByCategory']").val();
                alert(optionValue);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url()."/Qa/getQaByCat";?>",
                    data:{category:optionValue},
                    beforeSend: function(){
                        $("#loadingmessage").css({ 'display': "block" });
                    },
                    success: function(response){
                        $("#loadingmessage").css({ 'display': "none" });
                    }
                });
            });

            $("select[name='cboArea1']").change(function(){
                var optionValue = $("select[name='cboArea1']").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url()."/Qa/getCity";?>",
                    data:{country:optionValue},
                    success: function(response){
                        $("select[name='cboArea2']").html(response);
                    }
                });
            });

            $("select[name='cboArea2']").change(function(){
                var optionValue = $("select[name='cboArea2']").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url()."/Qa/getCity";?>",
                    data:{country:optionValue},
                    success: function(response){
                        $("select[name='cboArea3']").html(response);
                    }
                });
            });

            $("select[name='cboArea3']").change(function(){
                var optionValue = $("select[name='cboArea3']").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url()."/Qa/getCity";?>",
                    data:{country:optionValue},
                    success: function(response){
                        $("select[name='cboArea4']").html(response);
                    }
                });
            });
        });

        function addVote(id,action, type) {
            var heycookies = getCookie("heykorean");
            if(heycookies == '') {
                alert('You have to login');
            } else {
                $.ajax({
                    url: "<?php echo base_url()."Qa/addVote";?>",
                    data:{id:id, action: action, type : type},
                    type: "POST",
                    beforeSend: function(){
                        if(action == 'report') {
                            $("#loading_report").css({ 'display': "block" });
                        } else {
                            $("#loadingmessage").css({ 'display': "block" });
                        }
                    },
                    success: function(result){
                        if(JSON.parse(result) == "duplicated") {
                            if(action == 'report') {
                                var votes = $('#fNotRecommend').html();
                                $('#fNotRecommend').html(parseInt(votes));
                                $("#loading_report").css({ 'display': "none" });
                            } else {
                                var votes = $('#fRecommend').html();
                                $('#fRecommend').html(parseInt(votes));
                                $("#loadingmessage").css({ 'display': "none" });
                            }
                        } else {
                            if(action == 'report') {
                                var votes = $('#fNotRecommend').html();
                                $('#fNotRecommend').html(parseInt(votes) + 1);
                                $("#loading_report").css({ 'display': "none" });
                            } else {
                                var votes = $('#fRecommend').html();
                                $('#fRecommend').html(parseInt(votes) + 1);
                                $("#loadingmessage").css({ 'display': "none" });
                            }
                        }
                    }
                });
            }
        }

        function addVoteReply(reply_id,action,qa_id, type) {
            var heycookies = getCookie("heykorean");
            if(heycookies == '') {
                alert('You have to login');
            } else {
                $.ajax({
                    url: "<?php echo base_url()."Qa/addVoteReply";?>",
                    data:{reply_id:reply_id, action: action, qa_id: qa_id, type: type},
                    type: "POST",
                    beforeSend: function(){
                        if(action == 'report') {
                            $("#loading_report").css({ 'display': "block" });
                        } else {
                            $("#loadingmessage").css({ 'display': "block" });
                        }
                    },
                    success: function(result){
                        if(JSON.parse(result) == "duplicated") {
                            if(action == 'report') {
                                var votes = $('#fNotReplyRecommend'+reply_id).html();
                                $('#fNotReplyRecommend'+reply_id).html(parseInt(votes));
                                $("#loading_report").css({ 'display': "none" });
                            } else {
                                var votes = $('#fReplyRecommend'+reply_id).html();
                                $('#fReplyRecommend'+reply_id).html(parseInt(votes));
                                $("#loadingmessage").css({ 'display': "none" });
                            }
                        } else {
                            if(action == 'report') {
                                var votes = $('#fNotReplyRecommend'+reply_id).html();
                                $('#fNotReplyRecommend'+reply_id).html(parseInt(votes) + 1);
                                $("#loading_report").css({ 'display': "none" });
                            } else {
                                var votes = $('#fReplyRecommend'+reply_id).html();
                                $('#fReplyRecommend'+reply_id).html(parseInt(votes) + 1);
                                $("#loadingmessage").css({ 'display': "none" });
                            }
                        }
                    }
                });
            }
        }

        function validateFormReply() {
            var fComment = document.forms["writeReply"]["fComment"].value;
            if (fComment == null || fComment == "") {
                alert("Content must be filled out");
                return false;
            }
        }

        function validateFormReplySharing() {
            var fContent = document.forms["writeReplySharing"]["fContent"].value;
            if (fContent == null || fContent == "") {
                alert("Content must be filled out");
                return false;
            }
        }

        function validateForm() {
            var fContent = tinyMCE.get('fContent').getContent();
            var fTitle = document.forms["writeanswer"]["fTitle"].value;

            if (fTitle == null || fTitle == "") {
                alert("Title must be filled out");
                return false;
            }
            if (fContent == null || fContent == "") {
                alert("Content must be filled out");
                return false;
            }
        }
        function validateFormWrite() {
            var fWrite = tinyMCE.get('fWrite').getContent();
            var fTitle = document.forms["formWrite"]["fTitle"].value;
            var location = document.forms["formWrite"]["choosearea"].value;
            var category = document.forms["formWrite"]["categoryName"].value;

            if (fTitle == null || fTitle == "") {
                alert("Title must be filled out");
                return false;
            }
            if (fWrite == null || fWrite == "") {
                alert("Content must be filled out");
                return false;
            }
            if (location == null || location == "") {
                alert("Location must be filled out");
                return false;
            }
            if (category == null || category == "") {
                alert("Category must be filled out");
                return false;
            }
        }

        function getColor(fMb) {
            var color = '';
            if(fMb < 50) {
                color = 'waterdrops01';
            } else if (fMb > 50 && fMb < 100) {
                color = 'waterdrops02';
            } else {
                color = 'waterdrops03';
            }
            return color;
        }

        function openPopup(url, w,h){
            var winl = (screen.width - w) / 2;
            var wint = (screen.height - h) / 2;

            window.open(url,'_blank','width='+w+', height='+h+', Scrollbars=no, top='+wint+', left='+winl);
        }
    </script>
</html>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <link rel="icon" href="http://image1.heykorean.com/v1/Icon/heykorean.ico" type="image/x-icon">
    <meta property="og:title" content="<?php echo $meta_title;?>">
    <meta property="og:description" content="<?php echo $meta_description;?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo $og_url;?>"/>
    <meta property="og:image" content="<?php echo $og_image;?>">
    <meta property="og:site_name" content="HeyKorean.com">
    <meta property="fb:app_id" content="154780491386487" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta name="title" content="<?php echo $meta_title;?>">
    <meta name="keywords" content="<?php echo $meta_keywords;?>">
    <meta name="description" content="<?php echo $meta_description;?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<?php echo $base_url ?>assets/mentor_new/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo $base_url ?>assets/mentor_new/images/heykorean.ico" type="image/x-icon">
    <link href="<?php echo $base_url ?>assets/mentor_new/lib/bootstrap/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>assets/mentor_new/lib/icheck/blue.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>assets/mentor_new/css/common.css" rel="stylesheet">
    <title>HeyKorean Mentoring</title>
   <!--  <link rel="stylesheet" href="<?php echo $base_url ?>assets/css/common.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/css/mentor.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo $base_url ?>assets/css/etc.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo $base_url ?>assets/css/jquery.bxslider.css"> -->
<!--    <script type="text/javascript" src="--><?php //echo $base_url ?><!--assets/js/jquery-1.12.3.min.js"></script>-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo $base_url ?>assets/js/common.js"></script> -->
<!--    <script type="text/javascript" src="--><?php //echo $base_url ?><!--assets/js/market.js"></script>-->
    <!-- <script type="text/javascript" src="<?php echo $base_url ?>assets/js/market_common.js"></script> -->
    <script type="text/javascript" src="<?php echo $base_url ?>assets/js/adutil-v3.js"></script>
    <!-- <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> -->
    <!-- Customs CSS -->
    <?php
        if (isset($style)){
            foreach ($style as $url){
                echo "<link rel='stylesheet' href='" . $url . "'>";
            }
        }

    ?>
    <!-- Customs JS -->
    <?php
        if (isset($scripts)){
            foreach ($scripts as $url){
                echo "<script type='text/javascript' src='" . $url . "'></script>";
            }
        }

    ?>
</head>
<body>


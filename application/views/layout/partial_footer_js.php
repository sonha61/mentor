<script type="text/javascript">
	$(".btn-collapse-info").click(function(){
	    $("#main-list").addClass("collapse-info");
	    $("#main-list").removeClass("expand-info");
	    $(".btn-collapse-info").addClass("active");
	    $(".btn-expand-info").removeClass("active");
	});
	$(".btn-expand-info").click(function(){
	    $("#main-list").addClass("expand-info");
	    $("#main-list").removeClass("collapse-info");
	    $(".btn-expand-info").addClass("active");
	    $(".btn-collapse-info").removeClass("active");
	});
    function validateFormReplySharing(id) {
            var fContent = document.forms["writeReplySharing"]["fContent"+id].value;
            if (fContent == null || fContent == "") {
                alert("Content must be filled out");
                return false;
            }
    }

    var inputQuantity = [];
    $(function() {
      $(".quantity").each(function(i) {
        inputQuantity[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $(".quantity").on("keyup", function (e) {
        var $field = $(this),
            val=this.value,
            $thisIndex=parseInt($field.data("idx"),10); // retrieve the index
//        window.console && console.log($field.is(":invalid"));
          //  $field.is(":invalid") is for Safari, it must be the last to not error in IE8
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputQuantity[$thisIndex];
            return;
        } 
        if (val.length > 4 || val > 1000) {
          val=1000;
          $field.val(val);
        } 
        inputQuantity[$thisIndex]=val;
      });      
    });


    function validateFormWrite() {
            // var fWrite = tinyMCE.get('fWrite').getContent();
            var fWrite = document.forms["formWrite"]["fWrite"].value;
            var fTitle = document.forms["formWrite"]["fTitle"].value;
            // alert(fTitle);
            // var location = document.forms["formWrite"]["choosearea"].value;
            // var category = document.forms["formWrite"]["categoryName"].value;

            if (fTitle == null || fTitle == "") {
                alert("Title must be filled out");
                return false;
            }
            if (fWrite == null || fWrite == "") {
                alert("Content must be filled out");
                return false;
            }
            // if (location == null || location == "") {
            //     alert("Location must be filled out");
            //     return false;
            // }
            // if (category == null || category == "") {
            //     alert("Category must be filled out");
            //     return false;
            // }
    }
</script>
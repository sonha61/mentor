<!-- Footer -->
<div class="container">
    <div class="footer-menu">
        <ul>
            <li>
                <a href="">About Us</a>
            </li>
            <li>
                <a href="">Advertising</a>
            </li>
            <li>
                <a href="">Sponsors</a>
            </li>
            <li>
                <a href="">Terms</a>
            </li>
            <li>
                <a href="">Personal</a>
            </li>
            <li>
                <a href="">Avoiding Scams & Fraud </a>
            </li>
            <li>
                <a href="">Contact us</a>
            </li>
        </ul>
    <span class="copyright">
      Copyright © <img src="<?php echo $base_url ?>assets/mentor_new/images/hey-korean-logo.png"> All Right Reserved.
    </span>
    </div>
</div>
<!-- Footer -->
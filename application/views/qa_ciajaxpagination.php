<div class="box-subject clearfix">
    <h3>US Life</h3>
    <div class="sort-by">
        <dl>
            <dt><label for="sortBy">Sort by : </label></dt>
            <dd>
                <select id="sortBy">
                    <option>US Life</option>
                    <option>Advice</option>
                    <option>Regional Info.</option>
                    <option>Multiple</option>
                    <option>Sharing</option>
                </select>
            </dd>
            <dd>
                <select>
                    <option>Within 1 week</option>
                    <option>Within 1 month</option>
                    <option>Within 3 months</option>
                    <option>Within 6 months</option>
                    <option>Within 1 year</option>
                </select>
            </dd>
        </dl>
    </div>
</div>
<table>
    <colgroup>
        <col style="width:80px">
        <col style="*">
        <col style="width:100px">
        <col style="width:100px">
        <col style="width:100px">
    </colgroup>
    <thead>
    <tr>
        <th scope="row">Areas<span></span></th>
        <th scope="row">Title<span></span></th>
        <th scope="row">Fields</th>
        <th scope="row">ID</th>
        <th scope="row">Newest</th>
    </tr>
    </thead>
    <tbody>

    <?php if($results) { foreach($results as $data) { ?>
        <tr>
            <td>NJ</td>
            <td><a class="mentor-title" href="<?php echo $base_url . "qa/view/" . $data->fSeq; ?>"><?php echo $data->fTitle;?></a><span class="comment-num">[<?php echo $this->Qa_Model->get_total_reply($data->fSeq);?>]</span></td>
            <td class="mentor-titlecate"><span><?php echo $data->fCatSeq;?></span></td>
            <td><?php echo $data->fUserID;?></td>
            <td data-utc-date="<?php echo date('M d, Y', strtotime($data->fFirstRegDT));?>"><?php echo date('M d, Y', strtotime($data->fFirstRegDT));?></td>
        </tr>
    <?php } } ?>
    </tbody>
</table>

<?php if($links) : ?>
    <div class="pagewrap" id="ajax_pagingsearc">
<!--        <a class="btn-first"><span class="hidden">처음목록</span></a>-->
<!--        <a class="btn-pre"><span class="hidden">이전목록</span></a>-->
        <?php echo $links; ?>
<!--        <a class="btn-next"><span class="hidden">다음목록</span></a>-->
<!--        <a class="btn-last"><span class="hidden">마지막목록</span></a>-->
    </div>
<?php endif;?>


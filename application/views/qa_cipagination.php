<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring" href="<?php echo $base_url;?>"><!--<img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring">-->Mentoring</a>
                    <span><a class="p-area-title"></a></span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
							0<span>Bxs</span>
							0<span>Bts</span>
							0<span>Dps</span>
						</a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
            <div class="location">
                <a href="<?php echo $base_url;?>">Home</a> &gt;
                <a href="<?php echo $base_url.'qa/best'?>"><strong>Best</strong></a>
            </div>
            <div class="newly-wrap">
                <span class="announcement">공지</span>
                <p><a href="http://www.heykorean.com/HK_Service/Event/board.asp?mode=view&idx=255&page=1&SearchType=&SearchKeyword=">멘토링을 개편 하였습니다. 멘토링을 개편 하였습니다.</a></p>
            </div>
        </div>

        <div class="top-con clearfix">
            <ul class="category-wrap best-top">
                <li class="first">
                    <div>
                        <a href="<?php echo $base_url . "qa/view/" . $results[0]->fSeq; ?>"><?php echo $results[0]->fTitle;?></a>
                        <span class="user-cate"><?php echo $results[0]->fCatSeq;?></span>
                        <span class="user-name"><a><?php echo $results[0]->fUserID;?></a></span>
                        <span class="user-date" data-utc-date="<?php echo date('M d, Y', strtotime($results[0]->fFirstRegDT));?>"><?php echo date('M d, Y', strtotime($results[0]->fFirstRegDT));?></span>
                    </div>
                    <span class="comment-count">[<?php echo $this->Qa_Model->get_total_reply($results[0]->fSeq);?>]</span>
                    <p><?php echo $this->Common_Model->truncate($results[0]->fContent, 150);?></p>
                </li>
                <li>
                    <div>
                        <a href="<?php echo $base_url . "qa/view/" . $results[1]->fSeq; ?>"><?php echo $results[1]->fTitle;?></a>
                        <span><?php echo $results[1]->fCatSeq;?></span>
                        <span><a><?php echo $results[1]->fUserID;?></a></span>
                        <span><?php echo date('M d, Y', strtotime($results[1]->fFirstRegDT));?></span>
                    </div>
                    <span class="comment-count">[<?php echo $this->Qa_Model->get_total_reply($results[1]->fSeq);?>]</span>
                </li>
                <li>
                    <div>
                        <a href="<?php echo $base_url . "qa/view/" . $results[2]->fSeq; ?>"><?php echo $results[2]->fTitle;?></a>
                        <span><?php echo $results[2]->fCatSeq;?></span>
                        <span><a><?php echo $results[2]->fUserID;?></a></span>
                        <span><?php echo date('M d, Y', strtotime($results[2]->fFirstRegDT));?></span>
                    </div>
                    <span class="comment-count">[<?php echo $this->Qa_Model->get_total_reply($results[2]->fSeq);?>]</span>
                </li>
                <li>
                    <div>
                        <a href="<?php echo $base_url . "qa/view/" . $results[3]->fSeq; ?>"><?php echo $results[3]->fTitle;?></a>
                        <span><?php echo $results[3]->fCatSeq;?></span>
                        <span><a><?php echo $results[3]->fUserID;?></a></span>
                        <span><?php echo date('M d, Y', strtotime($results[3]->fFirstRegDT));?></span>
                    </div>
                    <span class="comment-count">[<?php echo $this->Qa_Model->get_total_reply($results[3]->fSeq);?>]</span>
                </li>
            </ul>
            <div class="login-wrap">
                <!-- logout -->
                <!--
                <form action="post">
                    <fieldset>
                        <legend>heykorean login</legend>
                        <div class="login-input">
                            <h3>HeyKorean Login</h3>
                            <input type="text" placeholder="ID">
                            <input type="password">
                        </div>
                        <div class="login-save">
                            <label><input type="checkbox">Secured</label>
                            <label><input type="checkbox">SaveID</label>
                            <button type="submit">Log In</button>
                        </div>
                        <div class="signup-wrap">
                            <button type="submit">Sign up</button>
                            <a href="http://www.heykorean.com/HK_Member/en_id_pass_k.asp">Forgot id/password</a>
                        </div>
                    </fieldset>
                </form>
                 -->
                <!-- //logout -->
                <!-- login -->
                <strong class="my-nickname"><a href="http://www.heykorean.com/HK_Mypage/Myinfo/index.asp" target="_blank">heykorean</a></strong>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a>999+</a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt>Voted As</dt>
                        <dd style="width:65px;"><a>999+</a>  <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a href="<?php echo $base_url . "user/myMentor"; ?>" target="_blank" class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
        </div>
        <div class="most-wrap clearfix" style="width: 200px;float: right;">
            <h2 class="hidden">heykorean lnb</h2>
            <div class="thuydinh">
                <div class="most-keywords">
                    <h3>Most Popular Keywords</h3>
                    <ul class="son-list">
                        <?php foreach($most_popular_keywords as $keyword):?>
                            <li class="sonha"><span class="ranking"><?php echo $keyword->fCount;?></span><a><?php echo $keyword->fKeyword;?></a><span class="keyword-count"></span></li>
                        <?php endforeach;?>
                    </ul>
                    <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                    <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
                </div>
            </div>
        </div>
        <div id="container" class="mentor-main clearfix" style="clear: both">
            <!-- 中 -->
            <div class="mentoring-con advice">
                <div class="mentoring-board tab-style02">
                    <a class="more" title="help">Help</a>
                    <h3 class="hidden">mentoring board</h3>
                    <?php
                    $open = 'none';
                    $class = '';
                    if(!isset($_GET['fTab'])) {
                        $open = 'block';
                        $class = 'on';
                    } else {
                        if(isset($_GET['fTab']) && $_GET['fTab'] == 'open') {
                            $open = 'block';
                            $class = 'on';
                        }
                    }
                    ?>
                    <div class="tab-title clearfix">
                        <a href="<?php echo $base_url . "qa/best"; ?>" title="Most Popular" class="<?php echo $class; ?>">Most Popular</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab08" style="display:<?php echo $open; ?>">
                            <div class="box-subject clearfix">
                                <h3>US Life</h3>
                                <div class="sort-by">
                                    <dl>
                                        <dt><label for="sortBy">Sort by : </label></dt>
                                        <dd>
                                            <select id="sortByCat" name="sortByCategory">
                                                <option value="01">US Life</option>
                                                <option value="02";>Advice</option>
                                                <option value="03";>Regional Info.</option>
                                                <option value="05";>Sharing</option>
                                            </select>
                                        </dd>
                                        <dd>
                                            <select>
                                                <option>Within 1 week</option>
                                                <option>Within 1 month</option>
                                                <option>Within 3 months</option>
                                                <option>Within 6 months</option>
                                                <option>Within 1 year</option>
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <div id="loadingmessage" style="display: none;" role="status" aria-hidden="true">
                                    <div id="processMessage">
                                        <img src="http://image1.heykorean.com/v2/icon/waitanimation.gif" width="30" height="30" alt="waiting">
                                    </div>
                                </div>
                                <?php foreach($results as $data) { ?>
                                    <tr>
                                        <td>NJ</td>
                                        <td><a class="mentor-title" href="<?php echo $base_url . "qa/view/" . $data->fSeq; ?>"><?php echo $data->fTitle;?></a><span class="comment-num">[<?php echo $this->Qa_Model->get_total_reply($data->fSeq);?>]</span></td>
                                        <td class="mentor-titlecate"><span><?php echo $data->fCatSeq;?></span></td>
                                        <td><?php echo $data->fUserID;?></td>
                                        <td data-utc-date="<?php echo date('M d, Y', strtotime($data->fFirstRegDT));?>"><?php echo date('M d, Y', strtotime($data->fFirstRegDT));?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <?php if($links) : ?>
                                <div class="pagewrap" id="ajax_pagingsearc">
<!--                                    <a class="btn-first"><span class="hidden">처음목록</span></a>-->
<!--                                    <a class="btn-pre"><span class="hidden">이전목록</span></a>-->
                                    <?php echo $links; ?>
<!--                                    <a class="btn-next"><span class="hidden">다음목록</span></a>-->
<!--                                    <a class="btn-last"><span class="hidden">마지막목록</span></a>-->
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //中 -->
            <!-- 右 -->
            <div class="aside">
                <a class="btn-mainask" title="질문하기" href="<?php echo $base_url . "qa/write"; ?>">ASK</a>
                <div class="most answered-questions advice">
                    <h3>Best Recommended <span>Q&amp;A</span></h3>
                    <ol>
                        <?php foreach ($best_recommended_qa as $key => $qa):?>
                            <li><span class="num"><?php echo ($key + 1);?></span><a href="<?php echo $base_url . "qa/view/" . $qa->fSeq; ?>"><?php echo $qa->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($qa->fSeq);?>]</span></li>
                        <?php endforeach;?>
                    </ol>
                </div>
                <div class="most answered-questions advice">
                    <h3><span>US Life</span> Best Reply 5</h3>
                    <ol>
                        <?php foreach ($us_life_best_reply as $key => $best_reply):?>
                            <li><span class="num"><?php echo ($key + 1);?></span><a href="<?php echo $base_url . "qa/view/" . $best_reply->fSeq; ?>"><?php echo $best_reply->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($best_reply->fSeq);?>]</span></li>
                        <?php endforeach;?>
                    </ol>
                </div>
                <div class="sponsor-ad">
                    <h3>Sponsor <span>Ads</span></h3>
                    <a href="http://www.heykorean.com/HK_Club/HK_Club_main.asp?club_id=10000003" target="_blank" title="뉴욕유학생협회 KSANY 바로가기">
                        <img src="https://s3.amazonaws.com/img.ad.heykorean.com/Advertise/2015/12/04/2OrGtHjvC3Q.gif" alt="뉴욕유학생협회 KSANY 바로가기">
                    </a>
                    <div class="btn-sponsorad">
                        <a href="javascript:void(0)" class="style01" onclick="openPopup('http://www.heykorean.com/HK_Help/faqs_view.asp?section=7',980,670);">FAQ</a>
                        <a href="javascript:void(0)" class="style02" onclick="openPopup('http://www.heykorean.com/HK_Help/email.asp',980,670);">문의/제안</a>
                    </div>
                </div>
            </div>
            <!-- //右 -->
        </div>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"> </script>
<script type="text/javascript">
    $(function() {
        applyPagination();

        function applyPagination() {
            $("#ajax_pagingsearc a").click(function() {
                var url = $(this).attr("href");
                console.log(url);

                $.ajax({
                    type: "POST",
                    data: "ajax=1",
                    url: url,
                    success: function(msg) {
                        console.log(msg);
                        $("#tab08").html(msg);
                        applyPagination();
                    }
                });
                return false;
            });
        }
    });
</script>
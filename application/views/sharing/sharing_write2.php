<header>
		   <?php $this->view('layout/partial_header');?>
</header>
		<?php //echo validation_errors(); ?>
		<!-- Main content -->
		<section>
			<!-- <form name="write" method="post"> -->
			<?php echo form_open('sharing/write', array('onsubmit' => 'return validateFormWrite()', 'name' => 'formWrite')); ?>
			<div class="container">
				<!-- Left content -->
				<div class="main-content q-ask-wrap" id="main">
					<div class="select-location-wrap clearfix">
						<div class="input-group">
						  <span class="input-group-addon">Directory</span>
						  <select class="select-form select-location">
								<?php foreach($all_category as $key => $category) { ?>
									<option><?php echo $category->en_name;?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="title-wrap clearfix">
						<input class="title-box" type="text" name="fTitle" value="<?php echo set_value('fTitle'); ?>" onkeyup="textCounter(this,'counter',100);" id="..." placeholder="Q.Title">
            			<input disabled  maxlength="3" size="3" value="0/100" id="counter" class="input-counter">
          			</div>
          <div class="text-editor-wrap clearfix">
          	<textarea rows="10" name="fWrite"></textarea>
          </div>
          <div class="q-ask-tags tags-wrap clearfix">
          	<div class="input-group">
				<span class="input-group-addon">Tags</span>
						  <input type="text" class="form-control" data-role="tagsinput" value="visa, passport">
			</div>
          </div>
          <div class="q-ask-info-wrap clearfix">
          	<span class="row-title">Enter Location</span>
          	<div class="clearfix radio-wrap">
          		<label>
					      <input type="radio" name="iCheck" checked> Hanoi
					    </label>
					    <label>
					      <input type="radio" name="iCheck"> Danang
					    </label>
					    <label>
					      <input type="radio" name="iCheck"> Hanoi
					    </label>
					    <label>
					      <input type="radio" name="iCheck"> Danang
					    </label>
          	</div>
          	<div class="waterdrop-wrap clearfix"> 
          		<div class="clearfix">
          			<span class="title">Waterdrop</span>
        				<span class="help-icon"><img src="<?php echo $base_url ?>assets/mentor_new/images/question-icon-gray.png" alt="."></span>
        			</div>
	          	<div class="slider-container clearfix">
							  <div id="waterdrop-slider"></div>
							</div>
							<input type="number" id="amount" max="1000" min="1" class="amount quantity" />
							<span>to users who best reply</span>
						</div>
						<div class="checkbox-wrap clearfix">
	        		<label>
					      <input type="checkbox" checked> Automatic repost after 24 hours
					    </label>
					    <label>
					      <input type="checkbox"> Post as private
					    </label>
					    <label>
					      <input type="checkbox"> Notification
					    </label>
					  </div>
          </div>
          			<div class="button-group clearfix">
						<button class="btn btn-orange btn-save">SAVE</button>
						<button class="btn btn-gray btn-discard">DISCARD</button>
						<button class="btn btn-blue btn-post" type="submit" name="submit">POST</button>
					</div>
					<!-- Float Tutorial -->
					<div class="tutorial-wrap clearfix">
						<img src="<?php echo $base_url ?>assets/mentor_new/images/lamp-icon.png" alt="">
						<span>Ralghiwr</span>
						<p>Regarding long questions, for the sake of simplicity please divide your question into paragraphs!</p>
						<span>Ralghiwr</span>
						<p>When you have multiple questions, please <strong>sort them out and present them seperately</strong></p>
					</div>
					<!-- End Float Tutorial -->
				</div>
				<!-- End Left content -->
				<!-- Right content -->
				<aside>
					
				</aside> 
			</form>
		</section>
		<footer>
    		<?php $this->view('layout/partial_footer');?>
		</footer>
<!-- End Main content -->
<?php $this->view('layout/partial_js');?>
<?php $this->view('layout/partial_footer_js');?>
  </body>
</html>
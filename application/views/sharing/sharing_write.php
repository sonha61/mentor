<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({
                        pageLanguage: 'en',
                        includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW',
                        layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                        multilanguagePage: true
                    }, 'google_translate_element');
                }
            </script>
            <script type="text/javascript"
                    src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>
<div id="wrap" class="clearfix">
        <div id="header" class="clearfix">
            <div class="title-wrapper">
                <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
<!--                <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>-->
                <a class="service-title" title="mentoring" href="/">Mentoring</a>
                <span><a href="/en-us" class="p-area-title"></a></span>
            </div>
            <div class="search-box">
<!--                <form action="post">-->
<!--                    <fieldset>-->
<!--                        <legend>search</legend>-->
<!--                        <input type="search" id="containerSearch" placeholder="Find an answer">-->
<!--                        <label for="containerSearch"><button type="submit">Search</button></label>-->
<!--                    </fieldset>-->
<!--                </form>-->
            </div>
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
        </div>
        <div class="aside">
        </div>
        <div id="container" class="sub">
            <div class="mentoring-con advice">
                <div class="mentoring-board tab-style02">
                    <a class="more">more</a>
                    <h3 class="hidden">mentoring ask</h3>
                    <div class="tab-title clearfix">
                        <a href="#tab01" title="Sharing" class="on">Sharing</a>
                        <a href="<?php echo $base_url . "qa/write"; ?>" title="Essay Question">Essay Question</a>
                    </div>
                    <?php if(validation_errors() != false) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php } ?>
                    <?php echo form_open('sharing/write', array('onsubmit' => 'return validateFormWrite();', 'name' => 'formWrite')); ?>
                    <div class="tab-con">
                        <div id="tab01">
                            <div class="write-title">
                                <span>Q Ask</span>
								<span class="tooltip-point">
									<input type="text" style="width:785px" name="fTitle" value="<?php echo set_value('fTitle'); ?>">
									<div class="tooltip-wrap">
                                        <p>You are allowed to ask a simple question within the title alone. However,</p>
                                        <p><strong>if you want an accurate answer, ask a concrete and accurate question!</strong></p>
                                    </div>
								</span>
                                <div class="chars clearfix">
                                    <span>(<strong>0</strong> / 100 chars)</span>

                                </div>
                            </div>
						<span class="tooltip-point">
<!--							<img src="./images/write_editor.jpg" style="display:block;width:100%">-->
                            <?php echo $editor;?>
                            <textarea name="fWrite" id="fWrite"><?php echo set_value('fWrite'); ?></textarea>
							<div class="tooltip-wrap">
                                <p class="caution">Guidelines of Posting Content</p>
                                <p>Regarding long questions, for the sakeof simplicity <strong>please divide your question into paragraphs!</strong></p>
                                <p>When you have multiple questions, please <strong>sort them out and present them seperately</strong> (e.g. Question 1, Question 2, Question 3....etc.)!</p>
                                <p>Please be aware that users who post <strong>jokes, slander, advertisements etc.</strong> can, without warning, <strong>have their posts removed or have their access to services revoked</strong> by the discretion of the administrator, so be careful!</p>
                            </div>
						</span>
                            <div class="select-write">
                                <div class="select-loccation tooltip-point">
                                    <h4>Enter location</h4>
                                    <div class="select_location">
                                        <ul class="step03_select" data-notify="Choose an area."></ul>
                                    </div>
                                    <div class="tooltip-wrap">
                                        <p class="caution"> Please select the region from responses from.</p>
                                        <p>By default, the email you entered upon registration will be used. If you would like to change it now, please login and change it in your account details.</p>
                                    </div>
                                </div>
                                <div class="select-directory tooltip-point">
                                    <h4>Directory</h4>
                                    <div class="directory-con">
                                        <div>
                                            <label><input type="radio" name="directory" value="01" onclick="loadDirectory('01')" checked><span>US Life</span></label>
                                            <label><input type="radio" name="directory" value="02" onclick="loadDirectory('02')"><span>Advice</span></label>
                                            <label><input type="radio" name="directory" value="03" onclick="loadDirectory('03')"><span>Regional Info.</span></label>
                                        </div>
                                        <div class="select-category clearfix">
                                            <ul class="list_direct" id="lstCategory2">
                                               <?php foreach ($all_category as $category) : ?>
                                                   <li title="Immigration/Visa/Passport" onclick="Category_Click(0, this, '2', '<?php echo $category->code;?>');LoadCategory('3', '<?php echo $category->code;?>')"><?php echo $category->en_name;?></li>
                                               <?php endforeach;?>
                                            </ul>
                                            <ul class="list_direct" id="lstCategory3">
                                            </ul>
                                            <ul class="list_direct" id="lstCategory4">
                                            </ul>
                                            <ul class="list_direct" id="lstCategory5">
                                            </ul>
                                            <input type="hidden" name="categoryName">
<!--                                            <input type="hidden" name="categoryName2">-->
<!--                                            <input type="hidden" name="categoryName3">-->
<!--                                            <input type="hidden" name="categoryName4">-->
<!--                                            <input type="hidden" name="categoryName5">-->
                                        </div>
                                    </div>
                                    <div class="tooltip-wrap">
                                        <p class="caution"> Please specify a directory!</p>
                                        <p>So that we can properly distribute even more information with other people, it is necessary to put contents in the correct category so that they are easier to find.</p>
                                    </div>
                                </div>
                                <div class="offer-waterdrops tooltip-point">
                                    <h4>Offer Waterdrops which you would like to receive</h4>
                                    <div>
                                        <input type="number" name="fMB" value="<?php echo set_value('fMB')?>"> to users who best reply.
                                    </div>
                                    <div class="tooltip-wrap">
                                        <p class="caution">Offer Waterdrops</p>
                                        <p>The amount of Waterdrops that you put on a question is the amount that the answerer will get.. The more Waterdrops you offer on a question, the faster you will receive responses.</p>
                                        <p>The posting will be listed as a new post until you receive the answer you desire. After checking automatic reposting, select the frequency of reposting, and your post will be renewed a maximum of 3 times. If within 14 days no answer is selected, the question will be placed in the "In voting" channel</p>
                                    </div>
                                </div>
                                <div class="automatically-repost">
                                    <h4><label><input type="checkbox"> Automatically repost</label></h4>
                                    <div>
                                        <select>
                                            <option>24</option>
                                            <option>48</option>
                                            <option>72</option>
                                        </select>
                                        Hourly
                                    </div>
                                </div>
                                <div class="select-etc clearfix">
                                    <div class="private tooltip-point">
                                        <label><input type="checkbox"> <strong>-Private-</strong></label>
                                        <div class="tooltip-wrap">
                                            <p class="caution">-Private-</p>
                                            <p>If -Private- is selected, anyone reading your posts will see 'Private' listed in place of your ID, and will cost you 20 Waterdrops.</p>
                                        </div>
                                    </div>
                                    <div class="alert tooltip-point">
                                        <label><strong>Alert replies</strong> <input type="checkbox"> Letter</label>
                                        <div class="tooltip-wrap">
                                            <p class="caution">Response Alert</p>
                                            <p>If you select email or message, whenever your question gets a response, you will receive an alert. To change your receiving email, press the 'Change' button and change the email listed within your account details.</p>
                                        </div>
                                    </div>
                                    <div class="qna">
                                        <label><strong>1:1 Q&amp;A</strong></label>
                                    </div>
                                </div>
                            </div>
                            <p>In accordance with the User Agreement and related laws, users can be penalized for posting content that violates copyrights or defames another's character.</p>
                            <p class="caution">Please enter an amount of Waterdrops between 10 and 1000.</p>
                            <div class="btnwrite-wrap">
<!--                                <input type="submit" class="post" name="submit" value="Create" />-->
                                <button type="submit" class="post">Post</button>
<!--                                <button type="button">Save now</button>-->
                                <button type="reset">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({
                        pageLanguage: 'en',
                        includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW',
                        layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                        multilanguagePage: true
                    }, 'google_translate_element');
                }
            </script>
            <script type="text/javascript"
                    src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header" class="clearfix">
            <div class="title-wrapper">
                <h1><a href="http://www.heykorean.com/" title="heykorean"><img
                            src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
<!--                <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>-->
                <a class="service-title" title="mentoring" href="/">Mentoring</a>
                <span><a href="/en-us" class="p-area-title"></a></span>
            </div>
            <div class="search-box">
                <form action="post">
                    <fieldset>
                        <legend>search</legend>
                        <input type="search" id="containerSearch" placeholder="Find an answer">
                        <label for="containerSearch">
                            <button type="submit">Search</button>
                        </label>
                    </fieldset>
                </form>
            </div>
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
            <div class="location" style="width: 76%;">
<!--                <a>Home</a> &gt;-->
<!--                --><?php //echo $this->Common_Model->breadcrumbs($sharing_info->fCatSeq);?>
                <?php echo $this->Common_Model->createPath($sharing_info->fCatSeq);?>
            </div>
            <div class="newly-wrap">
                <span class="announcement">공지</span>
                <p><a href="http://www.heykorean.com/HK_Service/Event/board.asp?mode=view&idx=255&page=1&SearchType=&SearchKeyword=">멘토링을 개편 하였습니다. 멘토링을 개편 하였습니다.</a></p>
            </div>
        </div>
        <div class="aside">
            <div class="login-wrap">
                <!-- logout -->
                <!--
                <form action="post">
                    <fieldset>
                        <legend>heykorean login</legend>
                        <div class="login-input">
                            <h3>HeyKorean Login</h3>
                            <input type="text" placeholder="ID">
                            <input type="password">
                        </div>
                        <div class="login-save">
                            <label><input type="checkbox">Secured</label>
                            <label><input type="checkbox">SaveID</label>
                            <button type="submit">Log In</button>
                        </div>
                        <div class="signup-wrap">
                            <button type="submit">Sign up</button>
                            <a href="http://www.heykorean.com/HK_Member/en_id_pass_k.asp">Forgot id/password</a>
                        </div>
                    </fieldset>
                </form>
                 -->
                <!-- //logout -->
                <!-- login -->
                <strong class="my-nickname"><a href="http://www.heykorean.com/HK_Mypage/Myinfo/index.asp" target="_blank">heykorean</a></strong>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a>999+</a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt style="width:58px;">Voted As</dt>
                        <dd style="width:59px;"><a>999+</a> <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a href="<?php echo $base_url . "user/myMentor"; ?>" target="_blank" class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
            <a class="btn-mainask" title="질문하기" href="<?php echo $base_url . "qa/write"; ?>">ASK</a>
            <div class="most answered-questions">
                <h3>Most Answered <span>Questions</span></h3>
                <ol>
                    <?php foreach ($most_answered as $key => $value):?>
                        <li><span class="num"><?php echo $key + 1;?></span><a href="<?php echo $base_url . "qa/view/" . $value->fBSeq; ?>"><?php echo $value->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($value->fBSeq);?>]</span></li>
                    <?php endforeach;?>
                </ol>
                <a class="more" href="<?php echo $base_url . "qa/index/1/?fTab=resolved";?>">more</a>
            </div>
            <div class="most recommended-sharing">
                <h3>Most Recommended <span>Sharing</span></h3>
                <ul>
                    <?php foreach ($most_recommended_sharing as $sharing):?>
                        <li><a href="<?php echo $base_url . "qa/view/" . $sharing->fSeq; ?>"><?php echo $sharing->fTitle;?></a></li>
                    <?php endforeach;?>
                </ul>
                <a class="more" href="<?php echo $base_url . "sharing";?>">more</a>
            </div>
            <div class="most">
                <h3>Today's best <span>Advice</span></h3>
                <ul>
                    <?php foreach ($today_best_advice as $advice):?>
                        <li>
                            <a href="<?php echo $base_url . "qa/view/" . $advice->fSeq; ?>"><?php echo $advice->fTitle;?></a>
                            <span class="comment">[<?php echo $this->Qa_Model->get_total_reply($advice->fSeq);?>]</span>
                        </li>
                    <?php endforeach;?>
                </ul>
                <a class="more" href="<?php echo $base_url . "qa/category/01";?>">more</a>
            </div>
            <div class="most">
                <h3>Most Popular Local <span>Questions</span></h3>
                <ul>
                    <?php foreach ($most_popular_local as $popular):?>
                        <li><a href="<?php echo $base_url . "qa/view/" . $popular->fBSeq; ?>"><?php echo $popular->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($popular->fBSeq);?>]</span></li>
                    <?php endforeach;?>
                </ul>
                <a class="more" href="<?php echo $base_url . "qa/category/01";?>">more</a>
            </div>
        </div>
        <div id="container" class="sub">
            <div class="qa-box sharing-box">
                <h3 class="hidden">질문글</h3>
                <div class="qa-title clearfix">
                    <span>Sharing</span>
                    <div>
                        <p><?php echo $sharing_info->fTitle;?></p>
                        <span class="writer"><a><?php echo $sharing_info->fUserID;?></a></span>
                        <!-- <span class="writer">-Private-</span> --><!-- 비공개 -->
                        <span class="date"><?php echo date('M d, Y h:i A', strtotime($sharing_info->fFirstRegDT));?></span>
                        <span class="area">[NY]</span>
                        <span class="hits">Hits <strong><?php echo $sharing_info->fHit;?></strong></span>
                    </div>
                </div>
<!--                <a class="btn-helpful">-->
<!--                    <span>Very Helpful</span>-->
<!--                    <span class="count">10</span>-->
<!--                </a>-->
                <a class="btn-helpful" onclick="addVote(<?php echo $sharing_info->fSeq;?>,'add','sharing')">
                    <span>Very Helpful</span>
                    <span class="count" id="fRecommend"><?php echo $sharing_info->fRecommend;?></span>
                </a>
                <div class="qa-con">
                    <?php echo $sharing_info->fContent;?>
                </div>
                <div class="report-source clearfix">
                    <div><span>Source</span><p>내가 자주 가는 레스토랑</p></div>
                    <a onclick="addVote(<?php echo $sharing_info->fSeq;?>,'report', 'sharing')">Report <span id="fNotRecommend"><?php echo $sharing_info->fNotRecommend;?></span>
                    </a>
                </div>


                <div class="profile-box clearfix">
                    <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                    <div>
                        <strong><?php echo $sharing_info->fUserID;?></strong>
                        <div class="profile-level"><span class="level level01"></span>Skull</div>
                        <div class="about-profile clearfix">
                            <dl class="progress">
                                <dt>Voted</dt>
                                <dd style="color:#ff4c0d">999+</dd>
                                <dt>Rate</dt>
                                <dd class="odd" style="color:#ff4c0d">100%</dd>
                                <dt>Ask </dt>
                                <dd>-999+</dd>
                                <dt>Resolved</dt>
                                <dd class="odd">100%</dd>
                            </dl>
                            <dl class="represent ">
                                <dt>[Rep. Answer]</dt>
                                <dd>This user has not specified anything.</dd>
                                <dt>[Rep. Knowledge]</dt>
                                <dd>This user has not specified anything.</dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="comment-box">
                    <form method="post" name="writeReplySharing" onsubmit="return validateFormReplySharing();">
                        <fieldset>
                            <legend>write comment</legend>
                            <div class="select-box">
                                <label><input type="radio" name="comment01">None selected</label>
                                <label><input type="radio" name="comment01">Other comments</label>
                                <label><input type="radio" name="comment01">Additional information</label>
                            </div>
                            <div class="write-box">
                                <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                <textarea id="fContent" rows="10" cols="20" name="fContent"></textarea>
                                <button type="submit" name="submit">Comment</button>
                            </div>
                            <label><input type="checkbox" name="fHideIDReply">-Private-</label>
                        </fieldset>
                    </form>
                </div>

                <div class="reply-wrap">
                    <?php foreach ($all_reply as $key => $reply):?>
                    <div class="reply-box clearfix">
                        <div class="reply-num">
                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                            <span>Reply<?php echo ($key != 0) ? $key+1 : '';?></span>
                        </div>
                        <div class="reply-con">
                            <div>
                                    <a><strong><?php echo $reply->fUserID;?></strong></a>
                                <span><?php echo date('M d, Y h:i A', strtotime($reply->fRegDT));?></span>
                            </div>
                            <p><?php echo $reply->fContent;?></p>
                            <span class="report" onclick="addVoteReply(<?php echo $reply->fSeq;?>,'report', <?php echo $sharing_info->fSeq;?>,'sharing')"><a>Report</a><span id="fNotReplyRecommend<?php echo $reply->fSeq;?>"><?php echo ($reply->fNotRecommend != null || $reply->fNotRecommend != 0) ? $reply->fNotRecommend : 0;?></span></span>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>

            <div class="btn-mentorbox">
                <a class="go-save">Save to my watchlist<img src="<?php echo $base_url ?>assets/images/bg_cabinet.gif"></a>
                <a class="go-list">List<img src="<?php echo $base_url ?>assets/images/bg_golist.gif"></a>
            </div>
            <div class="otherlist-wrap">
                <div class="other-list">
                    <div class="otherlist-title clearfix">
                        <h3>Other Fields in this Category</h3>
                        <a class="more1" href="<?php echo $base_url . "sharing?fCatSeq=" . $catId; ?>">more</a>
                    </div>
                    <ul>
                        <?php foreach($best_sharing as $sharing) { ?>
                            <li>
                            <span class="list-title">
                                <a href="<?php echo $base_url . "sharing/view/" . $sharing->fSeq; ?>"><?php echo $sharing->title;?></a>
                                <span>[<?php echo $this->Sharing_Model->get_total_reply($sharing->fSeq);?>]</span>
                            </span>
                                <span class="list-area"><?php echo date('M d, Y', strtotime($sharing->fFirstRegDT));?></span>
                                <span class="list-date">U.S.A</span>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
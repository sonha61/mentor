<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
<!--                    <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>-->
                    <a class="service-title" title="mentoring" href="/">Mentoring</a>
                    <span><a class="p-area-title"></a></span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
							0<span>Bxs</span>
							0<span>Bts</span>
							0<span>Dps</span>
						</a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
            <div class="location">
                <a href="<?php echo $base_url;?>">Home</a> &gt;
                <a href="<?php echo $base_url.'sharing';?>"><strong>Sharing List</strong></a>
            </div>
            <div class="newly-wrap">
                <span class="announcement">공지</span>
                <p><a href="http://www.heykorean.com/HK_Service/Event/board.asp?mode=view&idx=255&page=1&SearchType=&SearchKeyword=">멘토링을 개편 하였습니다. 멘토링을 개편 하였습니다.</a></p>
            </div>
        </div>

        <div class="top-con clearfix">
            <div class="category-wrap">
                <div class="category-title">
                    Sharing
                </div>
                <div class="newly-wrap">
                    <span class="newly">Posted today : </span>
                    <span>999+</span>
                    <span class="newly">Replied today : </span>
                    <span>999+</span>
                </div>
                <div class="category-con">
                    <div class="category-con02 clearfix">
                        <div class="sharing-row">
                        <?php
                        $count = count($category);
                        $numItemsPerRow = ceil($count / 2);
                        //we need this in case of 2-1-1 for example, otherwise you get 2-2
                        $numItemsOffsetFix = $count % 2 == 1;
                        $index  = 0;
                        foreach($category as $term){
                            if ($index > 0 and $index % 4 == 0) {
                                echo '</div><div class="sharing-row">';
                                if ($numItemsOffsetFix) {
                                    $numItemsPerRow--;
                                    $numItemsOffsetFix = false;
                                }
                            }
                            echo '<div><h4><a href="'.$base_url . "sharing?fCatSeq=" . $term['code'].'">'.$term['name'].'</a></h4>';
                            echo '<ul>';
                            foreach($term['sub_category'] as $key => $value) {
                                echo '<li><a href="'.$base_url . "sharing?fCatSeq=" . $value->code.'">'.$value->name.'</a></li>';
                            }
                            echo '</ul>';
                            echo '</div>';
                            $index++;
                        }?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-wrap">
                <!-- logout -->
                <!--
                <form action="post">
                    <fieldset>
                        <legend>heykorean login</legend>
                        <div class="login-input">
                            <h3>HeyKorean Login</h3>
                            <input type="text" placeholder="ID">
                            <input type="password">
                        </div>
                        <div class="login-save">
                            <label><input type="checkbox">Secured</label>
                            <label><input type="checkbox">SaveID</label>
                            <button type="submit">Log In</button>
                        </div>
                        <div class="signup-wrap">
                            <button type="submit">Sign up</button>
                            <a href="http://www.heykorean.com/HK_Member/en_id_pass_k.asp">Forgot id/password</a>
                        </div>
                    </fieldset>
                </form>
                 -->
                <!-- //logout -->
                <!-- login -->
                <strong class="my-nickname"><a href="http://www.heykorean.com/HK_Mypage/Myinfo/index.asp" target="_blank">heykorean</a></strong>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a>999+</a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt>Voted As</dt>
                        <dd style="width:65px;"><a>999+</a>  <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a href="<?php echo $base_url . "user/myMentor"; ?>" target="_blank" class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
        </div>
        <div class="most-wrap clearfix" style="width: 200px;float: right;">
            <h2 class="hidden">heykorean lnb</h2>
            <div class="thuydinh">
                <div class="most-keywords">
                    <h3>Most Popular Keywords</h3>
                    <ul class="rank-list">
                        <?php foreach($most_popular_keywords as $keyword):?>
                            <li class="sonha"><span class="ranking"><?php echo $keyword->fCount;?></span><a><?php echo $keyword->fKeyword;?></a><span class="keyword-count"></span></li>
                        <?php endforeach;?>
                    </ul>
                    <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                    <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
                </div>
            </div>
        </div>
        <div id="container" class="mentor-main clearfix" style="clear: both;">
            <!-- 中 -->
            <div class="mentoring-con advice">
                <div class="mentoring-board tab-style02">
                    <a class="more" title="help">Help</a>
                    <h3 class="hidden">mentoring board</h3>
                    <?php
                    $open = 'none';
                    $class = '';
                    if(!isset($_GET['fTab'])) {
                        $open = 'block';
                        $class = 'on';
                    } else {
                        if(isset($_GET['fTab']) && $_GET['fTab'] == 'new') {
                            $open = 'block';
                            $class = 'on';
                        }
                    }
                    ?>
                    <div class="tab-title clearfix">
                        <a href="<?php echo $base_url . "sharing/index/1?fTab=new"; ?>" title="New"  class="<?php echo $class; ?>">New</a>
                        <a href="<?php echo $base_url . "sharing/index/1?fTab=besthit"; ?>" title="Best Hit" class="<?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'besthit') ? 'on' : ''; ?>">Best Hit</a>
                        <a href="<?php echo $base_url . "sharing/index/1?fTab=bestreply"; ?>" title="Best Reply" class="<?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'bestreply') ? 'on' : ''; ?>">Best Reply</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab08" style="display:<?php echo $open; ?>">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($new as $item) :?>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title" href="<?php echo $base_url . "sharing/view/" . $item->fSeq; ?>"><?php echo $item->fTitle;?></a><span class="comment-num">[<?php echo $this->Sharing_Model->get_total_reply($item->fSeq);?>]</span></td>
                                    <td class="mentor-titlecate"><span><?php echo $item->fCatSeq;?></span></td>
                                    <td><?php echo $item->fUserID;?></td>
                                    <td><?php echo date('M d, Y', strtotime($item->fRegDT));?></td>
                                </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab09" style="display: <?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'besthit') ? 'block' : 'none'; ?>">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($besthit as $item) :?>
                                    <tr>
                                        <td>OH</td>
                                        <td><a class="mentor-title" href="<?php echo $base_url . "sharing/view/" . $item->fSeq; ?>"><?php echo $item->fTitle;?></a><span class="comment-num">[<?php echo $this->Sharing_Model->get_total_reply($item->fSeq);?>]</span></td>
                                        <td class="mentor-titlecate"><span><?php echo $item->fCatSeq;?></span></td>
                                        <td><?php echo $item->fUserID;?></td>
                                        <td><?php echo date('M d, Y', strtotime($item->fRegDT));?></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab10" style="display: <?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'bestreply') ? 'block' : 'none'; ?>">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($bestreply as $item) :?>
                                    <tr>
                                        <td>OH</td>
                                        <td><a class="mentor-title" href="<?php echo $base_url . "sharing/view/" . $item->fSeq; ?>"><?php echo $item->fTitle;?></a><span class="comment-num">[<?php echo $this->Sharing_Model->get_total_reply($item->fSeq);?>]</span></td>
                                        <td class="mentor-titlecate"><span><?php echo $item->fCatSeq;?></span></td>
                                        <td><?php echo $item->fUserID;?></td>
                                        <td><?php echo date('M d, Y', strtotime($item->fRegDT));?></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if($links) : ?>
                        <div class="pagewrap">
<!--                            <a class="btn-first"><span class="hidden">처음목록</span></a>-->
<!--                            <a class="btn-pre"><span class="hidden">이전목록</span></a>-->
                            <?php echo $links; ?>
<!--                            <a class="btn-next"><span class="hidden">다음목록</span></a>-->
<!--                            <a class="btn-last"><span class="hidden">마지막목록</span></a>-->
                        </div>
                    <?php endif;?>
                    <div class="mentorlist-search">
                        <form action="post">
                            <fieldset>
                                <legend>search mentoring list</legend>
                                <select>
                                    <option>Title</option>
                                    <option>Content</option>
                                    <option>Tag</option>
                                    <option>ID</option>
                                </select>
                                <input type="search" id="metoringlistSearch" style="width:184px;">
                                <label for="metoringlistSearch"><button type="button">Search</button></label>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <!-- //中 -->
            <!-- 右 -->
            <div class="aside">
                <a class="btn-mainask" title="질문하기" href="<?php echo $base_url . "sharing/write"; ?>">ASK</a>
                <div class="most answered-questions advice">
                    <h3>Best Recommended <span>Q&amp;A</span></h3>
                    <ol>
                        <?php foreach ($most_answered as $key => $value):?>
                            <li><span class="num"><?php echo $key + 1;?></span><a href="<?php echo $base_url . "sharing/view/" . $value->fBSeq; ?>"><?php echo $value->title;?></a><span class="comment">[<?php echo $this->Sharing_Model->get_total_reply($value->fBSeq);?>]</span></li>
                        <?php endforeach;?>
                    </ol>
                </div>
                <div class="most answered-questions advice">
                    <h3><span>US Life</span> Best Reply 5</h3>
                    <ol>
                        <?php foreach ($best_sharing as $key => $value):?>
                            <li><span class="num"><?php echo $key + 1;?></span><a href="<?php echo $base_url . "sharing/view/" . $value->fBSeq; ?>"><?php echo $value->title;?></a><span class="comment">[<?php echo $this->Sharing_Model->get_total_reply($value->fBSeq);?>]</span></li>
                        <?php endforeach;?>
                    </ol>
                </div>
                <div class="sponsor-ad">
                    <h3>Sponsor <span>Ads</span></h3>
<!--                    <a href="http://www.heykorean.com/HK_Club/HK_Club_main.asp?club_id=10000003" target="_blank" title="뉴욕유학생협회 KSANY 바로가기">-->
<!--                        <img src="https://s3.amazonaws.com/img.ad.heykorean.com/Advertise/2015/12/04/2OrGtHjvC3Q.gif" alt="뉴욕유학생협회 KSANY 바로가기">-->
<!--                    </a>-->
                    <div data-ad-code="mk01"></div>
                    <div class="btn-sponsorad">
                        <a href="javascript:void(0)" class="style01" onclick="openPopup('http://www.heykorean.com/HK_Help/faqs_view.asp?section=7',980,670);">FAQ</a>
                        <a href="javascript:void(0)" class="style02" onclick="openPopup('http://www.heykorean.com/HK_Help/email.asp',980,670);">문의/제안</a>
                    </div>
                </div>
            </div>
            <!-- //右 -->
        </div>
    </div>
<?php
$kaim_path = $base_url."/assets/mentor_new"; // common.php 의 상대 경로
?>
    <header>
    <?php $this->view('layout/partial_header');?>
    </header>
        <!-- Category list -->
        <section>
            <div class="container">
                <div class="category-list-wrap clearfix">
                    <header>
                        <h3 class="best-category">
                            Best Category
                            <a class="see-all" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">See all
                            </a>
                        </h3>
                    </header>
                    <ul class="category-list main-category col-4">
                        <li><a href="">Identification</a></li>
                        <li><a href="">Living/Eenstrangmernt</a></li>
                        <li><a href="" class="active">Identity Theft/Fraud</a></li>
                        <li><a href="">School & Academy Study</a></li>
                    </ul>
                </div>
                <div class="collapse" id="collapseExample">
                    <div class="sub-category-wrap panel panel-100">
                        <h3 class="sub-category-header">Identity Theft/Fraud</h3>
                        <ul class="sub-category-list col-4">
                            <li><a href="">All</a></li>
                            <li><a href="">School</a></li>
                            <li><a href="">Shopping</a></li>
                            <li><a href="">Entertainment</a></li>
                            <li><a href="">Travelling</a></li>
                            <li><a href="">Low Society & Culture</a></li>
                            <li><a href="">Immigration</a></li>
                            <li><a href="">Study Abroad</a></li>
                            <li><a href="">Tips & Produce</a></li>
                            <li><a href="">Perfomance</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </section>
        <!-- End Category list -->

        <!-- Main content -->
        <section>
            <div class="container">
                <!-- Left content -->
                <div class="main-content" id="main">
                  <!-- Nav tabs -->
                  <ul class="list-tab" role="tablist">
                    <li role="presentation" class="active"><a href="#answers" aria-controls="answers" role="tab" data-toggle="tab" class="tab-answers"><span class="icon"></span>Please Answers</a></li>
                    <li role="presentation"><a href="#adopt" aria-controls="adopt" role="tab" data-toggle="tab" class="tab-adopt"><span class="icon"></span>Please Adopt</a></li>
                    <li role="presentation"><a href="#settle-question" aria-controls="messages" role="tab" data-toggle="tab" class="tab-settle"><span class="icon"></span>Settle Question</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="answers">
                        <!-- Header: Toolbar -->
                            <h3>Sharing</h3>
                            <div class="toolbar-wrap clearfix">
                                <div class="style-group">
                                    <button class="btn btn-collapse-info"></button>
                                    <button class="btn btn-expand-info active"></button>
                                </div>
                                <div class="search-wrap">
                                    <div class="select-btn btn-group">
                                      <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        option <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu">
                                        <li><a href="#">Option 1</a></li>
                                        <li><a href="#">Option 2</a></li>
                                        <li><a href="#">Option 3</a></li>
                                        <li><a href="#">Option 4</a></li>
                                      </ul>
                                    </div>
                                    <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                  <span class="input-group-btn">
                                    <button class="btn btn-search" type="button"></button>
                                  </span>
                                </div>
                                    <a href="#" class="help-icon"></a>
                                </div>
                            </div>
                            <!-- End Header: Toolbar -->
                            <!-- List item -->
                            <div class="list-content-wrap expand-info clearfix" id="main-list">
                            <?php foreach($result as $key => $sharing) { ?>
                                <div class="item clearfix">
                                    <div class="waterdrop">
                                        <img src="<?php echo $base_url ?>assets/mentor_new/images/<?php echo $this->Common_Model->getWaterColor($sharing->fMB);?>" alt="water-drop">
                                        <span class="point"><?php echo $sharing->fMB;?></span>
                                    </div>
                                    <div class="item-context">
                                        <div class="header">
                                            <a href="#" class="category"><?php echo $this->Category_Model->get_category_name_by_code($sharing->fCatSeq);?></a>
                                            <a href="<?php echo $base_url . "sharing/view/" . $sharing->fSeq; ?>" class="title"><?php echo $sharing->fTitle;?></a>
                                            <span class="comment-counter"><?php echo $this->Qa_Model->get_total_reply($sharing->fSeq);?></span>
                                            <span class="location">US</span>
                                            <span class="like">370</span>
                                            <span class="view">150</span>
                                        </div>
                                        <p>
                                            저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
                                            니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
                                            졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
                                        </p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <!-- <button class="btn btn-more">더 보기 <img src="<?php echo $base_url ?>assets/mentor_new/images/arrow-down.png"></button> -->
                            <div class="paging-wrap clearfix">
                                <ul>
                                    <li class="paging"><a href="#" class="first-page page-button"></a></li>
                                    <li class="paging"><a href="#" class="prev-page page-button"></a></li>
                                    <li><a class="active" href="">1</a></li>
                                    <li><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href="">4</a></li>
                                    <li><a href="">5</a></li>
                                    <li><a href="">6</a></li>
                                    <li><a href="">7</a></li>
                                    <li><a href="">8</a></li>
                                    <li><a href="">9</a></li>
                                    <li><a href="">10</a></li>
                                    <li class="paging"><a href="#" class="next-page page-button"></a></li>
                                    <li class="paging"><a href="#" class="last-page page-button"></a></li>
                                </ul>
                            </div>
                            <!-- End List item -->
                    </div>
                    <div role="tabpanel" class="tab-pane" id="adopt">Please Adopt</div>
                    <div role="tabpanel" class="tab-pane" id="settle-question">Settle Question</div>
                  </div>
                </div>
                <!-- End Left content -->

                <!-- Right content -->
                <aside>
                    <div class="button-wrap clearfix">
                        <button class="btn btn-orange btn-ask"><img src="<?php echo $base_url ?>assets/mentor_new/images/question-icon-w.png" alt="..."> ask</button>
                        <button class="btn btn-blue btn-share"><img src="<?php echo $base_url ?>assets/mentor_new/images/comment-icon-w.png" alt="..."> share</button>
                    </div>
                    <!-- Keyword block -->
                    <div class="keyword-wrap aside-block clearfix">
                        <div class="header">
                            <h3>best keyword</h3>
                            <div class="control-block">
                                <a class="arrow-down-icon" role="button"></a>
                                <a class="arrow-up-icon" role="button"></a>
                            </div>                      
                        </div>
                        <div class="keyword-list clearfix">
                            <ol>
                                <li>
                                    <a href="">
                                        <span class="pos">1</span>
                                        마사지
                                        <span class="quantity">110</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="pos">2</span>
                                        플러싱
                                        <span class="quantity">110</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="pos">3</span>
                                        보톡스
                                        <span class="quantity">110</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="pos">4</span>
                                        뉴욕
                                        <span class="quantity">110</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="pos">5</span>
                                        뉴저지 영주권
                                        <span class="quantity">110</span>
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- End Keyword block -->

                    <!-- Ads block -->
                    <div class="ads-img clearfix">
                        <div data-ad-code="mt01"></div>
                    </div>
                    <!-- End Ads block -->

                    <!-- Leaderboard block -->
                    <div class="leader-board-wrap aside-block clearfix">
                        <div>
                          <!-- Nav tabs -->
                          <ul class="leader-board-header" role="tablist">
                            <li role="presentation" class="active"><a href="#best-qa" aria-controls="qa" role="tab" data-toggle="tab"><img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon-b.png"> Best Q & A</a></li>
                            <li role="presentation"><a href="#best-reply" aria-controls="reply" role="tab" data-toggle="tab">Best Reply</a></li>
                          </ul>
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active leader-board-content" id="best-qa">
                                <ol>
                                    <?php foreach ($best_recommended_qa as $key => $qa) { ?>
                                        <li>
                                        <a href="#" class="clearfix">
                                            <span class="number"><?php echo $key + 1;?></span>
                                            <span class="title"><?php echo $qa->fTitle;?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ol>
                                <a class="more" href="#">more</a>
                            </div>
                            <div role="tabpanel" class="tab-pane leader-board-content" id="best-reply">
                                <ol>
                                    <?php foreach ($us_life_best_reply as $key => $qa) { ?>
                                        <li>
                                        <a href="#" class="clearfix">
                                            <span class="number"><?php echo $key + 1;?></span>
                                            <span class="title"><?php echo $qa->fTitle;?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ol>
                                <a class="more" href="#">more</a>
                            </div>
                          </div>
                        </div>
                    </div>
                    <!-- End Leaderboard block -->

                    <!-- Leaderboard block -->
                    <div class="leader-board-wrap aside-block clearfix">
                        <div>
                          <!-- Nav tabs -->
                          <ul class="leader-board-header" role="tablist">
                            <li role="presentation" class="active"><a href="#today-qa" aria-controls="qa" role="tab" data-toggle="tab"><img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon-b.png"> Today Q & A</a></li>
                            <li role="presentation"><a href="#today-reply" aria-controls="reply" role="tab" data-toggle="tab">Today Reply</a></li>
                          </ul>
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active leader-board-content" id="today-qa">
                                <ol>
                                    <?php foreach ($best_recommended_qa as $key => $qa) { ?>
                                        <li>
                                        <a href="#" class="clearfix">
                                            <span class="number"><?php echo $key + 1;?></span>
                                            <span class="title"><?php echo $qa->fTitle;?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ol>
                                <a class="more" href="#">more</a>
                            </div>
                            <div role="tabpanel" class="tab-pane leader-board-content" id="today-reply">
                                <ol>
                                    <?php foreach ($us_life_best_reply as $key => $qa) { ?>
                                        <li>
                                        <a href="#" class="clearfix">
                                            <span class="number"><?php echo $key + 1;?></span>
                                            <span class="title"><?php echo $qa->fTitle;?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ol>
                                <a class="more" href="#">more</a>
                            </div>
                          </div>
                        </div>
                    </div>
                    <!-- End Leaderboard block -->
                    <div class="sponsor-wrap">
                        <h3>
                            Sponsor
                        </h3>
                        <div data-ad-code="mk01"></div>
                    </div>
                </aside>
            </div>
        </section>
        <footer>
            <?php $this->view('layout/partial_footer');?>
        </footer>
        <!-- End Main content -->
<?php //include_once($kaim_path."/include/js-include.html"); ?>
<?php $this->view('layout/partial_js');?>
<?php $this->view('layout/partial_footer_js');?>
</body>
</html>
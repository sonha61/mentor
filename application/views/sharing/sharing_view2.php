<header>
		   <?php $this->view('layout/partial_header');?>
</header>
		<!-- Main content -->
		<section>
			<div class="container">
				<!-- Left content -->
				<div class="main-wrap " id="main">
					<div class="left-wrap panel panel-100 clearfix">
						<!-- Bread Crumb -->
						<div class="breadcrumb-wrap clearfix">
							<ul>
							<?php if(isset($sharing_info->fCatSeq)) { echo $this->Common_Model->createPath($sharing_info->fCatSeq); } ?>
							</ul>
						</div>
						<!-- End Bread Crumb -->
						<!-- Main Topic -->
						<div class="main-topic clearfix">
							<div class="header clearfix">
								<div class="icon-share icon">
									<span class="share-text">Share</span>
								</div>
								<div class="title-wrap">
									<h3 class="title"><?php echo $sharing_info->fTitle;?></h3>
									<span class="user-name"><?php echo $sharing_info->fUserID;?></span>
									<span class="date">May 14, 2016</span>
									<span class="date"><?php echo date('M d, Y h:i A', strtotime($sharing_info->fFirstRegDT));?></span>
									<span class="view"><?php echo $sharing_info->fHit;?></span>
									<span class="comment"><?php echo $this->Sharing_Model->get_total_reply($sharing_info->fSeq);?></span>
								</div>
								<button class="btn btn-report"><?php echo $sharing_info->fNotRecommend;?></button>
							</div>
							<div class="content clearfix">
								<p>
									<?php echo $sharing_info->fContent;?>
								</p>
								<div class="source clearfix">
									Source.   Law,  Legislation
								</div>
							</div>
							<div class="social-wrap clearfix">
								<ul>
									<li class="like">
										<span><?php echo $sharing_info->fRecommend;?></span>
										<a href="#"><img src="<?php echo $base_url ?>assets/mentor_new/images/like-icon.png"></a>
									</li>
									<li>
										<a href="#" class="p-facebook-share"><img src="<?php echo $base_url ?>assets/mentor_new/images/fb-icon.png"></a>
									</li>
									<li>
										<a href="#" class="p-twitter-tweet"><img src="<?php echo $base_url ?>assets/mentor_new/images/tw-icon.png"></a>
									</li>
								</ul>
							</div>
							<div class="user-wrap clearfix">
								<div class="user-img">
									<img src="<?php echo $base_url ?>assets/mentor_new/images/travelling.png">
								</div>
								<div class="user-description">
									<div class="clearfix">
										<h3><?php echo $sharing_info->fUserID;?></h3>
										<span class="user-id">(<?php echo $sharing_info->fUser_Key;?>)</span>
									</div>
									<div class="introduction">
										<span>Hi ~ I am Zoe.</span>
										<p>
											I am Designer from Heykorean .
											I live in Vetnam. Thank you.
										</p>
									</div>
								</div>
								<div class="user-info">
									<div class="clearfix">
										<span class="level">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png">
											Silver
										</span>
										<span class="point">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/water-blue.png">
											450
										</span>
										<span class="rank">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon.png">
											15
										</span>
									</div>
									<div class="clearfix">
										<span class="text">주요활동분야 영어작문 생활 쇼핑 여행</span>
										<button class="btn btn-mentee">Add to Mentee</button>
									</div>
								</div>
							</div>
						</div>
						<!-- End Main Topic -->
						<hr>

						<!-- Comment box -->
						<div class="comment-box gray-box clearfix">
						<form method="post" name="writeReplySharing" onsubmit="return validateFormReplySharing();">
							<div class="user-img">
								<img src="<?php echo $base_url ?>assets/mentor_new/images/travelling.png">
							</div>
							<div class="text-editor">
								<div class="clearfix">
									<textarea rows="5" id="fContent" name="fContent"></textarea>
								</div>
								<div class="button-group clearfix">
									<button class="btn btn-orange btn-save">SAVE</button>
									<button class="btn btn-gray btn-discard">DISCARD</button>
									<button class="btn btn-blue btn-post" type="submit" name="submit">POST</button>
								</div>
							</div>
							</form>
						</div>
						<!-- End Comment box -->

						<!-- Comment List -->
						<div class="comment-list clearfix">
							<?php foreach($all_reply as $key => $reply) { ?>
							<div class="item clearfix">
								<div class="main-comment clearfix">
									<div class="user-img">
										<img src="<?php echo $base_url ?>assets/mentor_new/images/hands-all-in.png">
										<div class="popup">
											<div class="popup-user-img">
												<img src="<?php echo $base_url ?>assets/mentor_new/images/hands-all-in.png">
											</div>
											<div class="popup-user-info">
												<div class="clearfix">
													<span class="user-name"><?php echo $reply->fUserID;?></span>
													<span class="user-id"><?php echo $reply->fUser_Key;?></span>
													<span class="point">450</span>
													<span class="rank">Rank 15</span>
												</div>	
												<div class="clearfix">
													<span class="level"><img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png"> Silver</span>
													<button class="btn btn-mentee">Add to Mentee</button>
												</div>
											</div>
										</div>
									</div>
									<div class="comment-content">
										<div class="comment-info clearfix">
											<span class="user-name"><?php echo $reply->fUserID;?></span>
											<span class="date"><?php echo date('M d, Y h:i A', strtotime($reply->fRegDT));?></span>
										</div>
										<p>
											<?php echo $reply->fContent;?>
										</p>
										<div class="comment-action clearfix">
											<ul>
												<li>
													<button class="btn btn-like"><?php echo $reply->fRecommend;?></button>
												</li>
												<li>
													<button class="btn btn-dislike"><?php echo $reply->fNotRecommend;?></button>
												</li>
												<li>
													<span class="comment"><?php echo $this->Sharing_Model->get_total_reply_comment($reply->fSeq);?></span>
													<button class="btn btn-show-comment" data-toggle="collapse" data-target="#cmt<?php echo $reply->fSeq;?>" aria-expanded="false" aria-controls="cmt1"> Comment</button>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- Comment box -->
								<div class="comment-box reply-box clearfix collapse" id="cmt<?php echo $reply->fSeq;?>">
									<div class="user-img">
										<img src="<?php echo $base_url ?>assets/mentor_new/images/travelling.png">
									</div>
									<form method="post" name="replyComment" onsubmit="return validateFormReplySharing(<?php echo $reply->fSeq;?>);">
									<div class="text-editor">
										<div class="clearfix">
											<textarea rows="5" name="fComment" id="fComment"></textarea>
											<input type="hidden" name="replyID"  value="<?php echo $reply->fSeq;?>" >
										</div>
										<div class="button-group clearfix">
											<button class="btn btn-orange btn-save">SAVE</button>
											<button class="btn btn-gray btn-discard">DISCARD</button>
											<button class="btn btn-blue btn-post" type="submit" name="replyComment">POST</button>
										</div>
									</div>
									</form>
								</div>
								<!-- End Comment box -->
								<?php 
									$replyComment = $this->Reply_Sharing_Model->get_reply_comment_sharing($reply->fSeq);
								?>
								<?php if($replyComment) { foreach($replyComment as $key =>$replyCommentItem) { ?>
								<div class="item reply-item clearfix">
									<div class="clearfix">
										<div class="user-img">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/law.png">
											<div class="popup">
												<div class="popup-user-img">
													<img src="<?php echo $base_url ?>assets/mentor_new/images/law.png">
												</div>
												<div class="popup-user-info">
													<div class="clearfix">
														<span class="user-name"><?php echo $replyCommentItem->fUserID;?></span>
														<span class="user-id"><?php echo $replyCommentItem->fUser_Key;?></span>
														<span class="point">450</span>
														<span class="rank">Rank 15</span>
													</div>	
													<div class="clearfix">
														<span class="level"><img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png"> Silver</span>
														<button class="btn btn-mentee">Add to Mentee</button>
													</div>
												</div>
											</div>
										</div>
										<div class="comment-content">
											<div class="comment-info clearfix">
												<span class="user-name"><?php echo $replyCommentItem->fUserID;?></span>
												<span class="date"><?php echo date('M d, Y h:i A', strtotime($replyCommentItem->fRegDT));?></span>
											</div>
											<p>
												<?php echo $replyCommentItem->fContent;?>
											</p>
											<div class="comment-action clearfix">
												<ul>
													<li>
														<button class="btn btn-like">131</button>
													</li>
													<li>
														<button class="btn btn-dislike">3</button>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<?php } };?>
							</div>
							<?php } ?>
						</div>
						<!-- End Comment list -->
						<div class="back-list clearfix">
							<button class="btn btn-back-list"></button>
						</div>
					</div>
				</div>
				<!-- End Left content -->
				<!-- Right content -->
				<aside>
					<div class="button-wrap clearfix">
                		<a href="<?php echo $base_url . "qa/write";?>"><button class="btn btn-orange btn-ask">
                		<img src="<?php echo $base_url ?>assets/mentor_new/images/question-icon-w.png" alt="..."> ask</button>
                		</a>
                		<a href="<?php echo $base_url . "sharing/write";?>"><button class="btn btn-blue btn-share">
                		<img src="<?php echo $base_url ?>assets/mentor_new/images/comment-icon-w.png" alt="..."> share</button>
                		</a>
            		</div>
					<!-- Keyword block -->
					<div class="keyword-wrap aside-block clearfix">
						<div class="header">
							<h3>best keyword</h3>
							<div class="control-block">
								<a class="arrow-down-icon" role="button"></a>
								<a class="arrow-up-icon" role="button"></a>
							</div>						
						</div>
						<div class="keyword-list clearfix">
							<ol>
							<?php foreach($most_popular_keywords as $keyword):?>
								<li>
									<a href="">
										<span class="pos">1</span>
										<?php echo $keyword->fKeyword;?>
										<span class="quantity">110</span>
									</a>
								</li>
								<?php endforeach;?>
							</ol>
						</div>
					</div>
					<!-- End Keyword block -->

					<!-- Ads block -->
					<div class="ads-img clearfix">
						<img src="<?php echo $base_url ?>assets/mentor_new/images/banner.png" alt="ads">
					</div>
					<!-- End Ads block -->

					<!-- Leaderboard block -->
					<div class="leader-board-wrap aside-block clearfix">
						<div>
						  <!-- Nav tabs -->
						  <ul class="leader-board-header" role="tablist">
						    <li role="presentation" class="active"><a href="#best-qa" aria-controls="qa" role="tab" data-toggle="tab"><img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon-b.png"> Best Q & A</a></li>
						    <li role="presentation"><a href="#best-reply" aria-controls="reply" role="tab" data-toggle="tab">Best Reply</a></li>
						  </ul>
						  <!-- Tab panes -->
						  <div class="tab-content">
						    <div role="tabpanel" class="tab-pane active leader-board-content" id="best-qa">
						    	<ol>
						    	<?php foreach ($best_recommended_qa as $key => $qa) { ?>
						    			<li>
						    			<a href="#" class="clearfix">
						    				<span class="number"><?php echo $key + 1;?></span>
						    				<span class="title"><?php echo $qa->fTitle;?></span>
						    			</a>
						    		</li>
						    	<?php } ?>
						    		
						    	</ol>
						    	<a class="more" href="#">more</a>
						    </div>
						    <div role="tabpanel" class="tab-pane leader-board-content" id="best-reply">
						    	<ol>
						    		<?php foreach ($most_answered as $key => $qa) { ?>
						    			<li>
						    			<a href="#" class="clearfix">
						    				<span class="number"><?php echo $key + 1;?></span>
						    				<span class="title"><?php echo $qa->fTitle;?></span>
						    			</a>
						    		</li>
						    		<?php } ?>
						    	
						    	</ol>
						    	<a class="more" href="#">more</a>
						    </div>
						  </div>
						</div>
					</div>
					<!-- End Leaderboard block -->

					<!-- Leaderboard block -->
					<div class="leader-board-wrap aside-block clearfix">
						<div>
						  <!-- Nav tabs -->
						  <ul class="leader-board-header" role="tablist">
						    <li role="presentation" class="active"><a href="#today-qa" aria-controls="qa" role="tab" data-toggle="tab"><img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon-b.png"> Today Q & A</a></li>
						    <li role="presentation"><a href="#today-reply" aria-controls="reply" role="tab" data-toggle="tab">Today Reply</a></li>
						  </ul>
						  <!-- Tab panes -->
						  <div class="tab-content">
						    <div role="tabpanel" class="tab-pane active leader-board-content" id="today-qa">
						    	<ol>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">1</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">2</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">3</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">4</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">5</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    	</ol>
						    	<a class="more" href="#">more</a>
						    </div>
						    <div role="tabpanel" class="tab-pane leader-board-content" id="today-reply">
						    	<ol>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">1</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">2</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">3</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">4</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">5</span>
						    				<span class="title">콜택시 기사가 차로 우리언니를 콜택시 기사가 차로 우리언니를</span>
						    			</a>
						    		</li>
						    	</ol>
						    	<a class="more" href="#">more</a>
						    </div>
						  </div>
						</div>
					</div>
					<!-- End Leaderboard block -->
					<div class="sponsor-wrap">
						<h3>
							Sponsor
						</h3>
						<div data-ad-code="mk01"></div>
					</div>
				</aside>
			</div>
		</section>
		<footer>
    <?php $this->view('layout/partial_footer');?>
		</footer>
<!-- End Main content -->
<?php $this->view('layout/partial_js');?>
<?php $this->view('layout/partial_footer_js');?>
  </body>
</html>
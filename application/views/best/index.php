<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em>Seoul</em><img class="triangle" src="./images/triangle.png" alt="more location">
        </a>
        <div class="location_con" style="display: none;">
            <div class="container">
                <a title="close" class="p-area-close p-pointer" style="z-index: 100;">
                    <img src="http://cdn.heykorean.com/common/area_popup/locationcon_close.png" alt="close">
                </a>
                <li class="wide">
                    <dl>
                        <dt class="country">United State</dt>
                        <dd class="cancel">
                            <a href="javascript:void(0);" class="btnAreaClose" title="Close"></a>
                        </dd>
                        <dd class="line">
                            <img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif">
                        </dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state">New York City</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4672&amp;rUrl=http://www.heykorean.com" title="New York City">New York City</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4883&amp;rUrl=http://www.heykorean.com" title="Long Island">Long Island</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4865&amp;rUrl=http://www.heykorean.com" title="Binghamton">Binghamton</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4864&amp;rUrl=http://www.heykorean.com" title="Albany">Albany</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state">
                                <a href="http://global.heykorean.com/relation/?AI=4478&amp;rUrl=http://www.heykorean.com" title="New Jersey">New Jersey</a>
                            </dd>
                            <dd class="city">
                                <a href="#"></a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Maryland">Maryland</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4938&amp;rUrl=http://www.heykorean.com" title="Baltimore">Baltimore</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Massachusetts">Massachusetts</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4851&amp;rUrl=http://www.heykorean.com" title="Boston">Boston</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Washington">Washington</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4917&amp;rUrl=http://www.heykorean.com" title="Seattle">Seattle</a>
                            </dd>
                        </dl></div>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dd class="state" title="Illinois">Illinois</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4956&amp;rUrl=http://www.heykorean.com" title="Chicago">Chicago</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Georgia">Georgia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4919&amp;rUrl=http://www.heykorean.com" title="Atlanta">Atlanta</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="California">California</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4850&amp;rUrl=http://www.heykorean.com" title="San Francisco">San Francisco</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4849&amp;rUrl=http://www.heykorean.com" title="Los Angeles">Los Angeles</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="District of Columbia">District of Columbia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4922&amp;rUrl=http://www.heykorean.com" title="Washington D.C./Northern Virginia">Washington D.C./<br>Northern Virginia</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Pennsylvania">Pennsylvania</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4852&amp;rUrl=http://www.heykorean.com" title="Philadelphia">Philadelphia</a>
                            </dd>
                        </dl></div>
                </li>
                <li class="wide">
                    <dl>
                        <dt class="country" title="Asia Pacific">Asia Pacific</dt>
                        <dd class="line"><img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif"></dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state" title="Korea">Korea</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4822&amp;rUrl=http://www.heykorean.com" title="Seoul">Seoul</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4973&amp;rUrl=http://www.heykorean.com" title="Gyeongju">Gyeongju</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Vietnam">Vietnam</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4976&amp;rUrl=http://www.heykorean.com" title="Bac Ninh">Bac Ninh</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4914&amp;rUrl=http://www.heykorean.com" title="Hanoi">Hanoi</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4963&amp;rUrl=http://www.heykorean.com" title="Ho Chi Minh">Ho Chi Minh</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="China">China</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4882&amp;rUrl=http://www.heykorean.com" title="Beijing">Beijing</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl></div>
                </li>
            </div>
        </div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html">
            <a href="http://www.heykorean.com/hk_club/club_main.asp" title="club">club</a>
            <a href="http://www.heykorean.com/hkboard/room/rent_main.asp" title="housing">housing</a>
            <a href="http://www.heykorean.com/hk_job/index.asp" title="jobs">jobs</a>
            <a href="http://mentor.heykorean.com/" title="mentoring">mentoring</a>
            <a href="http://ktown.heykorean.com" title="yellowpage">yellowpage</a>
            <a class="more" title="more" href="#">more</a>
            <div class="more_wrap">
                <ul>
                    <li class="first"><a href="http://www.heykorean.com/hk_funtalk/">life</a></li>
                    <li><a href="http://www.heykorean.com/hk_news/">NEWS</a></li>
                    <li><a href="http://www.heykorean.com/hkinfo/infoview.asp">INFOPLAZA</a></li>
                    <li><a href="http://www.heykorean.com/hk_service/membership/">memberPlaza</a></li>
                    <li><a href="http://www.heykorean.com/hk_photoessay/">PhotoEssay</a></li>
                    <li class="last"><a href="javascript:openPopup('http://www.heykorean.com/hk_help/sitemap.asp',940,845)">All</a></li>
                </ul>
            </div>
            <div class="lang">
                <a title="english"><img src="./images/flag_en.gif"></a>
                <a title="korean"><img src="./images/flag_ko.gif"></a>
            </div>
        </div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>
                    <span>USA</span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
							0<span>Bxs</span>
							0<span>Bts</span>
							0<span>Dps</span>
						</a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <nav class="clearfix">
            <h2 class="hidden">heykorean mentoring menu</h2>
            <div class="menu-wrap">
                <div id="menu" class="clearfix">
                    <div><a title="Home"><span>Home</span></a></div>
                    <div class="on">
                        <a href="" title="US Life">
                            <span>US Life</span>
                        </a>
                        <div class="menu-sub">
                            <a>Immigration/Visa/Passport</a>
                            <a>School/Study Abroad/Academics</a>
                            <a>Shopping/Tips & Products</a>
                            <a>Economics, Money &amp; Banking</a>
                            <a>Beauty &amp; Style</a>
                            <a>Computer/Notebook/Videos</a>
                            <a>Food &amp; Restaurants</a>
                            <a>Entertainment/Performance/Arts</a>
                            <a>Sports/Leisure/Tournaments &amp; Competitions</a>
                            <a>Health &amp; Medicine</a>
                            <a>Laws, Society &amp; Culture</a>
                            <a>Motherhood &amp; Schooling</a>
                            <a>Lifestyle Info.</a>
                            <a>Traveling/Sightseeing</a>
                            <a>Other</a>
                            <a>HeyKorean</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="advice"><span>advice</span></a>
                        <div class="menu-sub">
                            <a>School &amp; Academy Selection</a>
                            <a>Studies, Major &amp; Career</a>
                            <a>Identification, Sojourn &amp; Legal Help</a>
                            <a>English Proficiency/Racial Discrimination</a>
                            <a>Working Life</a>
                            <a>Dating &amp; Marriage</a>
                            <a>Friends &amp; Relationships</a>
                            <a>Appearance &amp; Health</a>
                            <a>Depression &amp; Mental Illness</a>
                            <a>Living/Enstrangement/Invasion of Privacy</a>
                            <a>Personality &amp; Habits</a>
                            <a>Sexual Problems/Bodily Changes</a>
                            <a>Domestic/Marital Issues</a>
                            <a>Financial Difficulties/Living Expenses/Tuition</a>
                            <a>Identity Theft/Fraud/Damages</a>
                            <a>Help &amp; Advice</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Local Info"><span>Local Info</span></a>
                        <div class="menu-sub">
                            <a>New York</a>
                            <a>New Jersey</a>
                            <a>California</a>
                            <a>Washington, DC.</a>
                            <a>Pennsylvania</a>
                            <a>Connecticut</a>
                            <a>Illinois</a>
                            <a>Virginia</a>
                            <a>Massachusetts</a>
                            <a>Georgia</a>
                            <a>Florida</a>
                            <a>Texas</a>
                            <a>Washington</a>
                            <a>Indiana</a>
                            <a>Hawaii</a>
                            <a>Canada</a>
                            <a>@Traveling/Sightseeing</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Live Debate"><span>Live Debate</span></a>
                        <div class="menu-sub">
                            <a>Shopping &amp; Products</a>
                            <a>Entertainment &amp; Arts</a>
                            <a>Sports/Leisure/Games</a>
                            <a>Education &amp; Academics</a>
                            <a>Computer/Intertet</a>
                            <a>Travel/Geography</a>
                            <a>Life &amp; Family</a>
                            <a>Financial &amp; Investment Tips</a>
                            <a>Society &amp; Culture</a>
                            <a>Beauty &amp; Style</a>
                            <a>Health &amp; Medicine</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Sharing"><span>Sharing</span></a>
                        <div class="menu-sub">
                            <a>Lifestyle &amp; Shopping Know-how</a>
                            <a>Health/Dieting/Beauty</a>
                            <a>English Language</a>
                            <a>Study/Career/Education/Research</a>
                            <a>Literature &amp; Arts</a>
                            <a>Views on Happiness</a>
                            <a>Hobbies/Leisure/Travel</a>
                            <a>Knowledge/Trends</a>
                            <a>Dictionary of Terms</a>
                            <a>Entertainment</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Best"><span>Best</span></a>
                    </div>
                    <div>
                        <a href="" title="Good People"><span>Good People</span></a>
                        <div class="menu-sub">
                            <a>Honor Mentors</a>
                            <a>Leader Mentors</a>
                            <a>Nanumis</a>
                            <a>Expert Mentors</a>
                            <a>Partner Mentors</a>
                            <a>Jikimis</a>
                        </div>
                    </div>
                    <div>
                        <a title="My Mentoring"><span>My Mentoring</span></a>
                    </div>
                </div>
            </div>
            <div class="ask-share">
                <a class="btn-ask" title="Ask"><span></span>Ask</a>
                <a class="btn-share" title="Share"><span></span>Share</a>
            </div>
        </nav>
        <div class="mentoring-notice clearfix">
            <div class="location">
                <a>Home</a> &gt;
                <a><strong>US Life</strong></a>
            </div>
            <div class="newly-wrap">
                <span class="announcement">공지</span>
                <p><a>멘토링을 개편 하였습니다. 멘토링을 개편 하였습니다.</a></p>
            </div>
        </div>

        <div class="top-con clearfix">
            <ul class="category-wrap best-top">
                <li class="first">
                    <div>
                        <a>한국에 돌아가야 하는데 여권 연장이 걱정됩니다. 한국에 돌아가야 하는데 여권 연장이 걱정됩니다. </a>
                        <span class="user-cate">Other</span>
                        <span class="user-name"><a>HeyKorean</a></span>
                        <span class="user-date">Dec 31, 2016</span>
                    </div>
                    <span class="comment-count">[999+]</span>
                    <p>안녕하세요. 이번 6월 초에 한국에 돌아가서 2,3년 정도 있을 겁니다. 출국 날은 5월 20일 전쯤으로 정했는데 제 여권이 25일에 만료가 됩니다. 혹시나 해서 학교에 있는 교환학생 관련 사무실에 가서 얘기해보니, 자기 국가로의 출국을 포함한 모든 입국, 출국 시 여권 유효기간이 8달 이상 있어야 한다고 들었습니다. 사실인지 아닌지는 잘 모르겠는데 좀 걱정되네요. 한국 여권기간 연장을 하는데 새 여권을 받는 시간과 여권을 신청비용을 여쭤보고 싶은데, 도움 부탁드립니다....</p>
                </li>
                <li>
                    <div>
                        <a>한국에 돌아가야 하는데 여권 연장이 걱정됩니다. 한국에 돌아가야 하는데 여권 연장이 걱정됩니다. </a>
                        <span>Other</span>
                        <span><a>HeyKorean</a></span>
                        <span>Dec 30, 2016</span>
                    </div>
                    <span class="comment-count">[999+]</span>
                </li>
                <li>
                    <div>
                        <a>한국에 돌아가야 하는데 여권 연장이 걱정됩니다. 한국에 돌아가야 하는데 여권 연장이 걱정됩니다. </a>
                        <span>Other</span>
                        <span><a>HeyKorean</a></span>
                        <span>Dec 30, 2016</span>
                    </div>
                    <span class="comment-count">[999+]</span>
                </li>
                <li>
                    <div>
                        <a>한국에 돌아가야 하는데</a>
                        <span>Other</span>
                        <span><a>HeyKorean</a></span>
                        <span>Dec 30, 2016</span>
                    </div>
                    <span class="comment-count">[999+]</span>
                </li>
            </ul>
            <div class="login-wrap">
                <!-- logout -->
                <!--
                <form action="post">
                    <fieldset>
                        <legend>heykorean login</legend>
                        <div class="login-input">
                            <h3>HeyKorean Login</h3>
                            <input type="text" placeholder="ID">
                            <input type="password">
                        </div>
                        <div class="login-save">
                            <label><input type="checkbox">Secured</label>
                            <label><input type="checkbox">SaveID</label>
                            <button type="submit">Log In</button>
                        </div>
                        <div class="signup-wrap">
                            <button type="submit">Sign up</button>
                            <a href="http://www.heykorean.com/HK_Member/en_id_pass_k.asp">Forgot id/password</a>
                        </div>
                    </fieldset>
                </form>
                 -->
                <!-- //logout -->
                <!-- login -->
                <strong class="my-nickname">heykorean</strong>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a>999+</a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt>Voted As</dt>
                        <dd style="width:65px;"><a>999+</a>  <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
        </div>
        <div class="most-wrap clearfix">
            <h2 class="hidden">heykorean lnb</h2>
            <div class="thuydinh">
                <div class="most-keywords">
                    <h3>Most Popular Keywords</h3>
                    <ul class="rank-list">
                        <?php foreach($most_popular_keywords as $keyword):?>
                            <li class="sonha"><span class="ranking"><?php echo $keyword->fCount;?></span><a><?php echo $keyword->fKeyword;?></a><span class="keyword-count"></span></li>
                        <?php endforeach;?>
                    </ul>
                    <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                    <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
                </div>
            </div>
        </div>
        <div id="container" class="mentor-main clearfix">
            <!-- 中 -->
            <div class="mentoring-con advice">
                <div class="mentoring-board tab-style02">
                    <a class="more" title="help">Help</a>
                    <h3 class="hidden">mentoring board</h3>
                    <?php
                    $open = 'none';
                    $class = '';
                    if(!isset($_GET['fTab'])) {
                        $open = 'block';
                        $class = 'on';
                    } else {
                        if(isset($_GET['fTab']) && $_GET['fTab'] == 'open') {
                            $open = 'block';
                            $class = 'on';
                        }
                    }
                    ?>
                    <div class="tab-title clearfix">
                        <a href="<?php echo $base_url . "/best?fTab=most_popular"; ?>" title="Most Popular" class="<?php echo $class; ?>">Most Popular</a>
                        <a href="<?php echo $base_url . "/best?fTab=most_recommended"; ?>" title="Most Recommended" class="<?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'most_recommended') ? 'on' : ''; ?>" >Most Recommended</a>
                        <a href="<?php echo $base_url . "/best?fTab=director_choice"; ?>" title="Directors' Choices" class="<?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'director_choice') ? 'on' : ''; ?>" >Directors' Choices</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab08" style="display:<?php echo $open; ?>">
                            <div class="box-subject clearfix">
                                <h3>US Life</h3>
                                <div class="sort-by">
                                    <dl>
                                        <dt><label for="sortBy">Sort by : </label></dt>
                                        <dd>
                                            <select id="sortBy">
                                                <option>US Life</option>
                                                <option>Advice</option>
                                                <option>Regional Info.</option>
                                                <option>Multiple</option>
                                                <option>Sharing</option>
                                            </select>
                                        </dd>
                                        <dd>
                                            <select>
                                                <option>Within 1 week</option>
                                                <option>Within 1 month</option>
                                                <option>Within 3 months</option>
                                                <option>Within 6 months</option>
                                                <option>Within 1 year</option>
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>NJ</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>U.S.A</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NY</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>FL</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>MA</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>ETC</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>CT</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>PA</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>CA</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NJ</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	U.S.A</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	NY</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	FL</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>MA</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>ETC</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	CT</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>PA</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	CA</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab09" style="display: <?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'most_recommended') ? 'block' : 'none'; ?>">
                            <div class="box-subject clearfix">
                                <h3>US Life</h3>
                                <div class="sort-by">
                                    <dl>
                                        <dt><label for="sortBy">Sort by : </label></dt>
                                        <dd>
                                            <select id="sortBy">
                                                <option>US Life</option>
                                                <option>Advice</option>
                                                <option>Regional Info.</option>
                                                <option>Multiple</option>
                                                <option>Sharing</option>
                                            </select>
                                        </dd>
                                        <dd>
                                            <select>
                                                <option>Within 1 month</option>
                                                <option>Within 3 months</option>
                                                <option>Within 6 months</option>
                                                <option>Within 1 year</option>
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>MA</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>ETC</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>CT</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>PA</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>CA</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NJ</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>U.S.A</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NY</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>FL</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NJ</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	U.S.A</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	NY</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	FL</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>MA</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>ETC</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	CT</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>PA</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	CA</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab10" style="display: <?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'director_choice') ? 'block' : 'none'; ?>">
                            <div class="box-subject clearfix">
                                <h3>US Life</h3>
                                <div class="sort-by">
                                    <dl>
                                        <dt><label for="sortBy">Sort by : </label></dt>
                                        <dd>
                                            <select id="sortBy">
                                                <option>US Life</option>
                                                <option>Advice</option>
                                                <option>Regional Info.</option>
                                                <option>Multiple</option>
                                                <option>Sharing</option>
                                            </select>
                                        </dd>
                                        <dd>
                                            <select>
                                                <option>Within 3 months</option>
                                                <option>Within 6 months</option>
                                                <option>Within 1 year</option>
                                            </select>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Areas<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>CA</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NJ</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>U.S.A</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NY</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>FL</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>NJ</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	U.S.A</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	NY</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	FL</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>MA</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>ETC</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	CT</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>PA</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>	CA</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>MA</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>ETC</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>CT</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>PA</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>Mad Clown</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td class="mentor-titlecate"><span>Vehicles</span></td>
                                    <td>K.will</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td class="mentor-titlecate"><span>Music</span></td>
                                    <td>CHEN</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td class="mentor-titlecate"><span>Eastern US</span></td>
                                    <td>-Private-</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                <tr>
                                    <td>OH</td>
                                    <td><a class="mentor-title">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td class="mentor-titlecate"><span>OPT</span></td>
                                    <td>Shakira</td>
                                    <td>Dec 31, 2016</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if($links) : ?>
                        <div class="pagewrap">
                            <a class="btn-first"><span class="hidden">처음목록</span></a>
                            <a class="btn-pre"><span class="hidden">이전목록</span></a>
                            <?php echo $links; ?>
                            <a class="btn-next"><span class="hidden">다음목록</span></a>
                            <a class="btn-last"><span class="hidden">마지막목록</span></a>
                        </div>
                    <?php endif;?>
                    <div class="mentorlist-search">
                        <form action="post">
                            <fieldset>
                                <legend>search mentoring list</legend>
                                <select>
                                    <option>Title</option>
                                    <option>Content</option>
                                    <option>Tag</option>
                                    <option>ID</option>
                                </select>
                                <input type="search" id="metoringlistSearch" style="width:184px;">
                                <label for="metoringlistSearch"><button type="button">Search</button></label>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <!-- //中 -->
            <!-- 右 -->
            <div class="aside">
                <a class="btn-mainask" title="질문하기">ASK</a>
                <div class="most answered-questions advice">
                    <h3>Best Recommended <span>Q&amp;A</span></h3>
                    <ol>
                        <li><span class="num">1</span><a>외국친구와 갈만한 플러싱 한국식당 추천해주세요.</a><span class="comment">[999+]</span></li>
                        <li><span class="num">2</span><a>한인운영 복싱 킥복싱장 추천해주세요.</a><span class="comment">[0]</span></li>
                        <li><span class="num">3</span><a>뉴욕시티 근처 여행 갈만한 곳</a><span class="comment">[999+]</span></li>
                        <li><span class="num">4</span><a>H1b 직장옮기는데 걸리는 시간 혹은 영주권</a><span class="comment">[1]</span></li>
                        <li><span class="num">5</span><a>영주권 신분변경기간동안 운전면허</a><span class="comment">[16]</span></li>
                    </ol>
                </div>
                <div class="most answered-questions advice">
                    <h3><span>US Life</span> Best Reply 5</h3>
                    <ol>
                        <li><span class="num">1</span><a>외국친구와 갈만한 플러싱 한국식당 추천해주세요.</a><span class="comment">[999+]</span></li>
                        <li><span class="num">2</span><a>한인운영 복싱 킥복싱장 추천해주세요.</a><span class="comment">[0]</span></li>
                        <li><span class="num">3</span><a>뉴욕시티 근처 여행 갈만한 곳</a><span class="comment">[999+]</span></li>
                    </ol>
                </div>
                <div class="sponsor-ad">
                    <h3>Sponsor <span>Ads</span></h3>
                    <a href="http://www.heykorean.com/HK_Club/HK_Club_main.asp?club_id=10000003" target="_blank" title="뉴욕유학생협회 KSANY 바로가기">
                        <img src="https://s3.amazonaws.com/img.ad.heykorean.com/Advertise/2015/12/04/2OrGtHjvC3Q.gif" alt="뉴욕유학생협회 KSANY 바로가기">
                    </a>
                    <div class="btn-sponsorad">
                        <a class="style01">FAQ</a>
                        <a class="style02">문의/제안</a>
                    </div>
                </div>
            </div>
            <!-- //右 -->
        </div>
    </div>
<header>
		   <?php $this->view('layout/partial_header');?>
</header>
		<!-- Main content -->
		<section>
			<div class="container">
				<!-- Left content -->
				<div class="main-wrap" id="main">
					<div class="clearfix">
						<div class="user-panel panel panel-50 col-left">
							<div class="clearfix">
								<h3><?php echo ($user_info) ? $user_info->strUserID : 'Username';?></h3>
								<span class="user-id">(<?php echo ($user_info) ? $user_info->n4UserKey : 'UserId';?>)</span>
								<button class="btn btn-edit"></button>
							</div>	
							<div class="clearfix">
								<div class="user-img">
									<img src="<?php echo $base_url ?>assets/mentor_new/images/travelling.png" alt="...">
								</div>
								<div class="user-description">
									<div class="clearfix">
										<span class="level">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png">
											Silver
										</span>
										<span class="point">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/water-blue.png">
											450
										</span>
										<span class="rank">
											<img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon.png">
											15
										</span>
									</div>
									<div class="introduction clearfix">
										<span>Hi ~ I am Zoe.</span>
										<p>I am Designer from Heykorean .I live in Vetnam.Thank you.</p>
									</div>
								</div>
							</div>
							<div class="clearfix">
								<button class="btn btn-mentee">Add to Mentee</button>
							</div>
						</div>
						<div class="user-level panel panel-50 col-right">
							<div class="clearfix">
								<h3 class="header-level">MY LEVEL STATUS</h3>
								<button class="btn btn-question"></button>
							</div>	
							<div class="progress-wrap">
								<div class="current-level">
									<img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png">
								</div>
								<div class="progress">
								  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
								    <span class="point">35 pts left</span>
								  </div>
								</div>
								<div class="next-level">
									<img src="<?php echo $base_url ?>assets/mentor_new/images/level-icon.png">
								</div>
							</div>
							<div class="detail-info clearfix">
								<ul>
									<li class="color-blue">
										<span class="number">18</span>
										<span class="type">Answers</span>
									</li>
									<li class="color-orange">
										<span class="number">05</span>
										<span class="type">Questions</span>
									</li>
									<li class="color-red">
										<span class="number">05</span>
										<span class="type">Questions</span>
									</li>
									<li class="color-green">
										<span class="number">101</span>
										<span class="type">Points</span>
									</li>
									<li class="color-green">
										<span class="number">10%</span>
										<span class="type">Points</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="list-panel panel panel-100 clearfix">
						<div class="header clearfix">
							<h3>My <span>Answers</span></h3>
							<span class="quantity">(18)</span>
							<a href="#" class="more">more</a>
						</div>
						<div class="list-content">
							<table class="table">
								<?php foreach($my_answers as $key => $answer) { ?>
								<tr>
									<td class="title"><?php echo $answer->fTitle;?></td>
									<td class="category"><?php echo $this->Category_Model->get_category_name_by_code($this->Qa_Model->get_qa($answer->fSeq)->fCatSeq);?></td>
									<td class="check"><?php echo $this->Qa_Model->get_qa($answer->fSeq)->fRecommend;?></td>
									<td class="comment"><?php echo $this->Qa_Model->get_total_reply($answer->fSeq);?></td>
									<td class="view"><?php echo $this->Qa_Model->get_qa($answer->fSeq)->fHit;?></td>
								</tr>
								<?php } ?>
							</table>
						</div>
					</div>
					<div class="list-panel panel panel-100 clearfix">
						<div class="header clearfix">
							<h3>My <span>Questions</span></h3>
							<span class="quantity">(2)</span>
							<a href="#" class="more">more</a>
						</div>
						<div class="list-content">
							<table class="table">
								<?php foreach($my_questions as $key => $question) { ?>
								<tr>
									<td class="title"><?php echo $question->fTitle;?></td>
									<td class="category"><?php echo $this->Category_Model->get_category_name_by_code($question->fCatSeq);?></td>
									<td class="check"><?php echo $question->fRecommend;?></td>
									<td class="comment"><?php echo $this->Qa_Model->get_total_reply($question->fSeq);?></td>
									<td class="view"><?php echo $question->fHit;?></td>
								</tr>
								<?php } ?>
							</table>
						</div>
					</div>
					<div class="list-panel panel panel-100 clearfix">
						<div class="header clearfix">
							<h3>My <span>Share</span></h3>
							<span class="quantity">(3)</span>
							<a href="#" class="more">more</a>
						</div>
						<div class="list-content">
							<table class="table">
								<tr>
									<td class="title">뉴저지 주변의 쇼핑센터를 추천해주세요!</td>
									<td class="category">Working Life</td>
									<td class="check">02</td>
									<td class="comment">02</td>
									<td class="view">258</td>
								</tr>
								<tr>
									<td class="title">New York City tips everyone should know</td>
									<td class="category">Shopping</td>
									<td class="check">02</td>
									<td class="comment">02</td>
									<td class="view">258</td>
								</tr>
								<tr>
									<td class="title">뉴저지 주변의 쇼핑센터를 추천해주세요!</td>
									<td class="category">Working Life</td>
									<td class="check">02</td>
									<td class="comment">02</td>
									<td class="view">258</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- End Left content -->
				<!-- Right content -->
				<aside>
					<!-- Keyword block -->
					<div class="keyword-wrap aside-block clearfix">
						<div class="header">
							<h3>best keyword</h3>
							<div class="control-block">
								<a class="arrow-down-icon" role="button"></a>
								<a class="arrow-up-icon" role="button"></a>
							</div>						
						</div>
						<div class="keyword-list clearfix">
							<ol>
								<li>
									<a href="">
										<span class="pos">1</span>
										마사지
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">2</span>
										플러싱
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">3</span>
										보톡스
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">4</span>
										뉴욕
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">5</span>
										뉴저지 영주권
										<span class="quantity">110</span>
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- End Keyword block -->

					<!-- Ads block -->
					<div class="ads-img clearfix">
						<div data-ad-code="mt01"></div>
					</div>
					<!-- End Ads block -->
					<div class="sponsor-wrap">
						<h3>
							Sponsor
						</h3>
						<div data-ad-code="mk01"></div>
					</div>
				</aside> 
		</section>
		<footer>
    <?php $this->view('layout/partial_footer');?>
		</footer>
<!-- End Main content -->
<?php $this->view('layout/partial_js');?>
<?php $this->view('layout/partial_footer_js');?>
  </body>
</html>
<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>index.php/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>
                    <span><a class="p-area-title"></a></span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
							0<span>Bxs</span>
							0<span>Bts</span>
							0<span>Dps</span>
						</a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
            <div class="location">
                <a href="<?php echo $base_url ?>">Home</a> &gt;
                <a href="<?php echo $base_url ?>user/myMentor"><strong>MyMentoring</strong></a>
            </div>
        </div>

        <div id="container">
            <div class="mymentoring-main">
                <h2 class="hidden">heykorean mymentoring contents</h2>
                <div class="my-info">
                    <img src="<?php echo $base_url ?>images/no_img.gif" class="my-pic">
                    <strong>닉네임 없음</strong>
                    <dl class="my-info01">
                        <dt>Level</dt>
                        <dd>Skull</dd>
                        <dt>Rnaking</dt>
                        <dd>999+위</dd>
                        <dt>Points</dt>
                        <dd>999+</dd>
                    </dl>
                    <dl class="my-info02">
                        <dt class=""><span></span>쪽지</dt>
                        <dd>0</dd>
                        <dt class=""><span></span>MyMentee</dt>
                        <dd>0</dd>
                    </dl>
                </div>
                <div class="infobox01">
                    <p class="info-title">HeyKorean님을 소개해보세요.</p>
                    <p class="info-con">안녕하세요<br>HeyKorean님의 마이멘토링입니다.</p>
                    <dl>
                        <dt><strong>Location : </strong></dt>
                        <dd>활동 지역을 설정해주세요.</dd>
                    </dl>
                </div>
                <div class="infobox02">
                    <div class="mylevel-wrap">
                        <span class="level-start level01"><span class="hidden">나의 현재 level</span></span>
                        <span class="level-next level02"><span class="hidden">나의 다음 level</span></span>
                        <div class="progress-wrap">
                            <div class="progress-bar" style="width:35%"></div>
                            <div class="tooltip" style="left:35%">
                                Bronze Star까지 남은<br>채택답변:<strong>0</strong> 내공:<strong>116</strong>
                            </div>
                        </div>
                    </div>
                    <div class="txt-rate">
                        <div>
                            <span style="width:35%">0</span>
                            <span>0 %</span>
                            <span>0</span>
                        </div>
                        <div>
                            <span style="width:35%">Chosen</span>
                            <span>Success Rate</span>
                            <span>Total Answers</span>
                        </div>
                    </div>
                    <h3>Activity Summary</h3>
                    <dl>
                        <dt>Answers</dt>
                        <dd>999+ <span>(Chosen : 999+ | Open: 999+ |Success Rate: 11%)</span></dd>
                        <dt>Questions :</dt>
                        <dd>999+ </dd>
                        <dt>1:1 Q&amp;As :</dt>
                        <dd>999+ </dd>
                        <dt>Sharing :</dt>
                        <dd>999+ </dd>
                    </dl>
                </div>
            </div>
            <div class="mymentoring-bottom clearfix">
                <div class="mymentoring-lnb">
                    <a title="My Answers">My Answers <span>(999+)</span></a>
                    <a title="My Questions">My Questions <span>(999+)</span></a>
                    <a title="My Sharing">My Sharing <span>(999+)</span></a>
                    <a title="Mentors &amp; Mentees">Mentors &amp; Mentees</a>
                    <a title="My Profile">My Profile</a>
                </div>
                <div class="mymentoring-con">
                    <div class="mymentoring-table">
                        <h3>My Answers</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col style="*">
                                <col style="width:10%">
                                <col style="width:10%">
                                <col style="width:13%">
                                <col style="width:10%">
                                <col style="width:13%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope="row">Subject</th>
                                <th scope="row">Division</th>
                                <th scope="row">Directory</th>
                                <th scope="row">Status</th>
                                <th scope="row">Good</th>
                                <th scope="row">Newest</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="6">작성된 게시물이 없습니다.</td>
                            </tr>
                            <tr>
                                <td>
                                    <a>매일 울리는 벨벨벨 이젠 나를 배려 해줘 배터리 낭비하긴 싫어 자꾸만 봐 자꾸 자꾸만 와 전화가 펑 터질 것만 같아</a>
                                </td>
                                <td>US Life</td>
                                <td>Lifestyle</td>
                                <td></td>
                                <td>999+</td>
                                <td>Dec 31, 2016</td>
                            </tr>
                            </tbody>
                        </table>
                        <a class="more">More</a>
                    </div>
                    <div class="mymentoring-table">
                        <h3>My Questions</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col style="*">
                                <col style="width:10%">
                                <col style="width:10%">
                                <col style="width:13%">
                                <col style="width:10%">
                                <col style="width:13%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope="row">Subject</th>
                                <th scope="row">Division</th>
                                <th scope="row">Directory</th>
                                <th scope="row">Status</th>
                                <th scope="row">Good</th>
                                <th scope="row">Newest</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="6">작성된 게시물이 없습니다.</td>
                            </tr>
                            <tr>
                                <td>
                                    <a>몰라 몰라 숨도 못 쉰대 나 때문에 힘들어 쿵 심장이 떨어진대 왜 걔 말은 나 너무 예쁘대 자랑 하는건 아니구 </a>
                                </td>
                                <td>US Life</td>
                                <td>Internet/Computer</td>
                                <td></td>
                                <td>999+</td>
                                <td>Dec 31, 2016</td>
                            </tr>
                            </tbody>
                        </table>
                        <a class="more">More</a>
                    </div>
                    <div class="mymentoring-table">
                        <h3>My Sharing</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col style="*">
                                <col style="width:10%">
                                <col style="width:10%">
                                <col style="width:13%">
                                <col style="width:10%">
                                <col style="width:13%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope="row">Subject</th>
                                <th scope="row">Division</th>
                                <th scope="row">Directory</th>
                                <th scope="row">Status</th>
                                <th scope="row">Good</th>
                                <th scope="row">Newest</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="6">작성된 게시물이 없습니다.</td>
                            </tr>
                            <tr>
                                <td>
                                    <a>아 답장을 못해줘서 미안해 친구를 만나느라 shy shy shy 만나긴 좀 그렇구 미안해 좀 있다 연락할게 later 조르지마 어디 가지 않아 되어줄게 너의 Baby 너무 빨린 싫어 성의를 더 보여 내가 널 기다려줄게</a>
                                </td>
                                <td>US Life</td>
                                <td>Internet/Computer</td>
                                <td></td>
                                <td>999+</td>
                                <td>Dec 31, 2016</td>
                            </tr>
                            </tbody>
                        </table>
                        <a class="more">More</a>
                    </div>
                </div>
            </div>
        </div>
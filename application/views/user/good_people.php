<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em>Seoul</em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con" style="display: none;">
            <div class="container">
                <a title="close" class="p-area-close p-pointer" style="z-index: 100;">
                    <img src="http://cdn.heykorean.com/common/area_popup/locationcon_close.png" alt="close">
                </a>
                <li class="wide">
                    <dl>
                        <dt class="country">United State</dt>
                        <dd class="cancel">
                            <a href="javascript:void(0);" class="btnAreaClose" title="Close"></a>
                        </dd>
                        <dd class="line">
                            <img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif">
                        </dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state">New York City</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4672&amp;rUrl=http://www.heykorean.com" title="New York City">New York City</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4883&amp;rUrl=http://www.heykorean.com" title="Long Island">Long Island</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4865&amp;rUrl=http://www.heykorean.com" title="Binghamton">Binghamton</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4864&amp;rUrl=http://www.heykorean.com" title="Albany">Albany</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state">
                                <a href="http://global.heykorean.com/relation/?AI=4478&amp;rUrl=http://www.heykorean.com" title="New Jersey">New Jersey</a>
                            </dd>
                            <dd class="city">
                                <a href="#"></a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Maryland">Maryland</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4938&amp;rUrl=http://www.heykorean.com" title="Baltimore">Baltimore</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Massachusetts">Massachusetts</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4851&amp;rUrl=http://www.heykorean.com" title="Boston">Boston</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Washington">Washington</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4917&amp;rUrl=http://www.heykorean.com" title="Seattle">Seattle</a>
                            </dd>
                        </dl></div>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dd class="state" title="Illinois">Illinois</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4956&amp;rUrl=http://www.heykorean.com" title="Chicago">Chicago</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Georgia">Georgia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4919&amp;rUrl=http://www.heykorean.com" title="Atlanta">Atlanta</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="California">California</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4850&amp;rUrl=http://www.heykorean.com" title="San Francisco">San Francisco</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4849&amp;rUrl=http://www.heykorean.com" title="Los Angeles">Los Angeles</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="District of Columbia">District of Columbia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4922&amp;rUrl=http://www.heykorean.com" title="Washington D.C./Northern Virginia">Washington D.C./<br>Northern Virginia</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Pennsylvania">Pennsylvania</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4852&amp;rUrl=http://www.heykorean.com" title="Philadelphia">Philadelphia</a>
                            </dd>
                        </dl></div>
                </li>
                <li class="wide">
                    <dl>
                        <dt class="country" title="Asia Pacific">Asia Pacific</dt>
                        <dd class="line"><img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif"></dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state" title="Korea">Korea</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4822&amp;rUrl=http://www.heykorean.com" title="Seoul">Seoul</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4973&amp;rUrl=http://www.heykorean.com" title="Gyeongju">Gyeongju</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Vietnam">Vietnam</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4976&amp;rUrl=http://www.heykorean.com" title="Bac Ninh">Bac Ninh</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4914&amp;rUrl=http://www.heykorean.com" title="Hanoi">Hanoi</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4963&amp;rUrl=http://www.heykorean.com" title="Ho Chi Minh">Ho Chi Minh</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="China">China</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4882&amp;rUrl=http://www.heykorean.com" title="Beijing">Beijing</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl></div>
                </li>
            </div>
        </div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html">
            <a href="http://www.heykorean.com/hk_club/club_main.asp" title="club">club</a>
            <a href="http://www.heykorean.com/hkboard/room/rent_main.asp" title="housing">housing</a>
            <a href="http://www.heykorean.com/hk_job/index.asp" title="jobs">jobs</a>
            <a href="http://mentor.heykorean.com/" title="mentoring">mentoring</a>
            <a href="http://ktown.heykorean.com" title="yellowpage">yellowpage</a>
            <a class="more" title="more" href="#">more</a>
            <div class="more_wrap">
                <ul>
                    <li class="first"><a href="http://www.heykorean.com/hk_funtalk/">life</a></li>
                    <li><a href="http://www.heykorean.com/hk_news/">NEWS</a></li>
                    <li><a href="http://www.heykorean.com/hkinfo/infoview.asp">INFOPLAZA</a></li>
                    <li><a href="http://www.heykorean.com/hk_service/membership/">memberPlaza</a></li>
                    <li><a href="http://www.heykorean.com/hk_photoessay/">PhotoEssay</a></li>
                    <li class="last"><a href="javascript:openPopup('http://www.heykorean.com/hk_help/sitemap.asp',940,845)">All</a></li>
                </ul>
            </div>
            <div class="lang">
                <a title="english"><img src="<?php echo $base_url ?>assets/images/flag_en.gif"></a>
                <a title="korean"><img src="<?php echo $base_url ?>assets/images/flag_ko.gif"></a>
            </div>
        </div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>
                    <span>USA</span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
							0<span>Bxs</span>
							0<span>Bts</span>
							0<span>Dps</span>
						</a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <nav class="clearfix">
            <h2 class="hidden">heykorean mentoring menu</h2>
            <div class="menu-wrap">
                <div id="menu" class="clearfix">
                    <div><a title="Home"><span>Home</span></a></div>
                    <div>
                        <a href="" title="US Life">
                            <span>US Life</span>
                        </a>
                        <div class="menu-sub">
                            <a>Immigration/Visa/Passport</a>
                            <a>School/Study Abroad/Academics</a>
                            <a>Shopping/Tips & Products</a>
                            <a>Economics, Money &amp; Banking</a>
                            <a>Beauty &amp; Style</a>
                            <a>Computer/Notebook/Videos</a>
                            <a>Food &amp; Restaurants</a>
                            <a>Entertainment/Performance/Arts</a>
                            <a>Sports/Leisure/Tournaments &amp; Competitions</a>
                            <a>Health &amp; Medicine</a>
                            <a>Laws, Society &amp; Culture</a>
                            <a>Motherhood &amp; Schooling</a>
                            <a>Lifestyle Info.</a>
                            <a>Traveling/Sightseeing</a>
                            <a>Other</a>
                            <a>HeyKorean</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="advice"><span>advice</span></a>
                        <div class="menu-sub">
                            <a>School &amp; Academy Selection</a>
                            <a>Studies, Major &amp; Career</a>
                            <a>Identification, Sojourn &amp; Legal Help</a>
                            <a>English Proficiency/Racial Discrimination</a>
                            <a>Working Life</a>
                            <a>Dating &amp; Marriage</a>
                            <a>Friends &amp; Relationships</a>
                            <a>Appearance &amp; Health</a>
                            <a>Depression &amp; Mental Illness</a>
                            <a>Living/Enstrangement/Invasion of Privacy</a>
                            <a>Personality &amp; Habits</a>
                            <a>Sexual Problems/Bodily Changes</a>
                            <a>Domestic/Marital Issues</a>
                            <a>Financial Difficulties/Living Expenses/Tuition</a>
                            <a>Identity Theft/Fraud/Damages</a>
                            <a>Help &amp; Advice</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Local Info"><span>Local Info</span></a>
                        <div class="menu-sub">
                            <a>New York</a>
                            <a>New Jersey</a>
                            <a>California</a>
                            <a>Washington, DC.</a>
                            <a>Pennsylvania</a>
                            <a>Connecticut</a>
                            <a>Illinois</a>
                            <a>Virginia</a>
                            <a>Massachusetts</a>
                            <a>Georgia</a>
                            <a>Florida</a>
                            <a>Texas</a>
                            <a>Washington</a>
                            <a>Indiana</a>
                            <a>Hawaii</a>
                            <a>Canada</a>
                            <a>@Traveling/Sightseeing</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Live Debate"><span>Live Debate</span></a>
                        <div class="menu-sub">
                            <a>Shopping &amp; Products</a>
                            <a>Entertainment &amp; Arts</a>
                            <a>Sports/Leisure/Games</a>
                            <a>Education &amp; Academics</a>
                            <a>Computer/Intertet</a>
                            <a>Travel/Geography</a>
                            <a>Life &amp; Family</a>
                            <a>Financial &amp; Investment Tips</a>
                            <a>Society &amp; Culture</a>
                            <a>Beauty &amp; Style</a>
                            <a>Health &amp; Medicine</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Sharing"><span>Sharing</span></a>
                        <div class="menu-sub">
                            <a>Lifestyle &amp; Shopping Know-how</a>
                            <a>Health/Dieting/Beauty</a>
                            <a>English Language</a>
                            <a>Study/Career/Education/Research</a>
                            <a>Literature &amp; Arts</a>
                            <a>Views on Happiness</a>
                            <a>Hobbies/Leisure/Travel</a>
                            <a>Knowledge/Trends</a>
                            <a>Dictionary of Terms</a>
                            <a>Entertainment</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Best"><span>Best</span></a>
                    </div>
                    <div class="on">
                        <a href="" title="Good People"><span>Good People</span></a>
                        <div class="menu-sub">
                            <a>Honor Mentors</a>
                            <a>Leader Mentors</a>
                            <a>Nanumis</a>
                            <a>Expert Mentors</a>
                            <a>Partner Mentors</a>
                            <a>Jikimis</a>
                        </div>
                    </div>
                    <div>
                        <a title="My Mentoring"><span>My Mentoring</span></a>
                    </div>
                </div>
            </div>
            <div class="ask-share">
                <a class="btn-ask" title="Ask"><span></span>Ask</a>
                <a class="btn-share" title="Share"><span></span>Share</a>
            </div>
        </nav>

        <div id="container" class="goodpeople clearfix">
            <div class="slider">
                <div class="bx-wrapper">
                    <div class="bx-viewport">
                        <ul class="bxslider clearfix">
                            <li>
                                <ul class="weekly-best clearfix">
                                    <li class="on">
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean02HeyKorean02HeyKorean02</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean02HeyKorean02HeyKorean02</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean02HeyKorean02HeyKorean02</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/11221015153518.jpg">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul class="weekly-best clearfix">
                                    <li class="on">
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean03HeyKorean03HeyKorean03</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean04HeyKorean04HeyKorean04</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean03HeyKorean03HeyKorean03</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean04HeyKorean04HeyKorean04</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean03HeyKorean03HeyKorean03</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean04HeyKorean04HeyKorean04</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean03HeyKorean03HeyKorean03</a>
                                        <a>
                                            <img src="http://store1.heykorean.com/Mentoring/MyMentoring/User/Normal/05141108191785.JPG">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul class="weekly-best clearfix">
                                    <li class="on">
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean04HeyKorean04HeyKorean04</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean02HeyKorean02HeyKorean02</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean02HeyKorean02HeyKorean02</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Gold Star</span>
                                        <a>HeyKorean02HeyKorean02HeyKorean02</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Leaders</a>
                                        <div class="go-mentors">
                                            <a title="View Leaders Mentors" target="_blank">View Leaders Mentors</a>
                                            <a title="What is Leaders Mentor" target="_blank">What is Leaders Mentor</a>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="medal-grade">Bronze Medal</span>
                                        <a>HeyKorean01HeyKorean01HeyKorean01</a>
                                        <a>
                                            <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                        </a>
                                        <a class="weeklybest-cate">Honors</a>
                                        <div class="go-mentors">
                                            <a title="View Honor Mentors" target="_blank">View Honor Mentors</a>
                                            <a title="What is Honor Mentor" target="_blank">What is Honor Mentor</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="bx-controls bx-has-pager bx-has-controls-direction">
                            <div class="bx-pager bx-default-pager">
                                <div class="bx-pager-item">
                                    <a href="" data-slide-index="0" class="bx-pager-link active">1</a>
                                </div>
                                <div class="bx-pager-item">
                                    <a href="" data-slide-index="1" class="bx-pager-link">2</a>
                                </div>
                                <div class="bx-pager-item">
                                    <a href="" data-slide-index="2" class="bx-pager-link">3</a>
                                </div>
                            </div>
                            <div class="bx-controls-direction">
                                <a class="bx-prev" href="">Prev</a>
                                <a class="bx-next" href="">Next</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="best-leader">
                <div class="best-mentor">
                    <h3>Best Mentor</h3>
                    <div class="bestmentor-con">
                        <a><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                        <div class="bestmentor-info">
                            <a>HeyKorean</a>
                            <dl class="bestinfo-com">
                                <dt>Interest</dt>
                                <dd>999+</dd>
                                <dt>Level</dt>
                                <dd>Bronze Medal</dd>
                                <dt>Points</dt>
                                <dd>99999+</dd>
                            </dl>
                        </div>
                        <dl class="bestmentor-rate">
                            <dt>Chosen</dt>
                            <dd style="color:#0096de">999+</dd>
                            <dt>Success Rate</dt>
                            <dd style="color:#ff4800">99.9%</dd>
                            <dt>Total Answers</dt>
                            <dd style="color:#454545">999+</dd>
                        </dl>
                        <p>
                            <a>90년대 초 미국에 와서 현재 뉴욕에 있는 대학 부속 연구소를 운영하고 있으며 강의를 하고 있습니다. 헤이코리안을 수년간 애용해왔고 명예멘토에 뽑히게 된 것을 영광스럽고 감사하게 생각합니다.</a>
                        </p>
                    </div>
                    <a class="more">More</a>
                </div>
                <div class="mentors-wrap">
                    <h3>Leader Mentor</h3>
                    <ul class="clearfix">
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Sam Smith</span></a>
                            <span class="user-cate">이민.비자.이민.비자.이민.비자.이민.비자</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Sam Smith</span></a>
                            <span class="user-cate">이민.비자.이민.비자.이민.비자.이민.비자</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Sam Smith</span></a>
                            <span class="user-cate">이민.비자.이민.비자.이민.비자.이민.비자</span>
                        </li>
                    </ul>
                    <a class="more">More</a>
                </div>
            </div>
            <div class="mentor-section">
                <div class="mentors-wrap">
                    <h3>Nanumis</h3>
                    <ul class="clearfix">
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Jessie J</span></a>
                            <span class="user-cate">부동산.경제.부동산.경제.부동산.경제</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Jessie J</span></a>
                            <span class="user-cate">부동산.경제.부동산.경제.부동산.경제</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Jessie J</span></a>
                            <span class="user-cate">부동산.경제.부동산.경제.부동산.경제</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Jessie J</span></a>
                            <span class="user-cate">부동산.경제.부동산.경제.부동산.경제</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Jessie J</span></a>
                            <span class="user-cate">부동산.경제.부동산.경제.부동산.경제</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Jessie J</span></a>
                            <span class="user-cate">부동산.경제.부동산.경제.부동산.경제</span>
                        </li>
                    </ul>
                    <a class="more">More</a>
                </div>
                <div class="mentors-wrap">
                    <h3>Partner Mentors</h3>
                    <ul class="clearfix">
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Rihanna</span></a>
                            <span class="user-cate">유학생 보험.보험.유학생 보험.보험.유학생 보험.보험.</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Rihanna</span></a>
                            <span class="user-cate">유학생 보험.보험.유학생 보험.보험.유학생 보험.보험.</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Rihanna</span></a>
                            <span class="user-cate">유학생 보험.보험.유학생 보험.보험.유학생 보험.보험.</span>
                        </li>
                    </ul>
                    <a class="more">More</a>
                </div>
            </div>
            <div class="mentor-section">
                <div class="mentors-wrap">
                    <h3>Specialist Mentors</h3>
                    <ul class="clearfix">
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹.경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹.경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹.경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹.경제.머니.뱅킹</span>
                        </li>
                    </ul>
                    <a class="more">More</a>
                </div>
                <div class="mentors-wrap">
                    <h3>Jikimis</h3>
                    <ul class="clearfix">
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹</span>
                        </li>
                        <li>
                            <a class="user-img"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                            <a class="user-id"><span>Justin Bieber</span></a>
                            <span class="user-cate">경제.머니.뱅킹.경제.머니.뱅킹</span>
                        </li>
                    </ul>
                    <a class="more">More</a>
                </div>
            </div>
            <div class="top-wrap" style="float:right;width:180px;">
                <h3>Top</h3>
                <ul>
                    <li>
                        <div class="mentors-top">
                            <div class="thumb">
                                <a><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                                <a class="user-id"><span>Maroon 5</span></a>
                                <span class="user-cate">마더후드.스쿨링</span>
                                <dl>
                                    <dt>Chosen</dt>
                                    <dd class="chosen">999</dd>
                                </dl>
                                <dl>
                                    <dt>Success Rate</dt>
                                    <dd class="success-rate">99.9%</dd>
                                </dl>
                            </div>
                        </div>
                    </li>
                    <li><a>Sam Smith Sam Smith Sam Smith</a></li>
                    <li><a>Jessie J Jessie J Jessie J Jessie J Jessie J</a></li>
                    <li><a>Rihanna</a></li>
                    <li><a>Justin Bieber</a></li>
                    <li><a>Charlie Puth</a></li>
                    <li><a>Bruno Mars</a></li>
                    <li><a>Shakira</a></li>
                </ul>

            </div>
        </div>
    </div>
<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em>Seoul</em><img class="triangle" src="./images/triangle.png" alt="more location">
        </a>
        <div class="location_con" style="display: none;">
            <div class="container">
                <a title="close" class="p-area-close p-pointer" style="z-index: 100;">
                    <img src="http://cdn.heykorean.com/common/area_popup/locationcon_close.png" alt="close">
                </a>
                <li class="wide">
                    <dl>
                        <dt class="country">United State</dt>
                        <dd class="cancel">
                            <a href="javascript:void(0);" class="btnAreaClose" title="Close"></a>
                        </dd>
                        <dd class="line">
                            <img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif">
                        </dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state">New York City</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4672&amp;rUrl=http://www.heykorean.com" title="New York City">New York City</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4883&amp;rUrl=http://www.heykorean.com" title="Long Island">Long Island</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4865&amp;rUrl=http://www.heykorean.com" title="Binghamton">Binghamton</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4864&amp;rUrl=http://www.heykorean.com" title="Albany">Albany</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state">
                                <a href="http://global.heykorean.com/relation/?AI=4478&amp;rUrl=http://www.heykorean.com" title="New Jersey">New Jersey</a>
                            </dd>
                            <dd class="city">
                                <a href="#"></a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Maryland">Maryland</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4938&amp;rUrl=http://www.heykorean.com" title="Baltimore">Baltimore</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Massachusetts">Massachusetts</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4851&amp;rUrl=http://www.heykorean.com" title="Boston">Boston</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Washington">Washington</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4917&amp;rUrl=http://www.heykorean.com" title="Seattle">Seattle</a>
                            </dd>
                        </dl></div>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dd class="state" title="Illinois">Illinois</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4956&amp;rUrl=http://www.heykorean.com" title="Chicago">Chicago</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Georgia">Georgia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4919&amp;rUrl=http://www.heykorean.com" title="Atlanta">Atlanta</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="California">California</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4850&amp;rUrl=http://www.heykorean.com" title="San Francisco">San Francisco</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4849&amp;rUrl=http://www.heykorean.com" title="Los Angeles">Los Angeles</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="District of Columbia">District of Columbia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4922&amp;rUrl=http://www.heykorean.com" title="Washington D.C./Northern Virginia">Washington D.C./<br>Northern Virginia</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Pennsylvania">Pennsylvania</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4852&amp;rUrl=http://www.heykorean.com" title="Philadelphia">Philadelphia</a>
                            </dd>
                        </dl></div>
                </li>
                <li class="wide">
                    <dl>
                        <dt class="country" title="Asia Pacific">Asia Pacific</dt>
                        <dd class="line"><img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif"></dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state" title="Korea">Korea</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4822&amp;rUrl=http://www.heykorean.com" title="Seoul">Seoul</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4973&amp;rUrl=http://www.heykorean.com" title="Gyeongju">Gyeongju</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Vietnam">Vietnam</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4976&amp;rUrl=http://www.heykorean.com" title="Bac Ninh">Bac Ninh</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4914&amp;rUrl=http://www.heykorean.com" title="Hanoi">Hanoi</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4963&amp;rUrl=http://www.heykorean.com" title="Ho Chi Minh">Ho Chi Minh</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="China">China</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4882&amp;rUrl=http://www.heykorean.com" title="Beijing">Beijing</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl></div>
                </li>
            </div>
        </div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html">
            <a href="http://www.heykorean.com/hk_club/club_main.asp" title="club">club</a>
            <a href="http://www.heykorean.com/hkboard/room/rent_main.asp" title="housing">housing</a>
            <a href="http://www.heykorean.com/hk_job/index.asp" title="jobs">jobs</a>
            <a href="http://mentor.heykorean.com/" title="mentoring">mentoring</a>
            <a href="http://ktown.heykorean.com" title="yellowpage">yellowpage</a>
            <a class="more" title="more" href="#">more</a>
            <div class="more_wrap">
                <ul>
                    <li class="first"><a href="http://www.heykorean.com/hk_funtalk/">life</a></li>
                    <li><a href="http://www.heykorean.com/hk_news/">NEWS</a></li>
                    <li><a href="http://www.heykorean.com/hkinfo/infoview.asp">INFOPLAZA</a></li>
                    <li><a href="http://www.heykorean.com/hk_service/membership/">memberPlaza</a></li>
                    <li><a href="http://www.heykorean.com/hk_photoessay/">PhotoEssay</a></li>
                    <li class="last"><a href="javascript:openPopup('http://www.heykorean.com/hk_help/sitemap.asp',940,845)">All</a></li>
                </ul>
            </div>
            <div class="lang">
                <a title="english"><img src="./images/flag_en.gif"></a>
                <a title="korean"><img src="./images/flag_ko.gif"></a>
            </div>
        </div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>
                    <span>USA</span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
							0<span>Bxs</span>
							0<span>Bts</span>
							0<span>Dps</span>
						</a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <nav class="clearfix">
            <h2 class="hidden">heykorean mentoring menu</h2>
            <div class="menu-wrap">
                <div id="menu" class="clearfix">
                    <div class="on"><a title="Home"><span>Home</span></a></div>
                    <div>
                        <a href="" title="US Life">
                            <span>US Life</span>
                        </a>
                        <div class="menu-sub">
                            <a>Immigration/Visa/Passport</a>
                            <a>School/Study Abroad/Academics</a>
                            <a>Shopping/Tips & Products</a>
                            <a>Economics, Money &amp; Banking</a>
                            <a>Beauty &amp; Style</a>
                            <a>Computer/Notebook/Videos</a>
                            <a>Food &amp; Restaurants</a>
                            <a>Entertainment/Performance/Arts</a>
                            <a>Sports/Leisure/Tournaments &amp; Competitions</a>
                            <a>Health &amp; Medicine</a>
                            <a>Laws, Society &amp; Culture</a>
                            <a>Motherhood &amp; Schooling</a>
                            <a>Lifestyle Info.</a>
                            <a>Traveling/Sightseeing</a>
                            <a>Other</a>
                            <a>HeyKorean</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="advice"><span>advice</span></a>
                        <div class="menu-sub">
                            <a>School &amp; Academy Selection</a>
                            <a>Studies, Major &amp; Career</a>
                            <a>Identification, Sojourn &amp; Legal Help</a>
                            <a>English Proficiency/Racial Discrimination</a>
                            <a>Working Life</a>
                            <a>Dating &amp; Marriage</a>
                            <a>Friends &amp; Relationships</a>
                            <a>Appearance &amp; Health</a>
                            <a>Depression &amp; Mental Illness</a>
                            <a>Living/Enstrangement/Invasion of Privacy</a>
                            <a>Personality &amp; Habits</a>
                            <a>Sexual Problems/Bodily Changes</a>
                            <a>Domestic/Marital Issues</a>
                            <a>Financial Difficulties/Living Expenses/Tuition</a>
                            <a>Identity Theft/Fraud/Damages</a>
                            <a>Help &amp; Advice</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Local Info"><span>Local Info</span></a>
                        <div class="menu-sub">
                            <a>New York</a>
                            <a>New Jersey</a>
                            <a>California</a>
                            <a>Washington, DC.</a>
                            <a>Pennsylvania</a>
                            <a>Connecticut</a>
                            <a>Illinois</a>
                            <a>Virginia</a>
                            <a>Massachusetts</a>
                            <a>Georgia</a>
                            <a>Florida</a>
                            <a>Texas</a>
                            <a>Washington</a>
                            <a>Indiana</a>
                            <a>Hawaii</a>
                            <a>Canada</a>
                            <a>@Traveling/Sightseeing</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Live Debate"><span>Live Debate</span></a>
                        <div class="menu-sub">
                            <a>Shopping &amp; Products</a>
                            <a>Entertainment &amp; Arts</a>
                            <a>Sports/Leisure/Games</a>
                            <a>Education &amp; Academics</a>
                            <a>Computer/Intertet</a>
                            <a>Travel/Geography</a>
                            <a>Life &amp; Family</a>
                            <a>Financial &amp; Investment Tips</a>
                            <a>Society &amp; Culture</a>
                            <a>Beauty &amp; Style</a>
                            <a>Health &amp; Medicine</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Sharing"><span>Sharing</span></a>
                        <div class="menu-sub">
                            <a>Lifestyle &amp; Shopping Know-how</a>
                            <a>Health/Dieting/Beauty</a>
                            <a>English Language</a>
                            <a>Study/Career/Education/Research</a>
                            <a>Literature &amp; Arts</a>
                            <a>Views on Happiness</a>
                            <a>Hobbies/Leisure/Travel</a>
                            <a>Knowledge/Trends</a>
                            <a>Dictionary of Terms</a>
                            <a>Entertainment</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Best"><span>Best</span></a>
                    </div>
                    <div>
                        <a href="" title="Good People"><span>Good People</span></a>
                        <div class="menu-sub">
                            <a>Honor Mentors</a>
                            <a>Leader Mentors</a>
                            <a>Nanumis</a>
                            <a>Expert Mentors</a>
                            <a>Partner Mentors</a>
                            <a>Jikimis</a>
                        </div>
                    </div>
                    <div>
                        <a title="My Mentoring"><span>My Mentoring</span></a>
                    </div>
                </div>
            </div>
            <div class="ask-share">
                <a class="btn-ask" title="Ask"><span></span>Ask</a>
                <a class="btn-share" title="Share"><span></span>Share</a>
            </div>
        </nav>
        <div class="mentoring-notice clearfix">
            <div class="location">
                <a>Home</a> &gt;
                <a><strong>MyMentoring</strong></a>
            </div>
        </div>

        <div id="container">
            <div class="mymentoring-main">
                <h2 class="hidden">heykorean mymentoring contents</h2>
                <div class="my-info">
                    <img src="./images/no_img.gif" class="my-pic">
                    <strong>닉네임 없음</strong>
                    <dl class="my-info01">
                        <dt>Level</dt>
                        <dd>Skull</dd>
                        <dt>Rnaking</dt>
                        <dd>999+위</dd>
                        <dt>Points</dt>
                        <dd>999+</dd>
                    </dl>
                    <dl class="my-info02">
                        <dt class=""><span></span>쪽지</dt>
                        <dd>0</dd>
                        <dt class=""><span></span>MyMentee</dt>
                        <dd>0</dd>
                    </dl>
                </div>
                <div class="infobox01">
                    <p class="info-title">HeyKorean님을 소개해보세요.</p>
                    <p class="info-con">안녕하세요<br>HeyKorean님의 마이멘토링입니다.</p>
                    <dl>
                        <dt><strong>Location : </strong></dt>
                        <dd>활동 지역을 설정해주세요.</dd>
                        <dt><strong>채택을 기다리는 답변 : </strong></dt>
                        <dd>0</dd>
                    </dl>
                </div>
                <div class="infobox02">
                    <div class="mylevel-wrap my-page">
                        <h3>답변활동 그래프</h3>
                        <div class="progress-wrap">
                            <div class="progress-bar" style="width:35%"></div>
                            <div class="tooltip" style="left:35%">
                                채택률 (35%)
                            </div>
                        </div>
                    </div>
                    <div class="txt-rate">
                        <div>
                            <span style="width:35%">1</span>
                            <span>999+</span>
                        </div>
                        <div>
                            <span style="width:35%">채택답변수</span>
                            <span>전체답변수</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mymentoring-bottom clearfix">
                <div class="mymentoring-lnb">
                    <a title="My Answers">My Answers <span>(999+)</span></a>
                    <a title="My Questions">My Questions <span>(999+)</span></a>
                    <a title="My Sharing">My Sharing <span>(999+)</span></a>
                    <a title="Mentors &amp; Mentees">Mentors &amp; Mentees</a>
                    <a title="My Profile">My Profile</a>
                </div>
                <div class="mymentoring-con tab-style02">
                    <h3>나의 멘토 &amp; 멘티</h3>
                    <div class="tab-title clearfix">
                        <a href="#tab01" title="나의 멘토들" class="on">나의 멘토들</a>
                        <a href="#tab02" title="나의 멘티들">나의 멘티들</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab01">
                            <span class="total-mentor">전체 (0)</span>
                            <div class="myanswer-select">
                                <select>
                                    <option>전체보기</option>
                                    <option>명예멘토</option>
                                    <option>리더멘토</option>
                                </select>
                                <span>총 <strong>0명</strong>이 있습니다.</span>
                            </div>
                            <div class="mymentoring-table">
                                <table>
                                    <caption>mymentoring USlife list</caption>
                                    <colgroup>
                                        <col style="*">
                                        <col style="width:10%">
                                        <col style="width:10%">
                                        <col style="width:13%">
                                        <col style="width:10%">
                                        <col style="width:13%">
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th scope="row">Subject</th>
                                        <th scope="row">Division</th>
                                        <th scope="row">Directory</th>
                                        <th scope="row">Status</th>
                                        <th scope="row">Good</th>
                                        <th scope="row">Newest</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a>매일 울리는 벨벨벨 이젠 나를 배려 해줘 배터리 낭비하긴 싫어 자꾸만 봐 자꾸 자꾸만 와 전화가 펑 터질 것만 같아</a>
                                        </td>
                                        <td>US Life</td>
                                        <td>Lifestyle</td>
                                        <td></td>
                                        <td>999+</td>
                                        <td>Dec 31, 2016</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="metoring-paging">
                                <a class="first" title="first page"><span></span><em class="p-first">처음</em></a>
                                <div class="pagenum">
                                    <a class="on">1</a>
                                    <a>2</a>
                                    <a>3</a>
                                    <a>4</a>
                                    <a>5</a>
                                    <a>6</a>
                                    <a>7</a>
                                    <a>8</a>
                                    <a>9</a>
                                    <a>10</a>
                                </div>
                                <a class="next"><span></span></a>
                                <a class="last" title="last page"<em class="p-last">마지막</em><span></span></a>
                            </div>
                        </div>
                        <div id="tab02">
                            <div class="myanswer-select">
                                <select>
                                    <option>전체보기</option>
                                    <option>명예멘토</option>
                                    <option>리더멘토</option>
                                </select>
                                <span>총 <strong>0명</strong>이 있습니다.</span>
                            </div>
                            <div class="mymentoring-table">
                                <table>
                                    <caption>mymentoring USlife list</caption>
                                    <colgroup>
                                        <col style="*">
                                        <col style="width:10%">
                                        <col style="width:10%">
                                        <col style="width:13%">
                                        <col style="width:10%">
                                        <col style="width:13%">
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th scope="row">Subject</th>
                                        <th scope="row">Division</th>
                                        <th scope="row">Directory</th>
                                        <th scope="row">Status</th>
                                        <th scope="row">Good</th>
                                        <th scope="row">Newest</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="6">등록된 게시물이 없습니다.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

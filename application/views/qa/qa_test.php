<?php
$kaim_path = $base_url."/assets/mentor_new"; // common.php 의 상대 경로
?>

<header>
    <?php $this->view('layout/partial_header');?>
</header>
<!-- Best Mentoring -->
<section>
    <div class="container">
        <div class="best-mentoring-wrap clearfix">
            <h3 class="best-mentoring-header">Best Mentoring <a class="more" href="#">more</a></h3>
            <div class="list-item clearfix">
                <div class="item law-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
                    <div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
                        <p>
                            이거 제가 사기당하는 것인가요    
                            제가 포트리에서 교통사고를당했습니다.
                            법원에서 이거제가 사기당하는것인가요
                            포트리 교통사오 법원
                            질문이 있습니다.? 변호사 선임</p>
                        <div class="user-info">
                            <a href="#">
                                <img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt="...">
                                user name
                            </a>
                            <span class="date">Jul 14, 2016</span>
                        </div>
                    </div>
                </div>
                <div class="item banking-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
                    <div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
                        <p>
                            이거 제가 사기당하는 것인가요
                            제가 포트리에서 교통사고를당했습니다.
                            법원에서 이거제가 사기당하는것인가요
                            포트리 교통사오 법원
                            질문이 있습니다.? 변호사 선임</p>
                        <div class="user-info">
                            <a href="#">
                                <img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt="...">
                                user name
                            </a>
                            <span class="date">Jul 14, 2016</span>
                        </div>
                    </div>
                </div>
                <div class="item school-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
                    <div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
                        <p>
                            이거 제가 사기당하는 것인가요
                            제가 포트리에서 교통사고를당했습니다.
                            법원에서 이거제가 사기당하는것인가요
                            포트리 교통사오 법원
                            질문이 있습니다.? 변호사 선임</p>
                        <div class="user-info">
                            <a href="#">
                                <img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt="...">
                                user name
                            </a>
                            <span class="date">Jul 14, 2016</span>
                        </div>
                    </div>
                </div>
                <div class="item travelling-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
                    <div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
                        <p>
                            이거 제가 사기당하는 것인가요
                            제가 포트리에서 교통사고를당했습니다.
                            법원에서 이거제가 사기당하는것인가요
                            포트리 교통사오 법원
                            질문이 있습니다.? 변호사 선임</p>
                        <div class="user-info">
                            <a href="#">
                                <img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt="...">
                                user name
                            </a>
                            <span class="date">Jul 14, 2016</span>
                        </div>
                    </div>
                </div>
                <div class="item visa-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
                    <div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
                        <p>
                            이거 제가 사기당하는 것인가요
                            제가 포트리에서 교통사고를당했습니다.
                            법원에서 이거제가 사기당하는것인가요
                            포트리 교통사오 법원
                            질문이 있습니다.? 변호사 선임</p>
                        <div class="user-info">
                            <a href="#">
                                <img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt="...">
                                user name
                            </a>
                            <span class="date">Jul 14, 2016</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Best Mentoring -->

<!-- Category list -->
<section>
    <div class="container">
        <div class="category-list-wrap clearfix">
            <header>
                <h3>
                    Category
                    <a class="arrow-up-icon" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    </a>
                </h3>
            </header>
            <div class="collapse in" id="collapseExample">
                <ul class="category-list col-4">
                    <?php foreach($all_category as $category): ?>
                        <li><a href="<?php echo $base_url . "qa?fCatSeq=" . $category->code; ?>"><?php echo $category->en_name;?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End Category list -->

<!-- Main content -->
<section>
    <div class="container">
        <!-- Left content -->
        <div class="main-content home" id="main">
            <!-- Header: Toolbar -->
            <h3>List All</h3>
            <div class="toolbar-wrap clearfix">
                <div class="search-wrap">
                    <div class="select-btn btn-group">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            option <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Option 1</a></li>
                            <li><a href="#">Option 2</a></li>
                            <li><a href="#">Option 3</a></li>
                            <li><a href="#">Option 4</a></li>
                        </ul>
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
					      <span class="input-group-btn">
					        <button class="btn btn-search" type="button"></button>
					      </span>
                    </div>
                    <a href="#" class="help-icon"></a>
                </div>
            </div>
            <!-- End Header: Toolbar -->
            <!-- List item -->
            <div class="list-content-wrap expand-info clearfix">
                <?php foreach($result as $key => $value) { ?>
                <div class="item clearfix">
                    <div class="waterdrop">
                        <img src="<?php echo $base_url ?>assets/mentor_new/images/<?php echo $this->Common_Model->getWaterColor($value->fMB);?>" alt="water-drop">
                        <span class="point"><?php echo $value->fMB;?></span>
                    </div>
                    <div class="item-context">
                        <div class="header">
                            <a href="#" class="category"><?php echo isset($value->fCatSeq) ? $this->Category_Model->get_category_name_by_code($value->fCatSeq): 'N/A';?></a>
                            <a href="<?php echo $base_url . "qa/view/" . $value->fSeq; ?>" class="title"><?php echo strip_tags($this->Common_Model->truncate($value->fTitle, 40));?></a>
                            <span class="comment-counter"><?php echo $this->Qa_Model->get_total_reply($value->fSeq);?></span>
                            <span class="location">US</span>
                        </div>
                        <p>
                            <?php echo $this->Common_Model->truncate(strip_tags($value->fContent), 30);?>
                        </p>
                    </div>
                </div>
                <?php } ?>
            </div>

            <?php if($links) : ?>
<!--                        <a class="btn-first"><span class="hidden">처음목록</span></a>-->
<!--                        <a class="btn-pre"><span class="hidden">이전목록</span></a>-->
                        <?php echo $links; ?>
<!--                        <a class="btn-next"><span class="hidden">다음목록</span></a>-->
<!--                        <a class="btn-last"><span class="hidden">마지막목록</span></a>-->
                    <?php endif;?>

            <!-- <button class="btn btn-more">더 보기 <img src="<?php echo $base_url ?>assets/mentor_new/images/arrow-down.png"></button> -->
            <div class="paging-wrap clearfix">
                <ul>
                    <li class="paging"><a href="#" class="first-page page-button"></a></li>
                    <li class="paging"><a href="#" class="prev-page page-button"></a></li>
                    <li><a class="active" href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                    <li class="paging"><a href="#" class="next-page page-button"></a></li>
                    <li class="paging"><a href="#" class="last-page page-button"></a></li>
                </ul>
            </div>
            <!-- End List item -->
        </div>
        <!-- End Left content -->
        <!-- Right content -->
        <aside>
            <div class="button-wrap clearfix">
                <a href="<?php echo $base_url . "qa/write";?>"><button class="btn btn-orange btn-ask"><img src="<?php echo $base_url ?>assets/mentor_new/images/question-icon-w.png" alt="..."> ask</button></a>
                <a href="<?php echo $base_url . "sharing/write";?>"><button class="btn btn-blue btn-share"><img src="<?php echo $base_url ?>assets/mentor_new/images/comment-icon-w.png" alt="..."> share</button></a>
            </div>
            <!-- Keyword block -->
            <div class="keyword-wrap aside-block clearfix">
                <div class="header">
                    <h3>best keyword</h3>
                    <div class="control-block">
                        <a class="arrow-down-icon" role="button"></a>
                        <a class="arrow-up-icon" role="button"></a>
                    </div>
                </div>
                <div class="keyword-list clearfix most-keywords">
                    <ol class="rank-list">
                    <?php foreach($most_popular_keywords as $keyword):?>
                        <li>
                            <a href="">
                                <span class="pos">1</span>
                                마사지
                                <span class="quantity">110</span>
                            </a>
                        </li>
                    <?php endforeach;?>
                    </ol>
                </div>
            </div>
            <!-- End Keyword block -->

            <!-- Ads block -->
            <div class="ads-img clearfix">
                <div data-ad-code="mt01"></div>
            </div>
            <!-- End Ads block -->
            
            <!-- Leaderboard block -->
            <div class="leader-board-wrap aside-block clearfix">
                <div>
                    <!-- Nav tabs -->
                    <ul class="leader-board-header" role="tablist">
                        <li role="presentation" class="active"><a href="#best-mentors" aria-controls="home" role="tab" data-toggle="tab"><img src="<?php echo $base_url ?>assets/mentor_new/images/crown-icon-b.png"> Best Mentors</a></li>
                        <li role="presentation"><a href="#best-leaders" aria-controls="profile" role="tab" data-toggle="tab">Best Leaders</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active leader-board-content" id="best-mentors">
                            <ol>
                            <?php foreach($best_mentor as $key => $mentor) { ?>
                                <li>
                                    <a href="#" class="clearfix">
                                        <span class="number"><?php echo $key + 1    ;?></span>
                                        <span class="user-img"><img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name"><?php echo $mentor->strUserID;?></span>
							    				<span class="rank">
							    					<img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="<?php echo $base_url ?>assets/mentor_new/images/water-gray.png">
							    					<?php echo $mentor->n4CurrentlyLevelPoint;?>
							    				</span>
						    				</span>
                                    </a>
                                </li>
                            <?php } ?>
                            </ol>
                            <a class="more" href="#">more</a>
                        </div>
                        <div role="tabpanel" class="tab-pane leader-board-content" id="best-leaders">
                            <ol>
                                <?php foreach ($best_leader as $key => $leader) { ?>
                                    <li>
                                    <a href="#" class="clearfix">
                                        <span class="number"><?php echo $key + 1;?></span>
                                        <span class="user-img"><img src="<?php echo $base_url ?>assets/mentor_new/images/user-profile-img.png" alt=""></span>
                                            <span class="user-info">
                                                <span class="user-name"><a href="<?php echo $base_url . "user/myMentor/".$leader->strUserID;?>"><?php echo $leader->strUserID;?></a></span>
                                                <span class="rank">
                                                    <img src="<?php echo $base_url ?>assets/mentor_new/images/silver-icon.png">
                                                    silver
                                                </span>
                                                <span class="point">
                                                    <img src="<?php echo $base_url ?>assets/mentor_new/images/water-gray.png">
                                                    <?php echo $leader->n4CurrentlyLevelPoint;?>
                                                </span>
                                            </span>
                                    </a>
                                </li>
                                <?php }?>
                            </ol>
                            <a class="more" href="#">more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Leaderboard block -->
            <button onclick="openPopup('http://www.heykorean.com/HK_Help/faqs_view.asp?section=7',980,670);" class="btn btn-gray btn-help"><img src="<?php echo $base_url ?>assets/mentor_new/images/question-icon-t.png"> HELP DESK</button>
            <button onclick="openPopup('http://www.heykorean.com/HK_Help/email.asp',980,670);" class="btn btn-dark-blue btn-connect"><img src="<?php echo $base_url ?>assets/mentor_new/images/hey-icon.png"> CONNECT US</button>
            <div class="sponsor-wrap">
                <h3>
                    Sponsor
                </h3>
                <div data-ad-code="mk01"></div>
                <!-- <img src="<?php echo $base_url ?>assets/mentor_new/images/sponsor.png" alt="sponsor"> -->
            </div>
        </aside>
    </div>
</section>
<footer>
    <?php $this->view('layout/partial_footer');?>
</footer>
<!-- End Main content -->
<?php //include_once($kaim_path."/include/js-include.html"); ?>
<?php $this->view('layout/partial_js');?>
<?php //$this->view('layout/partial_footer_js');?>
</body>
</html>
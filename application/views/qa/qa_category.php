<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>
<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring" href="<?php echo $base_url;?>"><!--<img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring">-->Mentoring</a>
                    <span><a class="p-area-title"></a></span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
					<span class="my mymemo">
						<a class="get" title="my memo">0</a>
					</span>
					<span class="my mypage">
						<a class="get" title="my page">5</a>
					</span>
					<span class="my mywaterdrop">
						<a class="get" title="my mywaterdrop">
                            0<span>Bxs</span>
                            0<span>Bts</span>
                            0<span>Dps</span>
                        </a>
					</span>
					<span class="my myhk">
						<a class="get" title="my HK">500</a>
					</span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
            <div class="location">
                <a href="<?php echo $base_url;?>">Home</a> &gt;
                <a href="<?php echo $base_url . "qa/category/" . $catId; ?>"><strong> <?php echo (isset($cat_name) && $cat_name != '') ? $cat_name : 'No data';?></strong></a>
            </div>
            <div class="newly-wrap">
                <span class="announcement">공지</span>
                <p><a href="http://www.heykorean.com/HK_Service/Event/board.asp?mode=view&idx=255&page=1&SearchType=&SearchKeyword=">멘토링을 개편 하였습니다. 멘토링을 개편 하였습니다.</a></p>
            </div>
        </div>

        <div class="top-con clearfix">
            <div class="category-wrap">
                <div class="category-title">
                    <?php echo $cat_name;?>
                </div>
                <div class="newly-wrap">
                    <span class="newly">Newly Posted : </span>
                    <span><?php echo $newly_posted;?></span>
                    <span class="newly">Answered : </span>
                    <span><?php echo $answered;?></span>
                </div>
                <ul class="category-con clearfix">
                    <?php if($navi_category) { foreach ($navi_category as $navi):?>
                        <li><a href="<?php echo $base_url . "qa?fCatSeq=" . $navi->code; ?>"><?php echo $navi->en_name;?></a><span>(<?php echo $this->Qa_Model->record_count($navi->code, null, null);?>)</span></li>
<!--                        <li><a href="--><?php //echo $base_url . "qa?fCatSeq=" . $navi->code; ?><!--">--><?php //echo $navi->en_name;?><!--</a><span>(17,794)</span></li>-->
                    <?php endforeach; } ?>
                </ul>
            </div>
            <div class="login-wrap">
                <strong class="my-nickname"><a href="http://www.heykorean.com/HK_Mypage/Myinfo/index.asp" target="_blank">heykorean</a></strong>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a>999+</a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt>Voted As</dt>
                        <dd style="width:65px;"><a>999+</a>  <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a href="<?php echo $base_url . "user/myMentor"; ?>" target="_blank" class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
        </div>
        <div class="most-wrap clearfix" style="width: 200px;float: right;">
            <h2 class="hidden">heykorean lnb</h2>
            <div class="thuydinh">
                <div class="most-keywords">
                    <h3>Most Popular Keywords</h3>
                    <ul class="rank-list">
                        <?php foreach($most_popular_keywords as $keyword):?>
                            <li class="sonha"><span class="ranking"><?php echo $keyword->fCount;?></span><a><?php echo $keyword->fKeyword;?></a><span class="keyword-count"></span></li>
                        <?php endforeach;?>
                    </ul>
                    <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                    <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
                </div>
            </div>
        </div>
        <div id="container" class="mentor-main clearfix" style="clear: both;">
            <!-- 中 -->
            <div class="mentoring-con advice">
                <div class="mentoring-board tab-style02">
                    <a class="more">more</a>
                    <h3 class="hidden">mentoring board</h3>
                    <?php
                    $open = 'none';
                    $class = '';
                    if(!isset($_GET['fTab'])) {
                        $open = 'block';
                        $class = 'on';
                    } else {
                        if(isset($_GET['fTab']) && $_GET['fTab'] == 'open') {
                            $open = 'block';
                            $class = 'on';
                        }
                    }
                    ?>
                    <div class="tab-title clearfix">
                        <a href="<?php echo $base_url . "qa/category/".$catId."?fTab=open"; ?>" title="Open" class="<?php echo $class; ?>">Open</a>
                        <a href="<?php echo $base_url . "qa/category/".$catId."?fTab=voting"; ?>" title="In Voting" class="<?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'voting') ? 'on' : ''; ?>" >In Voting</a>
                        <a href="<?php echo $base_url . "qa/category/".$catId."?fTab=resolved"; ?>" title="Resolved" class="<?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'resolved') ? 'on' : ''; ?>" >Resolved</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab08" style="display:<?php echo $open; ?>">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Waterdrops<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($opening) : foreach($opening as $qa):?>
                                    <tr>
                                        <td class="waterdrop-list <?php echo $this->Common_Model->getWaterColor($qa->fMB);?>"><?php echo $qa->fMB;?></td>
                                        <td><a class="mentor-title" href="<?php echo $base_url . "qa/view/" . $qa->fSeq; ?>"><?php echo $qa->fTitle;?></a><span class="comment-num">[<?php echo $this->Qa_Model->get_total_reply($qa->fSeq);?>]</span></td>
<!--                                        <td><span><a href="">--><?php //echo $this->Common_Model->truncate($qa->fCatSeq, 10);?><!--</a></span></td>-->
                                        <td><span><a href="<?php echo $base_url . "qa/category/" . $qa->fCatSeq; ?>"><?php echo $this->Common_Model->truncate($this->Category_Model->get_category_name_by_code($qa->fCatSeq), 10);?></a></span></td>
<!--                                        <td><span><a href="">--><?php //echo $this->Category_Model->get_category_name_by_code($qa->fCatSeq);?><!--</a></span></td>-->
                                        <td>U.S.A</td>
                                        <td class="mentor-titlecate" data-utc-date="<?php echo date('M d, Y', strtotime($qa->fRegDT));?>"><?php echo date('M d, Y', strtotime($qa->fRegDT));?></td>
                                    </tr>
                                    <?php
                                endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab09" style="display: <?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'voting') ? 'block' : 'none'; ?>">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Waterdrops<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($voting) : foreach($voting as $qa):?>
                                    <tr>
                                        <td class="waterdrop-list <?php echo $this->Common_Model->getWaterColor($qa->fMB);?>"><?php echo $qa->fMB;?></td>
                                        <td><a class="mentor-title" href="<?php echo $base_url . "qa/view/" . $qa->fSeq; ?>"><?php echo $qa->fTitle;?></a><span class="comment-num">[<?php echo $this->Qa_Model->get_total_reply($qa->fSeq);?>]</span></td>
<!--                                        <td><span>--><?php //echo $this->Common_Model->truncate($qa->fCatSeq, 10);?><!--</span></td>-->
                                        <td><span><a href="<?php echo $base_url . "qa/category/" . $qa->fCatSeq; ?>"><?php echo $this->Common_Model->truncate($this->Category_Model->get_category_name_by_code($qa->fCatSeq), 10);?></a></span></td>
                                        <td>U.S.A</td>
                                        <td class="mentor-titlecate"><?php echo date('M d, Y', strtotime($qa->fRegDT));?></td>
                                    </tr>
                                    <?php
                                endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab10" style="display: <?php echo (isset($_GET['fTab']) && $_GET['fTab'] == 'resolved') ? 'block' : 'none'; ?>">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Waterdrops<span></span></th>
                                    <th scope="row">Title<span></span></th>
                                    <th scope="row">Fields</th>
                                    <th scope="row">ID</th>
                                    <th scope="row">Newest</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($resolved) : foreach($resolved as $qa):?>
                                    <tr>
                                        <td class="waterdrop-list <?php echo $this->Common_Model->getWaterColor($qa->fMB);?>"><?php echo $qa->fMB;?></td>
                                        <td><a class="mentor-title" href="<?php echo $base_url . "qa/view/" . $qa->fSeq; ?>"><?php echo $qa->fTitle;?></a><span class="comment-num">[<?php echo $this->Qa_Model->get_total_reply($qa->fSeq);?>]</span></td>
<!--                                        <td><span>--><?php //echo $this->Common_Model->truncate($qa->fCatSeq, 15);?><!--</span></td>-->
                                        <td><span><a href="<?php echo $base_url . "qa/category/" . $qa->fCatSeq; ?>"><?php echo $this->Common_Model->truncate($this->Category_Model->get_category_name_by_code($qa->fCatSeq), 10);?></a></span></td>
                                        <td>U.S.A</td>
                                        <td class="mentor-titlecate"><?php echo date('M d, Y', strtotime($qa->fRegDT));?></td>
                                    </tr>
                                    <?php
                                endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if($links) : ?>
                        <div class="pagewrap">
<!--                            <a class="btn-first"><span class="hidden">처음목록</span></a>-->
<!--                            <a class="btn-pre"><span class="hidden">이전목록</span></a>-->
                            <?php echo $links; ?>
<!--                            <a class="btn-next"><span class="hidden">다음목록</span></a>-->
<!--                            <a class="btn-last"><span class="hidden">마지막목록</span></a>-->
                        </div>
                    <?php endif;?>

                    <div class="mentorlist-search">
                        <!--                        <form action="post">-->
                        <form action="<?php echo $base_url . "qa/category/".$catId; ?>" class="form-horizontal" method="post">
                        <fieldset>
                            <legend>search mentoring list</legend>
                            <?php
                            echo form_dropdown('type_search', $all_search_field, set_value('type_search'), array("style" => 'width: 13%'));
                            ?>
                            <!--                                <input type="search" id="metoringlistSearch" style="width:184px;">-->
                            <input type="text" id="metoringlistSearch" style="width:184px;" name="search_name" value="<?php echo set_value('search_name'); ?>">
                            <label for="metoringlistSearch"><input type="submit" name="submit"  value="검색"></label>
                        </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <!-- //中 -->
            <!-- 右 -->
            <div class="aside">
                <a class="btn-mainask" title="질문하기" href="<?php echo $base_url . "sharing/write"; ?>">ASK</a>
                <div class="most answered-questions advice">
                    <h3>Best Recommended <span>Q&amp;A</span></h3>
                    <ol>
                        <?php foreach ($best_recommended_qa as $key => $qa):?>
                            <li><span class="num"><?php echo ($key + 1);?></span><a href="<?php echo $base_url . "qa/view/" . $qa->fSeq; ?>"><?php echo $qa->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($qa->fSeq);?>]</span></li>
                        <?php endforeach;?>
                    </ol>
                </div>
                <div class="most answered-questions advice">
                    <h3><span>US Life</span> Best Reply 5</h3>
                    <ol>
                        <?php foreach ($us_life_best_reply as $key => $best_reply):?>
                            <li><span class="num"><?php echo ($key + 1);?></span><a href="<?php echo $base_url . "qa/view/" . $best_reply->fSeq; ?>"><?php echo $best_reply->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($best_reply->fSeq);?>]</span></li>
                        <?php endforeach;?>
                    </ol>
                </div>
                <div class="sponsor-ad">
                    <h3>Sponsor <span>Ads</span></h3>
                    <a href="http://www.heykorean.com/HK_Club/HK_Club_main.asp?club_id=10000003" target="_blank" title="뉴욕유학생협회 KSANY 바로가기">
                        <img src="https://s3.amazonaws.com/img.ad.heykorean.com/Advertise/2015/12/04/2OrGtHjvC3Q.gif" alt="뉴욕유학생협회 KSANY 바로가기">
                    </a>
                    <div class="btn-sponsorad">
                        <a href="javascript:void(0)" class="style01" onclick="openPopup('http://www.heykorean.com/HK_Help/faqs_view.asp?section=7',980,670);">FAQ</a>
                        <a href="javascript:void(0)" class="style02" onclick="openPopup('http://www.heykorean.com/HK_Help/email.asp',980,670);">문의/제안</a>
                    </div>
                </div>
            </div>
            <!-- //右 -->
        </div>
    </div>
</div>
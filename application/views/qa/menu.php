<nav class="clearfix">
    <h2 class="hidden">heykorean mentoring menu</h2>
    <div class="menu-wrap">
        <div id="menu" class="clearfix">
            <div class="<?php echo (!isset($catId)) ? 'on' : ''; ?>"><a title="Home" href="/"><span>Home</span></a></div>
            <div class="<?php echo (isset($catId) && $catId == '01') ? 'on' : ''; ?>">
                <a href="<?php echo $base_url . "qa/category/01";?>" title="US Life">
                    <span>US Life</span>
                </a>
                <div class="menu-sub disNone">
                    <?php foreach($us_life as $item):?>
                        <a href="<?php echo $base_url . "qa?fCatSeq=" . $item->code; ?>"><?php echo $item->en_name;?></a>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="<?php echo (isset($catId) && $catId == '02') ? 'on' : ''; ?>">
                <a href="<?php echo $base_url . "qa/category/02";?>" title="advice"><span>advice</span></a>
                <div class="menu-sub disNone">
                    <?php foreach($advice as $item):?>
                        <a href="<?php echo $base_url . "qa?fCatSeq=" . $item->code; ?>"><?php echo $item->en_name;?></a>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="<?php echo (isset($catId) && $catId == '03') ? 'on' : ''; ?>">
                <a href="<?php echo $base_url . "qa/category/03";?>" title="Local Info"><span>Local Info</span></a>
                <div class="menu-sub disNone">
                    <?php foreach($local_info as $item):?>
                        <a href="<?php echo $base_url . "qa?fCatSeq=" . $item->code; ?>"><?php echo $item->en_name;?></a>
                    <?php endforeach;?>
                </div>
            </div>
<!--            <div>-->
<!--                <a href="--><?php //echo $base_url . "qa/category/04";?><!--" title="Live Debate"><span>Live Debate</span></a>-->
<!--                <div class="menu-sub disNone">-->
<!--                    --><?php //foreach($live_debate as $item):?>
<!--                        <a href="--><?php //echo $base_url . "qa?fCatSeq=" . $item->code; ?><!--">--><?php //echo $item->en_name;?><!--</a>-->
<!--                    --><?php //endforeach;?>
<!--                </div>-->
<!--            </div>-->
            <div class="<?php echo (isset($catId) && $catId == '05') ? 'on' : ''; ?>">
                <a href="<?php echo $base_url . "sharing";?>" title="Sharing"><span>Sharing</span></a>
                <div class="menu-sub disNone">
                    <?php foreach($sharing as $item):?>
                        <a href="<?php echo $base_url . "sharing?fCatSeq=" . $item->code; ?>"><?php echo $item->en_name;?></a>
                    <?php endforeach;?>
                </div>
            </div>
            <div>
                <a href="<?php echo $base_url . "qa/best";?>" title="Best"><span>Best</span></a>
            </div>
            <div>
                <a title="My Mentoring" href="<?php echo $base_url . "user/myMentor";?>"><span>My Mentoring</span></a>
            </div>
        </div>
    </div>
    <div class="ask-share">
        <a class="btn-ask" href="<?php echo $base_url . "qa/write"; ?>" title="Ask"><span></span>Ask</a>
        <a class="btn-share" href="<?php echo $base_url . "sharing/write"; ?>" title="Share"><span></span>Share</a>
    </div>
</nav>
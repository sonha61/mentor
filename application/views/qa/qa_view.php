<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em><img src="<?php echo $base_url ?>/images/loader/loader_area_name.gif" alt=""></em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/common/s_gnb.html" style="display: none;"></div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({
                        pageLanguage: 'en',
                        includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW',
                        layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                        multilanguagePage: true
                    }, 'google_translate_element');
                }
            </script>
            <script type="text/javascript"
                    src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="<?php echo $base_url;?>/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html"></div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header" class="clearfix">
            <div class="title-wrapper">
                <h1><a href="http://www.heykorean.com/" title="heykorean"><img
                            src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
<!--                <a class="service-title" title="mentoring"><img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring"></a>-->
                <a class="service-title" title="mentoring" href="<?php echo $base_url;?>"><!--<img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring">-->Mentoring</a>
                <span><a class="p-area-title"></a></span>
            </div>
            <div class="search-box">
                <form method="post">
                    <fieldset>
                        <legend>search</legend>
                        <input type="search" id="containerSearch" placeholder="Find an answer">
                        <label for="containerSearch">
                            <button type="submit">Search</button>
                        </label>
                    </fieldset>
                </form>
            </div>
            <!--
            <div class="etc-wrapper">
                <div class="top-logwrap login">
                    <span class="my mymemo">
                        <a class="get" title="my memo">0</a>
                    </span>
                    <span class="my mypage">
                        <a class="get" title="my page">5</a>
                    </span>
                    <span class="my mywaterdrop">
                        <a class="get" title="my mywaterdrop">
                            0<span>Bxs</span>
                            0<span>Bts</span>
                            0<span>Dps</span>
                        </a>
                    </span>
                    <span class="my myhk">
                        <a class="get" title="my HK">500</a>
                    </span>
                </div>
                <span class="korean lang"><span class="flag"></span><span>Korean</span></span>
            </div>
             -->
        </div>
        <?php $this->view('qa/menu');?>
        <div class="mentoring-notice clearfix">
            <div class="location" style="width: 76%;">
<!--                --><?php //echo $this->Common_Model->breadcrumbs($qa_info->fCatSeq);?>
                <?php if($qa_info->fCatSeq) { echo $this->Common_Model->createPath($qa_info->fCatSeq);} else { echo '<a href="'.$base_url.'">Home</a>'; } ?>
            </div>
            <div class="newly-wrap">
                <span class="announcement">공지</span>
                <p><a href="http://www.heykorean.com/HK_Service/Event/board.asp?mode=view&idx=255&page=1&SearchType=&SearchKeyword=">멘토링을 개편 하였습니다. 멘토링을 개편 하였습니다.</a></p>
            </div>
        </div>
        <div class="aside">
            <div class="login-wrap">
                <!-- logout -->
                <!--
                <form action="post">
                    <fieldset>
                        <legend>heykorean login</legend>
                        <div class="login-input">
                            <h3>HeyKorean Login</h3>
                            <input type="text" placeholder="ID">
                            <input type="password">
                        </div>
                        <div class="login-save">
                            <label><input type="checkbox">Secured</label>
                            <label><input type="checkbox">SaveID</label>
                            <button type="submit">Log In</button>
                        </div>
                        <div class="signup-wrap">
                            <button type="submit">Sign up</button>
                            <a href="http://www.heykorean.com/HK_Member/en_id_pass_k.asp">Forgot id/password</a>
                        </div>
                    </fieldset>
                </form>
                 -->
                <!-- //logout -->
                <!-- login -->
                <a href="http://www.heykorean.com/HK_Mypage/Myinfo/index.asp"><strong class="my-nickname"></strong></a>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a class="memonew"></a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt style="width:58px;">Voted As</dt>
                        <dd style="width:59px;"><a>999+</a> <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a href="<?php echo $base_url . "user/myMentor"; ?>" target="_blank" class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
            <a class="btn-mainask" title="질문하기" href="<?php echo $base_url . "qa/write"; ?>">ASK</a>
            <div class="most answered-questions">
                <h3>Most Answered <span>Questions</span></h3>
                <ol>
                    <?php foreach ($most_answered as $key => $value):?>
                        <li><span class="num"><?php echo $key + 1;?></span><a href="<?php echo $base_url . "qa/view/" . $value->fBSeq; ?>"><?php echo $value->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($value->fBSeq);?>]</span></li>
                    <?php endforeach;?>
                </ol>
                <a class="more" href="<?php echo $base_url . "qa/index/1/?fTab=resolved";?>">more</a>
            </div>
            <div class="most recommended-sharing">
                <h3>Most Recommended <span>Sharing</span></h3>
                <ul>
                    <?php foreach ($most_recommended_sharing as $sharing):?>
                        <li><a href="<?php echo $base_url . "sharing/view/" . $sharing->fSeq; ?>"><?php echo $sharing->fTitle;?></a></li>
                    <?php endforeach;?>
                </ul>
                <a class="more" href="<?php echo $base_url . "sharing";?>">more</a>
            </div>
            <div class="most">
                <h3>Today's best <span>Advice</span></h3>
                <ul>
                    <?php foreach ($today_best_advice as $advice):?>
                        <li>
                            <a href="<?php echo $base_url . "qa/view/" . $advice->fSeq; ?>"><?php echo $advice->fTitle;?></a>
                            <span class="comment">[<?php echo $this->Qa_Model->get_total_reply($advice->fSeq);?>]</span>
                        </li>
                    <?php endforeach;?>
                </ul>
                <a class="more" href="<?php echo $base_url . "qa/category/01";?>">more</a>
            </div>
            <div class="most">
                <h3>Most Popular Local <span>Questions</span></h3>
                <ul>
                    <?php foreach ($most_popular_local as $popular):?>
                        <li><a href="<?php echo $base_url . "qa/view/" . $popular->fBSeq; ?>"><?php echo $popular->fTitle;?></a><span class="comment">[<?php echo $this->Qa_Model->get_total_reply($popular->fBSeq);?>]</span></li>
                    <?php endforeach;?>
                </ul>
                <a class="more" href="<?php echo $base_url . "qa/category/01";?>">more</a>
            </div>
        </div>
        <div id="container" class="sub">
            <div class="qa-box">
                <h3 class="hidden">질문글</h3>
                <div class="qa-title question-title">
                    <p><?php echo $qa_info->fTitle;?><span class="waterdrop-list waterdrops01"><?php echo $qa_info->fMB;?></span></p>
                    <span class="writer"><a><?php echo $qa_info->fUserID;?></a></span>
                    <!-- <span class="writer">-Private-</span> --><!-- 비공개 -->
<!--                    <span class="date">Dec 31, 2016 11:59 PM </span>-->
                    <span class="date"  data-utc-date="<?php echo date('M d, Y h:i A', strtotime($qa_info->fRegDT));?>"><?php echo date('M d, Y h:i A', strtotime($qa_info->fFirstRegDT));?></span>
                    <span class="area">[NY]</span>
                    <span class="hits">Hits <strong><?php echo $qa_info->fHit;?></strong></span>
                </div>
                <a class="btn-helpful" onclick="addVote(<?php echo $qa_info->fSeq;?>,'add', 'qa')">
                    <span>Very Helpful</span>
                    <span class="count" id="fRecommend"><?php echo $qa_info->fRecommend;?></span>
                </a>
                <div class="replied"><span>Replied <strong><?php echo $count_reply;?></strong></span></div>

                <div id="loadingmessage" style="display: none;" role="status" aria-hidden="true">
                    <div id="processMessage">
                        <img src="http://image1.heykorean.com/v2/icon/waitanimation.gif" width="30" height="30" alt="waiting">
                    </div>
                </div>

                <div class="qa-con">
                    <?php echo $qa_info->fContent;?>
                </div>

                <div id="loading_report" style="display: none;" role="status" aria-hidden="true">
                    <div id="processMessage">
                        <img src="http://image1.heykorean.com/v2/icon/waitanimation.gif" width="30" height="30" alt="waiting">
                    </div>
                </div>

                <div class="report-source clearfix">
                    <a onclick="addVote(<?php echo $qa_info->fSeq;?>,'report', 'qa')">Report <span id="fNotRecommend"><?php echo $qa_info->fNotRecommend;?></span>
                    </a>
                </div>
                <p class="earn-points">Giving an answer earns <strong>15 points;</strong></p>
                <p class="earn-points">if your answer is selected, you earn: <strong><?php echo $qa_info->fMB;?></strong> points, <span></span>
                </p>
                <p class="what-about">
                    <a href="javascript:void(0)" onclick="openPopup('http://www.heykorean.com/HK_Help/faqs_view.asp?section=7',980,670);">What are points <img src="<?php echo $base_url ?>assets/images/bg_whatpoints.gif" alt="what are points"></a>
                    <a href="javascript:void(0)" onclick="openPopup('http://www.heykorean.com/HK_Help/faqs_view.asp?section=7',980,670);">About Waterdrops</a>
                </p>
            </div>
            <div class="btn-mentorbox">
                <a class="go-save">Save to my watchlist<img src="<?php echo $base_url ?>assets/images/bg_cabinet.gif"></a>
                <a class="go-list">List<img src="<?php echo $base_url ?>assets/images/bg_golist.gif"></a>
            </div>
            <?php if(validation_errors() != false) { ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo validation_errors(); ?>
                </div>
            <?php } ?>
            <div class="writeanswer-box">
                <form name="writeanswer" method="post" onsubmit="return validateForm()">
                    <fieldset>
                        <legend>글쓰기</legend>
                        <div class="answer-title">
                            <label>
                                <span>Answer</span>
                                <input type="text" name="fTitle" value="re: <?php echo $qa_info->fTitle;?>">
                            </label>
                        </div>
<!--                        <img src="--><?php //echo $base_url ?><!--assets/images/write_editor.jpg" style="width:820px;">-->

<!--                        --><?php //echo $editor;?>
                        <script type="text/javascript" src="<?php echo $base_url ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
                        <script>
                            tinymce.init({
                                selector: "textarea#fContent",
                                entity_encoding : "raw",
                                theme: "modern",
                                width: '.$width.',
                                height: 200,
                                relative_urls : false,
                                remove_script_host: false,
                                plugins: [
                                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                    "save table contextmenu directionality emoticons template paste textcolor"
                                ],
                                setup : function(ed) {
                                    ed.on('keydown', function(e) {
                                        var heycookies = getCookie("heykorean");
                                        if(heycookies == '') {
                                            alert('로그인을 먼저 해주세요.');
                                            var my_editor_id = 'fContent';
                                            tinymce.get(my_editor_id).setContent('');
                                        }
                                    });
                                },
                                content_css: "css/content.css",
                                style_formats: [
                                    {title: "Bold text", inline: "b"},
                                    {title: "Red text", inline: "span", styles: {color: "#ff0000"}},
                                    {title: "Red header", block: "h1", styles: {color: "#ff0000"}},
                                    {title: "Example 1", inline: "span", classes: "example1"},
                                    {title: "Example 2", inline: "span", classes: "example2"},
                                    {title: "Table styles"},
                                    {title: "Table row 1", selector: "tr", classes: "tablerow1"}
                                ]
                            });
                        </script>
                        <textarea name="fContent" id="fContent""></textarea>
                        <div class="source-wrap">
                            <label>
                                <span>Source : </span><input type="text" style="padding:3px;width:771px;" name="fFrom">
                            </label>
                        </div>
                        <div class="chk-private"><label><input type="checkbox" name="fHideID"> -Private-</label></div>
                        <input type="submit" name="submit" class="btn-post">
                    </fieldset>
                </form>
            </div>
            <?php foreach($all_reply as $key => $reply):?>
            <div class="qa-box answer-box">
                <h3 class="hidden">답글</h3>
                <div class="qa-title question-title select01 answertitle">
                    <p><?php echo $reply->fTitle; ?></p>
                    <span class="writer"><a><?php echo ($reply->fHideID == 0) ? $reply->fUserID : 'Private';?></a></span>
                    <span class="date" data-utc-date="<?php echo date('M d, Y h:i A', strtotime($reply->fRegDT));?>"><?php echo date('M d, Y h:i A', strtotime($reply->fRegDT));?></span>

                </div>
                <a class="btn-helpful" onclick="addVoteReply(<?php echo $reply->fSeq;?>,'add', <?php echo $qa_info->fSeq;?>,'qa')">
                    <span>Very Helpful</span>
                    <span class="count" id="fReplyRecommend<?php echo $reply->fSeq;?>"><?php echo ($reply->fRecommend != null || $reply->fRecommend != 0) ? $reply->fRecommend : 0;?></span>
                </a>
                <div class="replied"><span>No. <strong><?php echo $key + 1;?></strong></span></div>
<!--                <div class="thanks-comment">-->
<!--                    <strong>Thanks</strong>-->
<!--                    <p>답변 감사드려요. 도움이 많이 됐습니다! </p>-->
<!--                </div>-->
                <div class="qa-con">
                    <?php echo $reply->fContent; ?>
                </div>
                <div class="report-source clearfix">
                    <div><span>Source</span>
                        <p><?php echo $reply->fFrom;?></p></div>
                    <a onclick="addVoteReply(<?php echo $reply->fSeq;?>,'report', <?php echo $qa_info->fSeq;?>,'qa')">Report <span id="fNotReplyRecommend<?php echo $reply->fSeq;?>"><?php echo ($reply->fNotRecommend != null || $reply->fNotRecommend != 0) ? $reply->fNotRecommend : 0;?></span>
                    </a>
                </div>
                <button type="button" class="btn-comment">comment</button>
                <div class="comment-box">
                    <form method="post" action="" name="writeReply" onsubmit="return validateFormReply()">
                        <fieldset>
                            <legend>write comment</legend>
                            <div class="select-box">
                                <label><input type="radio" name="comment01">None selected</label>
                                <label><input type="radio" name="comment01">Other comments</label>
                                <label><input type="radio" name="comment01">Additional information</label>
                            </div>
                            <div class="write-box">
                                <img src="<?php echo $base_url ?>assets/images/no_img.gif">
                                <textarea id="fComment" rows="10" cols="20" name="fComment"></textarea>
                                <button type="submit" name="replyComment">Comment</button>
                            </div>
                            <input type="hidden" name="replyID"  value="<?php echo $reply->fSeq;?>" >
                            <label><input type="checkbox" name="fHideIDReply">-Private-</label>
                        </fieldset>
                    </form>
                </div>
            </div>
            <?php endforeach;?>
            <div class="btn-mentorbox">
                <a class="go-save">Save to my watchlist<img src="<?php echo $base_url ?>assets/images/bg_cabinet.gif"></a>
                <a class="go-list">List<img src="<?php echo $base_url ?>assets/images/bg_golist.gif"></a>
            </div>
            <div class="otherlist-wrap">
                <div class="other-list">
                    <div class="otherlist-title clearfix">
                        <h3>Unanswered Questions in this Category</h3>
                        <a class="more" href="<?php echo $base_url . "qa?fCatSeq=" . $qa_info->fCatSeq; ?>">more</a>
                    </div>
                    <ul>
                        <?php foreach ($unanswered_questions as $unanswered):?>
                        <li>
						<span class="list-title">
							<a href="<?php echo $base_url . "qa/view/" . $unanswered->fSeq; ?>"><?php echo $unanswered->fTitle;?></a>
							<span>[<?php echo $this->Qa_Model->get_total_reply($unanswered->fSeq);?>]</span>
						</span>
                            <span class="list-area"><?php echo date('M d, Y', strtotime($unanswered->fFirstRegDT));?></span>
                            <span class="list-date">U.S.A</span>
                        </li>
                        <?php endforeach;?>
                    </ul>
                    <a class="more" href="<?php echo $base_url . "qa?fCatSeq=" . $qa_info->fCatSeq; ?>">more</a>
                </div>
                <div class="other-list">
                    <div class="otherlist-title clearfix">
                        <h3>Resolved Questions in this Category</h3>
                        <a class="more" href="<?php echo $base_url . "qa?fCatSeq=" . $qa_info->fCatSeq; ?>">more</a>
                    </div>
                    <ul>
                        <?php if(!empty($resolved_questions)) { foreach ($resolved_questions as $resolved):?>
                        <li>
						<span class="list-title">
							<a href="<?php echo $base_url . "qa/view/" . $resolved->fBSeq; ?>"><?php echo $resolved->fTitle;?></a>
							<span>[<?php echo $this->Qa_Model->get_total_reply($resolved->fBSeq);?>]</span>
						</span>
                            <span class="list-area"><?php echo date('M d, Y', strtotime($resolved->fFirstRegDT));?></span>
                            <span class="list-date">U.S.A</span>
                        </li>
                        <?php endforeach; } ?>
                    </ul>
                    <a class="more" href="<?php echo $base_url . "qa?fCatSeq=" . $qa_info->fCatSeq; ?>">more</a>
                </div>
<!--                <div class="other-list">-->
<!--                    <div class="otherlist-title clearfix">-->
<!--                        <h3>Open Voting for Best Answer</h3>-->
<!--                        <a class="more" href="--><?php //echo $base_url . "qa?fCatSeq=" . $qa_info->fCatSeq; ?><!--">more</a>-->
<!--                    </div>-->
<!--                    <ul>-->
<!--                        <li>-->
<!--						<span class="list-title">-->
<!--							<a>피아노 연습할 수 있는 곳</a>-->
<!--							<span>[999+]</span>-->
<!--						</span>-->
<!--                            <span class="list-area">December 21, 2016</span>-->
<!--                            <span class="list-date">U.S.A</span>-->
<!--                        </li>-->
<!--                        <li>-->
<!--						<span class="list-title">-->
<!--							<a>작은 네일/ 발마사지 가게 홍보 잘 하는 방법은?</a>-->
<!--							<span>[999+]</span>-->
<!--						</span>-->
<!--                            <span class="list-area">December 21, 2016</span>-->
<!--                            <span class="list-date">U.S.A</span>-->
<!--                        </li>-->
<!--                        <li>-->
<!--						<span class="list-title">-->
<!--							<a>jfk공항 파킹 이지패스 질문입니다</a>-->
<!--							<span>[999+]</span>-->
<!--						</span>-->
<!--                            <span class="list-area">December 21, 2016</span>-->
<!--                            <span class="list-date">U.S.A</span>-->
<!--                        </li>-->
<!--                    </ul>-->
<!---->
<!--                </div>-->
            </div>
        </div>
    </div>
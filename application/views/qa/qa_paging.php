<div id="skipNav">
    <a href="#menu"><span>메뉴 바로가기</span></a>
    <a href="#container"><span>본문 바로가기</span></a>
</div>

<div class="s_gnb_wrap" style="height:36px;">
    <div class="s_gnb">
        <a class="btn p-pointer" title="change location">
            <em>Seoul</em><img class="triangle" src="<?php echo $base_url ?>assets/images/triangle.png" alt="more location">
        </a>
        <div class="location_con disNone">
            <div class="container">
                <a title="close" class="p-area-close p-pointer" style="z-index: 100;">
                    <img src="http://cdn.heykorean.com/common/area_popup/locationcon_close.png" alt="close">
                </a>
                <li class="wide">
                    <dl>
                        <dt class="country">United State</dt>
                        <dd class="cancel">
                            <a href="javascript:void(0);" class="btnAreaClose" title="Close"></a>
                        </dd>
                        <dd class="line">
                            <img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif">
                        </dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state">New York City</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4672&amp;rUrl=http://www.heykorean.com" title="New York City">New York City</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4883&amp;rUrl=http://www.heykorean.com" title="Long Island">Long Island</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4865&amp;rUrl=http://www.heykorean.com" title="Binghamton">Binghamton</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4864&amp;rUrl=http://www.heykorean.com" title="Albany">Albany</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state">
                                <a href="http://global.heykorean.com/relation/?AI=4478&amp;rUrl=http://www.heykorean.com" title="New Jersey">New Jersey</a>
                            </dd>
                            <dd class="city">
                                <a href="#"></a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Maryland">Maryland</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4938&amp;rUrl=http://www.heykorean.com" title="Baltimore">Baltimore</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Massachusetts">Massachusetts</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4851&amp;rUrl=http://www.heykorean.com" title="Boston">Boston</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Washington">Washington</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4917&amp;rUrl=http://www.heykorean.com" title="Seattle">Seattle</a>
                            </dd>
                        </dl></div>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dd class="state" title="Illinois">Illinois</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4956&amp;rUrl=http://www.heykorean.com" title="Chicago">Chicago</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Georgia">Georgia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4919&amp;rUrl=http://www.heykorean.com" title="Atlanta">Atlanta</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="California">California</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4850&amp;rUrl=http://www.heykorean.com" title="San Francisco">San Francisco</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4849&amp;rUrl=http://www.heykorean.com" title="Los Angeles">Los Angeles</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="District of Columbia">District of Columbia</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4922&amp;rUrl=http://www.heykorean.com" title="Washington D.C./Northern Virginia">Washington D.C./<br>Northern Virginia</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Pennsylvania">Pennsylvania</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4852&amp;rUrl=http://www.heykorean.com" title="Philadelphia">Philadelphia</a>
                            </dd>
                        </dl></div>
                </li>
                <li class="wide">
                    <dl>
                        <dt class="country" title="Asia Pacific">Asia Pacific</dt>
                        <dd class="line"><img src="http://heyimg.heykorean.com/2013/ktown/img/location_line.gif"></dd>
                    </dl>
                    <div class="locate_list"><dl class="ct_wrap">
                            <dt class="state" title="Korea">Korea</dt>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4822&amp;rUrl=http://www.heykorean.com" title="Seoul">Seoul</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4973&amp;rUrl=http://www.heykorean.com" title="Gyeongju">Gyeongju</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="Vietnam">Vietnam</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4976&amp;rUrl=http://www.heykorean.com" title="Bac Ninh">Bac Ninh</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4914&amp;rUrl=http://www.heykorean.com" title="Hanoi">Hanoi</a>
                            </dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4963&amp;rUrl=http://www.heykorean.com" title="Ho Chi Minh">Ho Chi Minh</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd class="state" title="China">China</dd>
                            <dd class="city">
                                <a href="http://global.heykorean.com/relation/?AI=4882&amp;rUrl=http://www.heykorean.com" title="Beijing">Beijing</a>
                            </dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl><dl class="ct_wrap">
                            <dd></dd>
                        </dl></div>
                </li>
            </div>
        </div>
        <div class="p-translate">
            <div id="google_translate_element">
                <div class="skiptranslate goog-te-gadget" dir="ltr">
                    <div id=":0.targetLanguage" class="goog-te-gadget-simple">
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,es,fr,id,ja,ru,tl,vi,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
                }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <a href="http://www.heykorean.com/help/login_proc.asp?ret=http%3A%2F%2Fmarket.heykorean.com%2F" class="btn_log">LOGIN</a>
        <div class="menu" data-load="/proxy/?url=http://heyhtml.heykorean.com/assets/en-us/market/indicator.html">
            <a href="http://www.heykorean.com/hk_club/club_main.asp" title="club">club</a>
            <a href="http://www.heykorean.com/hkboard/room/rent_main.asp" title="housing">housing</a>
            <a href="http://www.heykorean.com/hk_job/index.asp" title="jobs">jobs</a>
            <a href="http://mentor.heykorean.com/" title="mentoring">mentoring</a>
            <a href="http://ktown.heykorean.com" title="yellowpage">yellowpage</a>
            <a class="more" title="more" href="#">more</a>
            <div class="more_wrap">
                <ul>
                    <li class="first"><a href="http://www.heykorean.com/hk_funtalk/">life</a></li>
                    <li><a href="http://www.heykorean.com/hk_news/">NEWS</a></li>
                    <li><a href="http://www.heykorean.com/hkinfo/infoview.asp">INFOPLAZA</a></li>
                    <li><a href="http://www.heykorean.com/hk_service/membership/">memberPlaza</a></li>
                    <li><a href="http://www.heykorean.com/hk_photoessay/">PhotoEssay</a></li>
                    <li class="last"><a href="javascript:openPopup('http://www.heykorean.com/hk_help/sitemap.asp',940,845)">All</a></li>
                </ul>
            </div>
            <div class="lang">
                <a title="english"><img src="<?php echo $base_url ?>assets/images/flag_en.gif"></a>
                <a title="korean"><img src="<?php echo $base_url ?>assets/images/flag_ko.gif"></a>
            </div>
        </div>
    </div>

    <div id="wrap" class="clearfix">
        <div id="header">
            <div class="servicewrapper clearfix">
                <div class="title-wrapper">
                    <h1><a href="http://www.heykorean.com/" title="heykorean"><img src="http://image1.heykorean.com/v2/global/Top/logo.gif" alt="heykorean"></a></h1>
                    <a class="service-title" title="mentoring" href="/"><!--<img src="http://image1.heykorean.com/v2/global/Top/en/ttl_mentoring.gif" title="mentoring">-->Mentoring</a>
                    <span><a href="/en-us">USA</a></span>
                </div>
                <div class="etc-wrapper">
                    <!-- log in 경우 -->
                    <div class="top-logwrap login"><!-- log in 경우 class : login -->
                    <span class="my mymemo">
                        <a class="get" title="my memo">0</a>
                    </span>
                    <span class="my mypage">
                        <a class="get" title="my page">5</a>
                    </span>
                    <span class="my mywaterdrop">
                        <a class="get" title="my mywaterdrop">
                            0<span>Bxs</span>
                            0<span>Bts</span>
                            0<span>Dps</span>
                        </a>
                    </span>
                    <span class="my myhk">
                        <a class="get" title="my HK">500</a>
                    </span>
                    </div>
                    <!-- //log in -->
                    <!-- log out 경우 -->
                    <!--
                    <div class="top-logwrap logout">
                        <span class="my myhk">
                            <span class="hk">HK</span><span class="is">:</span>
                            <a class="get">Get HK</a>
                        </span>
                    </div>
                    -->
                    <!-- //log out -->
                </div>
            </div>
        </div>
        <nav class="clearfix">
            <h2 class="hidden">heykorean mentoring menu</h2>
            <div class="menu-wrap">
                <div id="menu" class="clearfix">
                    <div class="on"><a title="Home" href="/"><span>Home</span></a></div>
                    <div>
                        <a href="" title="US Life">
                            <span>US Life</span>
                        </a>
                        <div class="menu-sub disNone">
                            <a href="/advicemain.html">Immigration/Visa/Passport</a>
                            <a href="/advicemain.html">School/Study Abroad/Academics</a>
                            <a href="/advicemain.html">Shopping/Tips & Products</a>
                            <a href="/advicemain.html">Economics, Money &amp; Banking</a>
                            <a href="/advicemain.html">Beauty &amp; Style</a>
                            <a href="/advicemain.html">Computer/Notebook/Videos</a>
                            <a href="/advicemain.html">Food &amp; Restaurants</a>
                            <a href="/advicemain.html">Entertainment/Performance/Arts</a>
                            <a href="/advicemain.html">Sports/Leisure/Tournaments &amp; Competitions</a>
                            <a href="/advicemain.html">Health &amp; Medicine</a>
                            <a href="/advicemain.html">Laws, Society &amp; Culture</a>
                            <a href="/advicemain.html">Motherhood &amp; Schooling</a>
                            <a href="/advicemain.html">Lifestyle Info.</a>
                            <a href="/advicemain.html">Traveling/Sightseeing</a>
                            <a href="/advicemain.html">Other</a>
                            <a href="/advicemain.html">HeyKorean</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="advice"><span>advice</span></a>
                        <div class="menu-sub disNone">
                            <a href="/advicemain.html">School &amp; Academy Selection</a>
                            <a href="/advicemain.html">Studies, Major &amp; Career</a>
                            <a href="/advicemain.html">Identification, Sojourn &amp; Legal Help</a>
                            <a href="/advicemain.html">English Proficiency/Racial Discrimination</a>
                            <a href="/advicemain.html">Working Life</a>
                            <a href="/advicemain.html">Dating &amp; Marriage</a>
                            <a href="/advicemain.html">Friends &amp; Relationships</a>
                            <a href="/advicemain.html">Appearance &amp; Health</a>
                            <a href="/advicemain.html">Depression &amp; Mental Illness</a>
                            <a href="/advicemain.html">Living/Enstrangement/Invasion of Privacy</a>
                            <a href="/advicemain.html">Personality &amp; Habits</a>
                            <a href="/advicemain.html">Sexual Problems/Bodily Changes</a>
                            <a href="/advicemain.html">Domestic/Marital Issues</a>
                            <a href="/advicemain.html">Financial Difficulties/Living Expenses/Tuition</a>
                            <a href="/advicemain.html">Identity Theft/Fraud/Damages</a>
                            <a href="/advicemain.html">Help &amp; Advice</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Local Info"><span>Local Info</span></a>
                        <div class="menu-sub disNone">
                            <a href="/advicemain.html">New York</a>
                            <a href="/advicemain.html">New Jersey</a>
                            <a href="/advicemain.html">California</a>
                            <a href="/advicemain.html">Washington, DC.</a>
                            <a href="/advicemain.html">Pennsylvania</a>
                            <a href="/advicemain.html">Connecticut</a>
                            <a href="/advicemain.html">Illinois</a>
                            <a href="/advicemain.html">Virginia</a>
                            <a href="/advicemain.html">Massachusetts</a>
                            <a href="/advicemain.html">Georgia</a>
                            <a href="/advicemain.html">Florida</a>
                            <a href="/advicemain.html">Texas</a>
                            <a href="/advicemain.html">Washington</a>
                            <a href="/advicemain.html">Indiana</a>
                            <a href="/advicemain.html">Hawaii</a>
                            <a href="/advicemain.html">Canada</a>
                            <a href="/advicemain.html">@Traveling/Sightseeing</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Live Debate"><span>Live Debate</span></a>
                        <div class="menu-sub disNone">
                            <a href="/advicemain.html">Shopping &amp; Products</a>
                            <a href="/advicemain.html">Entertainment &amp; Arts</a>
                            <a href="/advicemain.html">Sports/Leisure/Games</a>
                            <a href="/advicemain.html">Education &amp; Academics</a>
                            <a href="/advicemain.html">Computer/Intertet</a>
                            <a href="/advicemain.html">Travel/Geography</a>
                            <a href="/advicemain.html">Life &amp; Family</a>
                            <a href="/advicemain.html">Financial &amp; Investment Tips</a>
                            <a href="/advicemain.html">Society &amp; Culture</a>
                            <a href="/advicemain.html">Beauty &amp; Style</a>
                            <a href="/advicemain.html">Health &amp; Medicine</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Sharing"><span>Sharing</span></a>
                        <div class="menu-sub disNone">
                            <a href="/advicemain.html">Lifestyle &amp; Shopping Know-how</a>
                            <a href="/advicemain.html">Health/Dieting/Beauty</a>
                            <a href="/advicemain.html">English Language</a>
                            <a href="/advicemain.html">Study/Career/Education/Research</a>
                            <a href="/advicemain.html">Literature &amp; Arts</a>
                            <a href="/advicemain.html">Views on Happiness</a>
                            <a href="/advicemain.html">Hobbies/Leisure/Travel</a>
                            <a href="/advicemain.html">Knowledge/Trends</a>
                            <a href="/advicemain.html">Dictionary of Terms</a>
                            <a href="/advicemain.html">Entertainment</a>
                        </div>
                    </div>
                    <div>
                        <a href="" title="Best"><span>Best</span></a>
                    </div>
                    <div>
                        <a href="" title="Good People"><span>Good People</span></a>
                        <div class="menu-sub disNone">
                            <a href="/advicemain.html">Honor Mentors</a>
                            <a href="/advicemain.html">Leader Mentors</a>
                            <a href="/advicemain.html">Nanumis</a>
                            <a href="/advicemain.html">Expert Mentors</a>
                            <a href="/advicemain.html">Partner Mentors</a>
                            <a href="/advicemain.html">Jikimis</a>
                        </div>
                    </div>
                    <div>
                        <a title="My Mentoring" href=""><span>My Mentoring</span></a>
                    </div>
                </div>
            </div>
            <div class="ask-share">
                <a class="btn-ask" title="Ask"><span></span>Ask</a>
                <a class="btn-share" title="Share"><span></span>Share</a>
            </div>
        </nav>
        <div class="mentoring-notice clearfix">
            <div class="top-notice">
                <h3>Notice</h3>
                <a target="_blank" href="view.html">뉴욕 &amp; 뉴저지 운전면허시험의 모든 것!! 뉴욕 &amp; 뉴저지 운전면허시험의 모든 것!! 뉴욕 &amp; 뉴저지 운전면허시험의 모든 것!!</a>
            </div>
            <div class="newly-wrap">
                <span class="newly">Newly Posted : </span>
                <span>999+</span>
                <span class="newly">Answered : </span>
                <span>999+</span>
            </div>
        </div>

        <div class="top-con clearfix" style="display:none;">
            <div class="search-wrap">
                <h3 class="hidden">heykorean mentoring search</h3>
                <p>Q&amp;A</p>
                <div class="search-con clearfix">
                    <form action="post">
                        <fieldset>
                            <legend></legend>
                            <input type="search" id="containerSearch" placeholder="Find an answer">
                            <label for="containerSearch"><button type="submit">Search</button></label>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="login-wrap">
                <!-- logout -->
                <!--
                <form action="post">
                    <fieldset>
                        <legend>heykorean login</legend>
                        <div class="login-input">
                            <h3>HeyKorean Login</h3>
                            <input type="text" placeholder="ID">
                            <input type="password">
                        </div>
                        <div class="login-save">
                            <label><input type="checkbox">Secured</label>
                            <label><input type="checkbox">SaveID</label>
                            <button type="submit">Log In</button>
                        </div>
                        <div class="signup-wrap">
                            <button type="submit">Sign up</button>
                            <a href="http://www.heykorean.com/HK_Member/en_id_pass_k.asp">Forgot id/password</a>
                        </div>
                    </fieldset>
                </form>
                 -->
                <!-- //logout -->
                <!-- login -->
                <strong class="my-nickname">heykorean</strong>
                <div>
                    <dl>
                        <dt><strong>Memo</strong></dt>
                        <dd><strong><a>999+</a></strong></dd>
                        <dt><strong>1:1</strong></dt>
                        <dd></dd>
                        <dt>My Qs</dt>
                        <dd><a>999+</a></dd>
                        <dt style="width:58px;">Voted As</dt>
                        <dd style="width:59px;"><a>999+</a>  <span>(100%)</span></dd>
                        <dt>Points</dt>
                        <dd><a>999+</a></dd>
                        <dt>Rank</dt>
                        <dd><a>999+</a></dd>
                        <dt>Level</dt>
                        <dd style="width:145px;">
                            <span class="level level06"></span><a>Skull</a>
                        </dd>
                    </dl>
                </div>
                <a class="btn-mymentoring">My Mentoring</a>
                <a class="btn-log">Log out</a>
                <!-- login -->
            </div>
        </div>
        <div class="most-wrap clearfix">
            <h2 class="hidden">heykorean lnb</h2>
            <div class="most-qa clearfix">
                <h3>Most Popular Q&amp;As</h3>
                <ul class="qna-list rank-list">
                    <li class="clearfix">
                        <div>
                            <span class="category">[Dieting]</span>
                            <a href="view.html">한인 PT 트레이너</a>
                            <span class="count-comment">[5]</span>
                        </div>
                        <span class="date">Dec 31, 2016</span>
                    </li>
                    <li class="clearfix">
                        <div>
                            <span class="category">[Permanent Residency/Green Card]</span>
                            <a href="view.html">245(i) 조항에 대해서 알고 싶습니다. 245(i) 조항에 대해서 알고 싶습니다. 245(i) 조항에 대해서 알고 싶습니다.</a>
                            <span class="count-comment">[5]</span>
                        </div>
                        <span class="date">Dec 30, 2016</span>
                    </li>
                    <li class="clearfix">
                        <div>
                            <span class="category">[Dieting]</span>
                            <a href="view.html">한인 PT 트레이너</a>
                            <span class="count-comment">[5]</span>
                        </div>
                        <span class="date">Dec 29, 2016</span>
                    </li>
                    <li class="clearfix">
                        <div>
                            <span class="category">[Dieting]</span>
                            <a href="view.html">한인 PT 트레이너</a>
                            <span class="count-comment">[5]</span>
                        </div>
                        <span class="date">Dec 28, 2016</span>
                    </li>
                </ul>
                <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
            </div>
            <div class="most-keywords">
                <h3>Most Popular Keywords</h3>
                <ul class="rank-list">
                    <li><span class="ranking">1</span><a>h1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1bh1b</a><span class="keyword-count">5</span></li>
                    <li><span class="ranking">22</span><a>플러싱</a><span class="keyword-count">16</span></li>
                    <li><span class="ranking">3</span><a>보톡스</a><span class="keyword-count"></span></li>
                    <li><span class="ranking">44</span><a>뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕뉴욕</a><span class="keyword-count">999+</span></li>
                    <li><span class="ranking">5</span><a>뉴저지뉴저지뉴저지뉴저지뉴저지뉴저지뉴저지뉴저지</a><span class="keyword-count">9</span></li>
                    <li><span class="ranking">66</span><a></a><span class="keyword-count"></span></li>
                    <li><span class="ranking">7</span><a></a><span class="keyword-count"></span></li>
                    <li><span class="ranking">8</span><a></a><span class="keyword-count"></span></li>
                    <li><span class="ranking">9</span><a></a><span class="keyword-count"></span></li>
                </ul>
                <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
            </div>
        </div>
        <div id="container" class="mentor-main clearfix">
            <!-- 左 -->
            <div class="mentor-category">
                <h3>Mentor Q&amp;A</h3>
                <div class="category-list">
                    <a class="<?php echo isset($fCatSeq) ? '' : 'active';?>" href="<?php echo $base_url . "/qa"; ?>"><span>All</span></a>
                    <?php foreach($all_category as $category): ?>
                        <a class="<?php echo (isset($fCatSeq) && $fCatSeq == $category->code) ? 'active' : '';?>" href="<?php echo $base_url . "/qa?fCatSeq=" . $category->code; ?>">
                            <span><?php echo $category->en_name;?></span>
                        </a>
                    <?php endforeach;?>
                </div>

                <a class="nanumi" title="나누미 모집 자세히 보기" style="display:none;">
                    <img src="<?php echo $base_url ?>assets/images/nanumi.gif" alt="열성적인 나누미들을 모집합니다.">
                </a>
                <div class="apply-position" style="display:none;">
                    <h3>Apply for Positions as :</h3>
                    <ul>
                        <li><span></span><a>Apply for Nanumi</a></li>
                        <li><span></span><a>Apply for Jikimi</a></li>
                        <li><span></span><a>Apply for Specialist</a></li>
                        <li><span></span><a>Apply for Partner</a></li>
                    </ul>
                </div>
                <div class="help-contact clearfix">
                    <a class="btn-helpdesk" title="Help Desk">Help Desk</a>
                    <a class="btn-contactus" title="Contact Us">Contact Us</a>
                </div>
            </div>
            <!-- //左 -->
            <!-- 中 -->
            <div class="mentoring-con">
                <div class="mentoring-board tab-style02">
                    <a class="more">more</a>
                    <h3 class="hidden">mentoring board</h3>
                    <div class="tab-title clearfix">
                        <a href="#tab08" title="답변해 주세요!" class="on">답변해 주세요!</a>
                        <a href="#tab09" title="채택해 주세요!">채택해 주세요!</a>
                        <a href="#tab10" title="해결된 질문">해결된 질문</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab08">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:60px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Waterdrops<span></span></th>
                                    <th scope="row">Time of writing<span></span></th>
                                    <th scope="row">Areas</th>
                                    <th scope="row">Fields</th>
                                </tr>
                                </thead>
                                <tbody id="sonha1">
                                <?php foreach($results as $qa):?>
                                    <tr>
                                        <td class="waterdrop-list <?php echo $this->Common_Model->getWaterColor($qa->fMB);?>"><?php echo $qa->fMB;?></td>
                                        <!--
                                            waterdrops01 : gray waterdrops
                                            waterdrops02 : glue waterdrops
                                            waterdrops03 : orange waterdrops
                                        -->
                                        <td><a class="mentor-title" href="<?php echo $base_url . "/qa/view/" . $qa->fSeq; ?>"><?php echo $qa->fTitle;?></a><span class="comment-num">[<?php echo $this->Qa_Model->get_total_reply($qa->fSeq);?>]</span></td>
                                        <td>U.S.A</td>
                                        <td class="mentor-titlecate"><span><?php echo $this->Category_Model->get_category_name_by_code($qa->fCatSeq);?></span></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab09">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:60px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Waterdrops<span></span></th>
                                    <th scope="row">Time of writing<span></span></th>
                                    <th scope="row">Areas</th>
                                    <th scope="row">Fields</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <!--
                                        waterdrops01 : gray waterdrops
                                        waterdrops02 : glue waterdrops
                                        waterdrops03 : orange waterdrops
                                    -->
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[0]</span></td>
                                    <td>NY</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[999+]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>NY</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant? Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="tab10">
                            <table>
                                <colgroup>
                                    <col style="width:80px">
                                    <col style="*">
                                    <col style="width:60px">
                                    <col style="width:100px">
                                </colgroup>
                                <thead>
                                <tr>
                                    <th scope="row">Waterdrops<span></span></th>
                                    <th scope="row">Time of writing<span></span></th>
                                    <th scope="row">Areas</th>
                                    <th scope="row">Fields</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <!--
                                        waterdrops01 : gray waterdrops
                                        waterdrops02 : glue waterdrops
                                        waterdrops03 : orange waterdrops
                                    -->
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[999]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>NY</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant? Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">10</td>
                                    <td><a class="mentor-title" href="view.html">뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요. 뉴저지 데마레스트에서 테니스 레슨 해주실 분 찾아요.</a><span class="comment-num">[5]</span></td>
                                    <td>U.S.A</td>
                                    <td class="mentor-titlecate"><span>Internet Shopping/Price Comparison</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">30</td>
                                    <td><a class="mentor-title" href="view.html">Anyone who knows how to promote a restaurant?</a><span class="comment-num">[0]</span></td>
                                    <td>Seoul</td>
                                    <td class="mentor-titlecate"><span>Restaurant managing &amp; consulting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops03">100</td>
                                    <td><a class="mentor-title" href="view.html">미국 한의대 어떤가요?</a><span class="comment-num">[3]</span></td>
                                    <td>Etc</td>
                                    <td class="mentor-titlecate"><span>Oriental Medicine</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops01">27</td>
                                    <td><a class="mentor-title" href="view.html">Deposit Only가 무슨 뜻일까요ㅠㅠ</a><span class="comment-num">[4]</span></td>
                                    <td>NJ</td>
                                    <td class="mentor-titlecate"><span>Taxes &amp; Accounting</span></td>
                                </tr>
                                <tr>
                                    <td class="waterdrop-list waterdrops02">50</td>
                                    <td><a class="mentor-title" href="view.html">영주권 카드 분실에 대해서 문의드립니다.</a><span class="comment-num">[2]</span></td>
                                    <td>MA</td>
                                    <td  class="mentor-titlecate"><span>Permanent Residency/Green Card</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagewrap">
                        <a class="btn-first"><span class="hidden">처음목록</span></a>
                        <a class="btn-pre"><span class="hidden">이전목록</span></a>
                        <?php echo $links; ?>
                        <a class="btn-next"><span class="hidden">다음목록</span></a>
                        <a class="btn-last"><span class="hidden">마지막목록</span></a>
                    </div>
                    <div class="mentorlist-search">
                        <form action="post">
                            <fieldset>
                                <legend>search mentoring list</legend>
                                <select>
                                    <option>Title</option>
                                    <option>Content</option>
                                    <option>Tag</option>
                                    <option>ID</option>
                                </select>
                                <input type="search" id="metoringlistSearch" style="width:184px;">
                                <label for="metoringlistSearch"><button type="button">검색</button></label>
                            </fieldset>
                        </form>
                    </div>
                    <div class="best-today clearfix">
                        <div class="best-recommended">
                            <h3>Best Recommended <span>Q&amp;A</span></h3>
                            <ul>
                                <li>
                                    <span class="category">[<span>New Jersey</span>]</span>
                                    <a href="view.html">뉴저지 아이들 보험 도와주세요 ㅠㅠ</a>
                                    <span class="count-comment">[5]</span>
                                </li>
                                <li>
                                    <span class="category">[<span>Massachusetts</span>]</span>
                                    <a href="view.html">한국 친구들과 어울리고 싶습니다. 한국 친구들과 어울리고 싶습니다. </a>
                                    <span class="count-comment">[999]</span>
                                </li>
                                <li>
                                    <span class="category">[<span>Massachusetts</span>]</span>
                                    <a href="view.html">뉴욕 운전면허 영주권 </a>
                                    <span class="count-comment">[111]</span>
                                </li>
                                <li>
                                    <span class="category">[<span>Massachusetts</span>]</span>
                                    <a href="view.html">취업이민 3순위 인터뷰 정보 알려주세요</a>
                                    <span class="count-comment">[0]</span>
                                </li>
                            </ul>
                            <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                            <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
                        </div>
                        <div class="today-sharing">
                            <h3>Today's <span>Sharing Knowledge</span></h3>
                            <ul>
                                <li>
                                    <a href="sharingview.html"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                                    <div>
                                        <span><a>Yuhki Kuramoto</a>님 </span>
                                        <span>English Language</span>
                                    </div>
                                    <p><a href="sharingview.html">영어 회화 표현: What are you into? What are you into?</a></p>
                                </li>
                                <li>
                                    <a href="sharingview.html"><img src="<?php echo $base_url ?>assets/images/img_thumb04.jpg"></a>
                                    <div>
                                        <span><a>Andra Day</a>님 </span>
                                        <span>Occupational and Investment Visas</span>
                                    </div>
                                    <p><a href="sharingview.html">영어 회화 표현: What are you into? What are you into?</a></p>
                                </li>
                            </ul>
                            <a class="btn-list btn-listpre"><span class="hidden">이전</span></a>
                            <a class="btn-list btn-listnext"><span class="hidden">다음</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //中 -->
            <!-- 右 -->
            <div class="main-aside">
                <a class="btn-mainask" title="질문하기" href="write.html">ASK</a>
                <a class="main-ad" target="_blank">
                    <img src="http://store1.heykorean.com/Banner/F5FAIX9QL.gif" alt="크사니배너" title="크사니배너">
                </a>
                <div class="best-qna">
                    <h3><span class="hidden">질문답변 BEST</span></h3>
                    <ul>
                        <?php foreach($best_qa as $bqa):?>
                            <li><a target="_blank" href="view.html"><?php echo explode(':',$bqa->fTitle)[1];?></a></li>
                        <?php endforeach;?>
                    </ul>
                    <a class="best-more">더보기</a>
                </div>
                <div class="about-nanumi tab-style01" style="display:none;">
                    <div class="tab-title">
                        <a href="#tab01" title="Honors" class="on">Honors</a>
                        <a href="#tab02" title="Leaders">Leaders</a>
                        <a href="#tab03" title="Nanumis">Nanumis</a>
                        <a href="#tab04" title="Experts">Experts</a>
                        <a href="#tab05" title="Partners">Partners</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab01">
                            <a class="img-thumb">
                                <img src="<?php echo $base_url ?>assets/images/img_thumb02.jpg">
                                <!-- 이미지 미등록시▼ -->
                                <!-- <img src="./images/no_img.jpg"> -->
                            </a>
                            <dl class="info-mentor">
                                <dt class="hidden">Nickname</dt>
                                <dd class="mentor-nickname"><a><strong>ariana grande</strong></a></dd>
                                <dt class="hidden">Category</dt>
                                <dd class="mentor-cate">Immigration/visa/Passport</dd>
                                <dt class="level-points">Level</dt>
                                <dd class="level-points"><span class="level level06"></span>Bronze Medal</dd>
                                <!--
                                    level01 : 해골
                                    level02 : 동별
                                    level03 : 은별
                                    level04 : 금별
                                    level05 : 쌍금별
                                    level06 : 동메달
                                    level07 : 은메달
                                    level08 : 금메달
                                    level09 : 동훈장
                                    level10 : 은훈장
                                    level11 : 금훈장
                                    level12 : 월계관
                                    level13 : 멘토신
                                -->
                                <dt class="level-points">Points</dt>
                                <dd class="level-points">26,307</dd>
                            </dl>
                            <p class="explain-mentor"><a>90년대 초 미국에 와서 현재 뉴욕에 있는 대학 부속 연구소를 운영하고 있으며 강의를 하고 있습니다. 헤...</a></p>
                            <a href="#" class="btn-gomore">더 많은 명예멘토 보러가기<span></span></a>
                        </div>
                        <div id="tab02">
                            <a class="img-thumb">
                                <img src="<?php echo $base_url ?>assets/images/img_thumb01.jpg">
                            </a>
                            <dl class="info-mentor">
                                <dt class="hidden">Nickname</dt>
                                <dd class="mentor-nickname"><a><strong>mamamoo</strong></a></dd>
                                <dt class="hidden">Category</dt>
                                <dd class="mentor-cate">Immigration/visa/Passport</dd>
                                <dt class="level-points">Level</dt>
                                <dd class="level-points"><span class="level level02"></span>Bronze Medal</dd>
                                <dt class="level-points">Points</dt>
                                <dd class="level-points">26,307</dd>
                            </dl>
                            <p class="explain-mentor"><a>월가 전문가들 "美 2차 긴축 6월로 앞당겨질 듯 미국의 3월 통화정책 회의를 닷새 앞두고 월가 전문가의 거의...</a></p>
                            <a href="#" class="btn-gomore">더 많은 리더멘토 보러가기<span></span></a>
                        </div>
                        <div id="tab03">
                            <a class="img-thumb">
                                <img src="<?php echo $base_url ?>assets/images/img_thumb03.jpg">
                            </a>
                            <dl class="info-mentor">
                                <dt class="hidden">Nickname</dt>
                                <dd class="mentor-nickname"><a><strong>zico</strong></a></dd>
                                <dt class="hidden">Category</dt>
                                <dd class="mentor-cate">Immigration/visa/Passport</dd>
                                <dt class="level-points">Level</dt>
                                <dd class="level-points"><span class="level level03"></span>Bronze Medal</dd>
                                <dt class="level-points">Points</dt>
                                <dd class="level-points">26,307</dd>
                            </dl>
                            <p class="explain-mentor"><a>중국 최대 전자상거래 업체인 알리바바가 최근 30억달러를 대출받았다는 보도가 나왔다. 사업을 확장하기 위해서다...</a></p>
                            <a href="#" class="btn-gomore">더 많은 나누미 보러가기<span></span></a>
                        </div>
                        <div id="tab04">
                            <a class="img-thumb">
                                <img src="<?php echo $base_url ?>assets/images/img_thumb04.jpg">
                            </a>
                            <dl class="info-mentor">
                                <dt class="hidden">Nickname</dt>
                                <dd class="mentor-nickname"><a><strong>produce101</strong></a></dd>
                                <dt class="hidden">Category</dt>
                                <dd class="mentor-cate">Immigration/visa/Passport</dd>
                                <dt class="level-points">Level</dt>
                                <dd class="level-points"><span class="level level04"></span>Bronze Medal</dd>
                                <dt class="level-points">Points</dt>
                                <dd class="level-points">26,307</dd>
                            </dl>
                            <p class="explain-mentor"><a>미국 셰일오일 업체들이 저유가 속에서 생존전략 짜기에 분주하다. 추락하는 유가 앞에서 자금 확보에 어려움을...</a></p>
                            <a href="#" class="btn-gomore">더 많은 파트너 멘토 보러가기<span></span></a>
                        </div>
                        <div id="tab05">
                            <a class="img-thumb">
                                <img src="<?php echo $base_url ?>assets/images/img_thumb05.jpg">
                            </a>
                            <dl class="info-mentor">
                                <dt class="hidden">Nickname</dt>
                                <dd class="mentor-nickname"><a><strong>twice</strong></a></dd>
                                <dt class="hidden">Category</dt>
                                <dd class="mentor-cate">Immigration/visa/Passport</dd>
                                <dt class="level-points">Level</dt>
                                <dd class="level-points"><span class="level level05"></span>Bronze Medal</dd>
                                <dt class="level-points">Points</dt>
                                <dd class="level-points">26,307</dd>
                            </dl>
                            <p class="explain-mentor"><a>인공지능 연관 기술은 이미 우리 삶 곳곳에 자리잡고 있다. 대표적인 게 페이스북이다...</a></p>
                            <a href="#" class="btn-gomore">더 많은 파트너 멘토 보러가기<span></span></a>
                        </div>
                    </div>
                </div>
                <div class="about-best tab-style01">
                    <h3 class="hidden">best mentors &amp; best leaders</h3>
                    <div class="tab-title clearfix">
                        <a href="#tab06" title="Best Mentors" class="on">Best Mentors</a>
                        <a href="#tab07" title="Best Leaders">Best Leaders</a>
                    </div>
                    <div class="tab-con">
                        <div id="tab06">
                            <ol>
                                <?php foreach($best_mentor as $mentor ):?>
                                    <li class="clearfix">
                                        <span class="best-num">1</span>
                                        <a class="img-thumb"><img src="<?php echo $base_url ?>assets/images/no_img.gif"></a>
                                        <div class="mento-leaders">
                                            <a class="nickname"><strong><?php echo $mentor->strUserID;?></strong></a>
                                            <dl>
                                                <dt>Level</dt>
                                                <dd class="level level<?php echo str_pad($mentor->n4CurrentlyLevelRank, 2, '0', STR_PAD_LEFT);?>"><span class="hidden">해골</span></dd>
                                                <!--
                                                    level01 : 해골
                                                    level02 : 동별
                                                    level03 : 은별
                                                    level04 : 금별
                                                    level05 : 쌍금별
                                                    level06 : 동메달
                                                    level07 : 은메달
                                                    level08 : 금메달
                                                    level09 : 동훈장
                                                    level10 : 은훈장
                                                    level11 : 금훈장
                                                    level12 : 월계관
                                                    level13 : 멘토신
                                                -->
                                                <dt>Points</dt>
                                                <dd class="best-points"><?php echo $mentor->n4CurrentlyLevelPoint;?></dd>
                                            </dl>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ol>
                        </div>
                        <div id="tab07">
                            <ul>
                                <?php foreach($best_leader as $leader):?>
                                    <li class="clearfix">
                                        <span class="best-num"></span>
                                        <a class="img-thumb"><img src="<?php echo $base_url ?>assets/images/img_thumb01.jpg"></a>
                                        <!--                                    <a class="img-thumb"><img src="--><?php //echo isset($leader->strFileFullURL_profile) ? $leader->strFileFullURL_profile : $base_url.'assets/images/img_thumb01.jpg';?><!--"></a>-->
                                        <div class="mento-leaders">
                                            <a class="nickname"><strong><?php echo $leader->strUserID;?></strong></a>
                                            <dl>
                                                <dt>Level</dt>
                                                <dd class="level level<?php echo str_pad($leader->n4CurrentlyLevelRank, 2, '0', STR_PAD_LEFT);?>"><span class="hidden">해골</span></dd>
                                                <dt>Points</dt>
                                                <dd class="best-points"><?php echo $leader->n4CurrentlyLevelPoint;?></dd>
                                            </dl>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <a class="more">more</a>
                </div>
                <div class="sponsor-ad">
                    <h3>Sponsor <span>Ads</span></h3>
                    <a href="http://www.heykorean.com/HK_Club/HK_Club_main.asp?club_id=10000003" target="_blank" title="뉴욕유학생협회 KSANY 바로가기">
                        <img src="https://s3.amazonaws.com/img.ad.heykorean.com/Advertise/2015/12/04/2OrGtHjvC3Q.gif" alt="뉴욕유학생협회 KSANY 바로가기">
                    </a>
                </div>
            </div>
            <!-- //右 -->
        </div>
    </div>
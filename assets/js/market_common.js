$(document).ready(function(){
    var cookieLanguage = "en";
    var i18n = "en-us";
    getLang = function() {
        cookieLanguage = getCookie("cookieLanguage");
        i18n = getCookie("i18n");
        cookieLanguage = cookieLanguage ? cookieLanguage : "en";
        //alert(i18n);
        if(!i18n || typeof i18n == "undefined" || i18n == "undefined")
            i18n = cookieLanguage == "en" ? "en-us" : "ko-kr";
    }
    $(document).on('click','.s_gnb > .btn', function(){
        $(document).find('.location_con').toggle();
    });

    $('.more_wrap').hide();
    $('.more').on('click', function(){
        $(this).toggleClass('on');
        if($(this).hasClass('on')){
            $('.more_wrap').show();
        }else{
            $('.more_wrap').hide();
        }
    });

    var intval4s_gnb  = setInterval(function() {
        if($.trim($('.location_con').html())) {
            clearInterval(intval4s_gnb);
            var widePop = $(document).find('.wide .ct_wrap');
            if($('.location_con').css('display', 'block')){
                $('.location_con').animate({
                    height:'hide'
                },0);
                for(var i=0; i < widePop.length; i+=5){
                    widePop.slice(i, i+5).wrapAll("<div class='locate_list'></div>");
                }
            }else{
                $('.location_con').animate({
                    height:'show'
                },200);
                widePop.find('div').unwrap();
            }
        }
    },50);


    getMemberInfo = function() {

        $.ajax({
            type: "GET",
            url: "http://ap.heykorean.com/v2/heykorean/member/memberinfo",
            dataType: "jsonp",
            jsonpCallback: "member",
            data: {},
            success: function (json) {
                if(json == null) {
                }
                else {
                    if(json.RESULT) {
                        setCookie("USERID",json.USERID,365);
                        setCookie("USERKEY",json.USERKEY,365);
                        setCookie("USERNAME",json.USERNAME,365);
                        $( ".top-logwrap" ).before("");
                        $( ".top-logwrap" ).before("<a href='http://www.heykorean.com/HK_Mypage/Myinfo/index.asp'>" + json.USERID +"</a>");
                        var mc = json.MEMBER_INFO.MEMO;//+100;
                        var member_info = json.MEMBER_INFO;//+100;
                        // console.log(mc);
                        mc = mc > 99 ? '99+' : mc;
                        $(".p-memo").text(mc);
                        $(".mymemo a").text(mc);

                        var name = json.USERID;
                        $(".p-userid").text(name);
                        $(".myhk a").text(member_info.HK);
                        $(".mypage a").text(member_info.MT);
                        $(".mywaterdrop #spanMB1").text(member_info.MB1);
                        $(".mywaterdrop #spanMB2").text(member_info.MB2);
                        $(".mywaterdrop #spanMB3").text(member_info.MB3);
                        //In View Page
                        // $(".my-nickname").text(name);
                        $(".my-nickname").text('');
                        $(".my-nickname").append('<a href="http://www.heykorean.com/HK_Mypage/Myinfo/index.asp" target="_blank">' + name + '</a>');
                        $(".memonew").text(mc);

                        $(".etc-wrapper").css("display", "block");
                        // jQuery(".btn-ask").text(name);

                        // getLang();

                        $(document).find(".s_gnb .btn_log").text(i18n=="ko-kr"? "ë¡œê·¸ì•„ì›ƒ" :"LOGOUT");// nut logout
                        $(document).find(".s_gnb .btn_log").attr("href", "http://www.heykorean.com/sso/xxo.asp?mode=logout&returnURL="+encodeURIComponent(window.location));
                        $(document).find(".btn-log").attr("href", "http://www.heykorean.com/help/login_proc.asp?ret="+encodeURIComponent(window.location));

                        //alert(jQuery(".user_box").html());
                        var intval = setInterval(function() {

                            if($.trim($(".user_box").html())) {
                                clearInterval(intval);

                                $(document).find(".p-scrap").text(number_format(json.MEMBER_INFO.SCRAP));
                                $(document).find(".p-hk").text(number_format(json.MEMBER_INFO.HK));
                                $(document).find(".p-mb1").text(number_format(json.MEMBER_INFO.MB1));
                                $(document).find(".p-mb2").text(number_format(json.MEMBER_INFO.MB2));
                                $(document).find(".p-mb3").text(number_format(json.MEMBER_INFO.MB3));


                                var ext = json.MARKET.PROFILE_IMG.strRight(".");
                                var path = json.MARKET.PROFILE_IMG.strLeft("."+json.MARKET.PROFILE_IMG.strRight("."));
                                var thumb_url_29 = path+"_29x29."+ext;
                                var thumb_url_99 = path+"_99x99."+ext;

                                //console.log(json.MARKET.PROFILE_IMG);
                                if(!$.trim(json.MARKET.PROFILE_IMG)) {
                                    //jQuery(document).find(".p-my-profile-img-for-market-profile em img").attr("src", "/layout/i.php?i="+encodeURIComponent(json.PROFILE.IMG)+"&w=29&h=29");
                                    //jQuery(document).find(".p-my-profile-img-for-market-comment em img").attr("src", "/layout/i.php?i="+encodeURIComponent(json.PROFILE.IMG)+"&w=100&h=100");
                                    $(document).find(".p-my-profile-img-for-market-profile em img").attr("src", "/images/bg_user.png");
                                    $(document).find(".p-my-profile-img-for-market-comment em img").attr("src", "/images/bg_user.png");
                                }
                                else {
                                    $(document).find(".p-my-profile-img-for-market-profile em img").attr("src", thumb_url_99);
                                    $(document).find(".p-my-profile-img-for-market-comment em img").attr("src", thumb_url_99);
                                }

                                $(document).find(".p-wishlist-count").text("("+(Number(json.MARKET.WISHLIST_COUNT)>999?"999+":json.MARKET.WISHLIST_COUNT)+")");
                                $(document).find(".p-product-count").text("("+(Number(json.MARKET.PRODUCT_COUNT)>999?"999+":json.MARKET.PRODUCT_COUNT)+")");
                            }

                        },1000);

                    }
                    else {
                        $(".p-logon-wrapper").hide();

                        $(document).find(".s_gnb .btn_log").attr("href", "http://www.heykorean.com/help/login_proc.asp?ret="+encodeURIComponent(window.location));
                    }

                }
            }
        });

    }
    getMemberInfo();

    // set language
    $(document).on('click', '.p-set-lang', function(e) {
        var lang = $(this).data('lang');
        var i18n = $(this).data('i18n');
        setLanguage(lang, i18n);
    });

    setLanguage = function(n, i18n) {
        setCookie("cookieLanguage",n,365);  // old
        setCookie("i18n",i18n,365);					// new
        location.reload();
    };
    setLanguage = function(n, i18n) {
        setCookie("cookieLanguage",n,365);  // old
        setCookie("i18n",i18n,365);					// new
        location.reload();
    };
    getCookies = function(cname) {

        var retval = new Array();
        var cookiestring = getCookie(cname);
        var cookies = cookiestring.split("&");
        for(var i =0; i < cookies.length ; i++) {
            var kvp = cookies[i].split('=')
            var kv = {key:decodeURIComponent(kvp[0]), value:Base64.decode(kvp[1].replace(/%3D/,'='))};
            retval[retval.length] = kv;
        }
        return retval;
    }
    getCookieFromCookies = function(cname, cookies) {

        for(var i=0;i<cookies.length;i++) {
            if(cookies[i].key==cname)
                return cookies[i].value;
        }
        return "";
    }

    var cookieLanguage = "en";
    var i18n = "en-us";
    getLang = function() {
        cookieLanguage = getCookie("cookieLanguage");
        i18n = getCookie("i18n");
        cookieLanguage = cookieLanguage ? cookieLanguage : "en";
        //alert(i18n);
        if(!i18n || typeof i18n == "undefined" || i18n == "undefined")
            i18n = cookieLanguage == "en" ? "en-us" : "ko-kr";
    }

    var Base64 = {
        _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;

            input = Base64._utf8_encode(input);

            while (i < input.length) {

                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

            }

            return output;
        },


        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;

            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            while (i < input.length) {

                enc1 = this._keyStr.indexOf(input.charAt(i++));
                enc2 = this._keyStr.indexOf(input.charAt(i++));
                enc3 = this._keyStr.indexOf(input.charAt(i++));
                enc4 = this._keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

            }

            output = Base64._utf8_decode(output);

            return output;

        },

        _utf8_encode: function (string) {
            //string = string.replace(/\r\n/g, "\n");
            var utftext = "";

            if(typeof(string) == "undefined"){
                string = "";
            }

            for (var n = 0; n < string.length; n++) {

                var c = string.charCodeAt(n);

                if (c < 128) {
                    utftext += String.fromCharCode(c);
                }
                else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }

            return utftext;
        },

        _utf8_decode: function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;

            while (i < utftext.length) {

                c = utftext.charCodeAt(i);

                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                }
                else if ((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                }
                else {
                    c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }

            }

            return string;
        }

    }

    // str_replace function prototype
    String.prototype.str_replace = function (search, replace) {
        return this.split(search).join(replace);
    }


    $("[data-load]").each(function(){
        $(this).load($(this).data("load"), function(e){});
    });

    // location
    $(document).on('click', '.s_gnb .btn', function(e) {
        $('.s_gnb').toggleClass('on');
    });

    $('.more_wrap').hide();
    $('.more').on('click', function(){
        $(this).toggleClass('on');
        if($(this).hasClass('on')){
            $('.more_wrap').show();
        }else{
            $('.more_wrap').hide();
        }
    });


    var intArea = setInterval(function() {
        if($.trim($(".location_con").html())) {
            clearInterval(intArea);

            $(document).find("a.p-area-close").addClass("p-pointer").css("z-index", 100);
            $(document).find("div.location_con div.container a.p-area-close").on("click", function(e) {
                $(document).find(".s_gnb .btn").click();
            });
        }
    },100);


    // Area Select
    $(document).on('click', '.location_con li a', function(e) {
        e.preventDefault();
        var href = $(this).attr("href");
        var qs = href.split('?')[1]; //AI=4672&rUrl=http://www.heykorean.com
        var ai= (qs.split('&')[0]).split('=')[1];
        console.log(ai);
        setArea(ai);
        setCookie("categoryidx", "");
    });

    setArea = function(ai) {

        setCookie("AreaIdx",ai,365); // old
        setCookie("areaidx",ai,365); // new

        //location.href = location.href;
        location.href = "/";
    };

    setCookie = function(n,t,i)	{
        var r=new Date;
        r.setTime(r.getTime()+432e5*i),document.cookie=n+"="+escape(t)+"; path=/; expires="+r.toGMTString()+";domain=.heykorean.com;"
    };
    getCookie = function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }

    // $("[data-utc-date]").each(function(){
    //     var utc_date_string = $(this).data("utc-date");
    //     console.log(utc_date_string);
    //     var localdate = new Date(utc_date_string);
    //     $(this).text(date("Y-m-d", localdate.getTime()/1000));
    // });
});

﻿/*
 * jQuery Bootstrap News Box v1.0.1
 * 
 * Copyright 2014, Dragan Mitrovic
 * email: gagi270683@gmail.com
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

//Utility
if (typeof Object.create !== 'function') {
    //Douglas Crockford inheritance function
    Object.create = function (obj) {
        function F() { };
        F.prototype = obj;
        return new F();
    };
}

(function ($, w, d, undefined) {

    var NewsBox = {

        init: function ( options, elem ) {
            //cache the references
            var self = this;
            self.elem = elem;
            self.$elem = $( elem );
            
            self.newsTagName = self.$elem.find(":first-child").prop('tagName');
            // console.log(self.newsTagName );
            self.newsClassName = self.$elem.find(":first-child").attr('class');
            // console.log(self.newsClassName );
            self.timer = null;
            self.resizeTimer = null; // used with window.resize event 
            self.animationStarted = false;
            self.isHovered = false;


            if ( typeof options === 'string' ) {
                //string was passed
                if(console) {
                    console.error("String property override is not supported");
                }
                throw ("String property override is not supported");
            } else {
                //object was passed
                //extend user options overrides
                self.options = $.extend( {}, $.fn.bootstrapNews.options, options );
                
                self.prepareLayout();


                //autostart animation
                if(self.options.autoplay) {
                    self.animate();
                }

                if ( self.options.navigation ) {
                    self.buildNavigation();
                }

                //enable users to override the methods
                if( typeof self.options.onToDo === 'function') {
                    self.options.onToDo.apply(self, arguments);
                }

            }
        },

        prepareLayout: function() {
            var self = this;

            //checking mouse position
                
            $(self.elem).find('.'+self.newsClassName).on('mouseenter', function(){
                self.onReset(true);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseout', function(){
                self.onReset(false);
            });

            //set news visible / hidden
            $.map(self.$elem.find(self.newsTagName), function(newsItem, index){
                if(index > self.options.newsPerPage - 1) {
                    $(newsItem).hide();
                } else {
                    $(newsItem).show();
                }
            });

            //prevent user to select more news that it actualy have

            if( self.$elem.find(self.newsTagName).length < self.options.newsPerPage ) {
                self.options.newsPerPage = self.$elem.find(self.newsTagName).length;
            }
            
            //get height of the very first self.options.newsPerPage news
            var height = 0;

            $.map(self.$elem.find(self.newsTagName), function( newsItem, index ) {
                if ( index < self.options.newsPerPage ) {
                    height = parseInt(height) + parseInt($(newsItem).height()) + 10;
                }
            });

            // console.log($(self.elem).parent().attr('class'));
            // $(self.elem).css({"overflow-y": "hidden", "height": height});

            if($(self.elem).parent().attr('class') != 'best-recommended') {
                $(self.elem).css({"overflow-y": "hidden", "height": 50});
            } else {
                $(self.elem).css({"overflow-y": "hidden", "height": 66});
            }

            //recalculate news box height for responsive interfaces
            $( w ).resize(function() {
                if ( self.resizeTimer !== null ) {
                    clearTimeout( self.resizeTimer );
                }
                self.resizeTimer = setTimeout( function() {
                  self.prepareLayout();
                }, 200 );
            });

        },

        findPanelObject: function() {
            var panel = this.$elem;

            while ( panel.parent() !== undefined ) {
                panel = panel.parent();
                if ( panel.parent().hasClass('sonha') ) {
                    return panel.parent();
                }
                if ( panel.hasClass('best-recommended') ) {
                    return panel.parent();
                }
            }

            return undefined;
        },

        buildNavigation: function() { 
            var panel = this.findPanelObject();
            // console.log(panel.attr('class'));
            if( panel && panel.attr('class') != 'best-today clearfix' ) {
                var nav = '<a class="new-pre btn-list btn-listpre" style="cursor: pointer;"><span class="hidden">이전</span></a>' +
                           '<a class="new-next btn-list btn-listnext" style="cursor: pointer;"><span class="hidden">다음</span></a>';

                $('.most-qa').append(nav);

                var self = this;
                $('.new-pre').on('click', function(ev){
                    ev.preventDefault();
                    self.onPrev();
                });

                $('.new-next').on('click', function(ev){
                    ev.preventDefault();
                    self.onNext();
                });
            } else {
                var nav = '<a class="recommended-pre btn-list btn-listpre" style="cursor: pointer;"><span class="hidden">이전</span></a>' +
                    '<a class="recommended-next btn-list btn-listnext" style="cursor: pointer;"><span class="hidden">다음</span></a>';

                $('.best-recommended').append(nav);

                var self = this;
                $('.recommended-pre').on('click', function(ev){
                    ev.preventDefault();
                    self.onPrev();
                });

                $('.recommended-next').on('click', function(ev){
                    ev.preventDefault();
                    self.onNext();
                });
            }
        },

        onStop: function() {
            
        },

        onPause: function() {
            var self = this;
            self.isHovered = true;
            if(this.options.autoplay && self.timer) {
                clearTimeout(self.timer);
            }
        },

        onReset: function(status) {
            var self = this;
            if(self.timer) {
                clearTimeout(self.timer);
            }

            if(self.options.autoplay) {
                self.isHovered = status;
                self.animate();
            }
        },

        animate: function() {
            var self = this;
            self.timer = setTimeout(function() {
                
                if ( !self.options.pauseOnHover ) {
                    self.isHovered = false;
                }

                if (! self.isHovered) {
                     if(self.options.direction === 'up') {
                        self.onNext();
                     } else {
                        self.onPrev();
                     }
                } 
            }, self.options.newsTickerInterval);
        },

        onPrev: function() {
            var self = this;

            if ( self.animationStarted ) {
                return false;
            }
            self.animationStarted = true;
            // console.log(self.newsTagName);//li
            // console.log(self.newsClassName);//news-item
            var html = '<' + self.newsTagName + ' style="display:none;" class="' + self.newsClassName + '">' + $(self.$elem).find(self.newsTagName).last().html() + '</' + self.newsTagName + '>';
            $(self.$elem).prepend(html);
            $(self.$elem).find(self.newsTagName).first().slideDown(self.options.animationSpeed, function(){
                $(self.$elem).find(self.newsTagName).last().remove();
            });

            $(self.$elem).find(self.newsTagName +':nth-child(' + parseInt(self.options.newsPerPage + 1) + ')').slideUp(self.options.animationSpeed, function(){
                self.animationStarted = false;
                self.onReset(self.isHovered);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseenter', function(){
                self.onReset(true);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseout', function(){
                self.onReset(false);
            });
        },

        onNext: function() {
            var self = this;

            if ( self.animationStarted ) {
                return false;
            }

            self.animationStarted = true;

            var html = '<' + self.newsTagName + ' style="display:none;" class=' + self.newsClassName + '>' + $(self.$elem).find(self.newsTagName).first().html() + '</' + self.newsTagName + '>';
            $(self.$elem).append(html);

            $(self.$elem).find(self.newsTagName).first().slideUp(self.options.animationSpeed, function(){
                $(this).remove();
            });

            $(self.$elem).find(self.newsTagName +':nth-child(' + parseInt(self.options.newsPerPage + 1) + ')').slideDown(self.options.animationSpeed, function(){
                self.animationStarted = false;
                self.onReset(self.isHovered);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseenter', function(){
                self.onReset(true);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseout', function(){
                self.onReset(false);
            });
        }
    };

    var KeywordsBox = {

        init: function ( options, elem ) {
            // console.log(elem);
            //cache the references
            var self = this;
            self.elem = elem;
            self.$elem = $( elem );

            self.newsTagName = self.$elem.find(":first-child").prop('tagName');
            self.newsClassName = self.$elem.find(":first-child").attr('class');
            self.timer = null;
            self.resizeTimer = null; // used with window.resize event
            self.animationStarted = false;
            self.isHovered = false;


            if ( typeof options === 'string' ) {
                //string was passed
                if(console) {
                    console.error("String property override is not supported");
                }
                throw ("String property override is not supported");
            } else {
                //object was passed
                //extend user options overrides
                self.options = $.extend( {}, $.fn.bootstrapKeywords.options, options );

                self.prepareLayout1();


                //autostart animation
                if(self.options.autoplay) {
                    self.animate1();
                }

                if ( self.options.navigation ) {
                    self.buildNavigation1();
                }

                //enable users to override the methods
                if( typeof self.options.onToDo === 'function') {
                    self.options.onToDo.apply(self, arguments);
                }

            }
        },

        prepareLayout1: function() {
            var self = this;

            //checking mouse position

            $(self.elem).find('.'+self.newsClassName).on('mouseenter', function(){
                self.onReset(true);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseout', function(){
                self.onReset(false);
            });

            //set news visible / hidden
            $.map(self.$elem.find(self.newsTagName), function(newsItem, index){
                if(index > self.options.newsPerPage - 1) {
                    $(newsItem).hide();
                } else {
                    $(newsItem).show();
                }
            });

            //prevent user to select more news that it actualy have

            if( self.$elem.find(self.newsTagName).length < self.options.newsPerPage ) {
                self.options.newsPerPage = self.$elem.find(self.newsTagName).length;
            }

            //get height of the very first self.options.newsPerPage news
            var height = 0;

            $.map(self.$elem.find(self.newsTagName), function( newsItem, index ) {
                if ( index < self.options.newsPerPage ) {
                    height = parseInt(height) + parseInt($(newsItem).height()) + 10;
                }
            });

            // console.log($(self.elem));
            // $(self.elem).css({"overflow-y": "hidden", "height": height});

            if($(self.elem).parent().attr('class') != 'today-sharing') {
                $(self.elem).css({"overflow-y": "hidden", "height": 23});
            } else {
                $(self.elem).css({"overflow-y": "hidden", "height": 63});
            }
        },

        findPanelObject1: function() {
            var panel = this.$elem;

            while ( panel.parent() !== undefined ) {
                panel = panel.parent();
                if ( panel.parent().hasClass('thuydinh') ) {
                    return panel.parent();
                }
                if ( panel.hasClass('today-sharing') ) {
                    return panel.parent();
                }
            }

            return undefined;
        },

        buildNavigation1: function() {
            var panel = this.findPanelObject1();
            // console.log(panel);

            if( panel && panel.attr('class') != 'best-today clearfix' ) {
                var nav = '<a class="arrow-down-icon" role="button"></a>' +
                    '<a class="arrow-up-icon" role="button"></a>';
                $('.control-block').append(nav);
                // alert('vao day');
                var self = this;
                $('.key-pre').on('click', function(ev){
                    ev.preventDefault();
                    self.onPrev1();
                });

                $('.key-next').on('click', function(ev){
                    ev.preventDefault();
                    self.onNext1();
                });

            } else {
                var nav = '<a class="sharing-pre btn-list btn-listpre" style="cursor: pointer;"><span class="hidden">이전</span></a>' +
                    '<a class="sharing-next btn-list btn-listnext" style="cursor: pointer;"><span class="hidden">다음</span></a>';
                $('.today-sharing').append(nav);

                var self = this;
                $('.sharing-pre').on('click', function(ev){
                    ev.preventDefault();
                    self.onPrev1();
                });

                $('.sharing-next').on('click', function(ev){
                    ev.preventDefault();
                    self.onNext1();
                });
            }
        },

        onStop1: function() {

        },

        onPause1: function() {
            var self = this;
            self.isHovered = true;
            if(this.options.autoplay && self.timer) {
                clearTimeout(self.timer);
            }
        },

        onReset1: function(status) {
            var self = this;
            if(self.timer) {
                clearTimeout(self.timer);
            }

            if(self.options.autoplay) {
                self.isHovered = status;
                self.animate1();
            }
        },

        animate1: function() {
            var self = this;
            self.timer = setTimeout(function() {

                if ( !self.options.pauseOnHover ) {
                    self.isHovered = false;
                }

                if (! self.isHovered) {
                    if(self.options.direction === 'up') {
                        self.onNext1();
                    } else {
                        self.onPrev1();
                    }
                }
            }, self.options.newsTickerInterval);
        },

        onPrev1: function() {
            // alert('vao day cmnr');
            var self = this;
            // console.log(self);
            // console.log(self.newsTagName);
            // console.log(self.newsClassName);

            if ( self.animationStarted ) {
                return false;
            }

            self.animationStarted = true;
            var html = '<' + self.newsTagName + ' style="display:none;" class="' + self.newsClassName + '">' + $(self.$elem).find(self.newsTagName).last().html() + '</' + self.newsTagName + '>';
            // console.log(html);
            $(self.$elem).prepend(html);
            $(self.$elem).find(self.newsTagName).first().slideDown(self.options.animationSpeed, function(){
                $(self.$elem).find(self.newsTagName).last().remove();
            });

            $(self.$elem).find(self.newsTagName +':nth-child(' + parseInt(self.options.newsPerPage + 1) + ')').slideUp(self.options.animationSpeed, function(){
                self.animationStarted = false;
                self.onReset1(self.isHovered);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseenter', function(){
                self.onReset1(true);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseout', function(){
                self.onReset1(false);
            });
        },

        onNext1: function() {
            var self = this;

            if ( self.animationStarted ) {
                return false;
            }

            self.animationStarted = true;

            var html = '<' + self.newsTagName + ' style="display:none;" class=' + self.newsClassName + '>' + $(self.$elem).find(self.newsTagName).first().html() + '</' + self.newsTagName + '>';
            $(self.$elem).append(html);

            $(self.$elem).find(self.newsTagName).first().slideUp(self.options.animationSpeed, function(){
                $(this).remove();
            });

            $(self.$elem).find(self.newsTagName +':nth-child(' + parseInt(self.options.newsPerPage + 1) + ')').slideDown(self.options.animationSpeed, function(){
                self.animationStarted = false;
                self.onReset1(self.isHovered);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseenter', function(){
                self.onReset1(true);
            });

            $(self.elem).find('.'+self.newsClassName).on('mouseout', function(){
                self.onReset1(false);
            });
        }
    };

    $.fn.bootstrapNews = function ( options ) {
        //enable multiple DOM object selection (class selector) + enable chaining like $(".class").bootstrapNews().chainingMethod()
        return this.each( function () {

            var newsBox = Object.create( NewsBox );

            newsBox.init( options, this );
            //console.log(newsBox);

        });
    };

    $.fn.bootstrapKeywords = function ( options ) {
        //enable multiple DOM object selection (class selector) + enable chaining like $(".class").bootstrapNews().chainingMethod()
        return this.each( function () {

            var keywordsBox = Object.create( KeywordsBox );
            // console.log(keywordsBox);

            keywordsBox.init( options, this );
            //console.log(newsBox);

        });
    };

    $.fn.bootstrapNews.options = {
        newsPerPage: 4,
        navigation: true,
        autoplay: true,
        direction:'up',
        animationSpeed: 'normal',
        newsTickerInterval: 4000, //4 secs
        pauseOnHover: true,
        onStop: null,
        onPause: null,
        onReset: null,
        onPrev: null,
        onNext: null,
        onToDo: null
    };

    $.fn.bootstrapKeywords.options = {
        newsPerPage: 4,
        navigation: true,
        autoplay: true,
        direction:'up',
        animationSpeed: 'normal',
        newsTickerInterval: 4000, //4 secs
        pauseOnHover: true,
        onStop: null,
        onPause: null,
        onReset: null,
        onPrev: null,
        onNext: null,
        onToDo: null
    };

})(jQuery, window, document);
jQuery(document).ready(function(){
	jQuery(document).on('click', '.more', function(e) {
		e.preventDefault();
		jQuery(this).toggleClass('on');
		if(jQuery(this).hasClass('on')) {
			jQuery(document).find(".more_wrap").show();
		}  	
		else {
			jQuery(document).find(".more_wrap").hide();
		}
	}); 

	jQuery('.search-box input').focusin(function(){
		jQuery('.search-box').animate({width:'500px'}, 300);
		jQuery('.search-box input').animate({width:'370px'}, 300);
	});
	jQuery('.search-box input').focusout(function(){
		jQuery('.search-box').animate({width:'300px'}, 100);
		jQuery('.search-box input').animate({width:'205px'}, 100);
	});

	jQuery('#menu > div .menu-sub').hide();
	jQuery('#menu > div').hover(function(){
		jQuery(this).find('.menu-sub').stop().slideDown();
	}, function(){
		//jQuery('#menu .menu-sub').slideUp('fast');
		jQuery('#menu .menu-sub').hide();
	});

	jQuery('.category-list a').hover(function(){
		jQuery(this).toggleClass('on');
	});

	jQuery('.apply-position li:nth-child(1) span').css('background', 'url("./images/img_apply1.gif") 0 6px no-repeat');
	jQuery('.apply-position li:nth-child(2) span').css('background', 'url("./images/img_apply2.gif") 0 3px no-repeat');
	jQuery('.apply-position li:nth-child(3) span').css('background', 'url("./images/img_apply3.gif") 0 4px no-repeat');
	jQuery('.apply-position li:nth-child(4) span').css('background', 'url("./images/img_apply4.gif") 0 3px no-repeat');

	// jQuery('.tab-con > div').not(jQuery('.tab-con > div:first-child')).hide();
	jQuery('.tab-style01  a').hover(function(){
		var tabCon = jQuery(this).attr('href');
	// 	jQuery(this).parent('.tab-title').find('a').not(jQuery(this)).removeClass('on');
	// 	jQuery(this).addClass('on');
		jQuery(this).parent('.tab-title').next('.tab-con').children('div').not(jQuery(tabCon)).hide();
		jQuery(tabCon).show();
	// 	return false;
	})
	jQuery('#tab07').hide();
	jQuery('.tab-style02 .tab-title a').on('click', function(){
		var tabCon = jQuery(this).attr('href');
		jQuery(this).parent('.tab-title').find('a').not(jQuery(this)).removeClass('on');
		jQuery(this).addClass('on');
		jQuery(this).parent('.tab-title').next('.tab-con').children('div').not(jQuery(tabCon)).hide();
		jQuery(tabCon).show();
		return false;
	});

	jQuery('.mentoring-board .tab-con tr td:nth-child(2)').css('text-align', 'left');

	/* button comment */
	jQuery('.comment-box').not(jQuery('.sharing-box .comment-box')).hide();
	jQuery('.btn-comment').on('click', function(){
		$(this).next('.comment-box').toggle();
	});

	/* layerpopup */
	jQuery('.layerpopup').hide();
	jQuery('.item-rate .magnify-image, .debatestyle .thumb').on('click', function(){
		var thumbHref = jQuery(this).closest(jQuery('.item-con')).find(jQuery('.thumb img')).attr('src');
		jQuery('.layerpopup-wrap01').find(jQuery('.img-debate')).attr('src', thumbHref);
		jQuery('.layerpopup').fadeIn();
		return false;
	});
	jQuery('.layerpopup .close').on('click', function(){
		jQuery(this).parents('.layerpopup').fadeOut();
	});

	
	jQuery('.tooltip-point').mouseenter(function(){
		jQuery(this).find(jQuery('.tooltip-wrap')).show();
	});
	jQuery('.tooltip-point').mouseleave(function(){
		jQuery(this).find(jQuery('.tooltip-wrap')).hide();
	});
});
jQuery(document).ready(function(){
	
	/* 
	 jQuery(1.9.x or later) required 
	 modernizr(2.8.3 or later) required 
	*/
	
	var BrowserDetect = {
      init: function () {
          this.browser = this.searchString(this.dataBrowser) || "Other";
          this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
      },
      searchString: function (data) {
          for (var i = 0; i < data.length; i++) {
              var dataString = data[i].string;
              this.versionSearchString = data[i].subString;

              if (dataString.indexOf(data[i].subString) !== -1) {
                  return data[i].identity;
              }
          }
      },
      searchVersion: function (dataString) {
          var index = dataString.indexOf(this.versionSearchString);
          if (index === -1) {
              return;
          }

          var rv = dataString.indexOf("rv:");
          if (this.versionSearchString === "Trident" && rv !== -1) {
              return parseFloat(dataString.substring(rv + 3));
          } else {
              return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
          }
      },

      dataBrowser: [
          {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
          {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
          {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
          {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
          {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
          {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  

          {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
          {string: navigator.userAgent, subString: "Safari", identity: "Safari"}       
      ]
  };
  BrowserDetect.init();
  //alert("You are using <b>" + BrowserDetect.browser + "</b> with version <b>" + BrowserDetect.version + "</b>");
  //document.write("You are using <b>" + BrowserDetect.browser + "</b> with version <b>" + BrowserDetect.version + "</b>");
	
  jQuery("[data-ad-code]").each(function(i, v){
  	
  	//console.log(jQuery(this).data("ad-code"));
    var code = jQuery(this).data("ad-code");
    if(code == null) {
    	code = jQuery(this).attr("data-ad-code");
    }

		var d = new Date();
		var utc = new Date(d.getTime() + d.getTimezoneOffset() * 60000)
		var ut= Math.floor(utc.getTime()/1000);
		
		try {

	    jQuery.ajax({	  
		    type: "get",    
		    url: "http://ap.heykorean.com/v3/Advertise/get/"+code.toLowerCase()+"/"+ut,
		    dataType: "jsonp",
		    success: function (json) {    	
		    	if(json == null);
		    	else {	    		
		    		if(jQuery.isArray(json.List)) {
		    			
		    			
			    		// TODO: 가중치 처리 필요
			    		var sum_priority = 0;
			    		jQuery.each(json.List, function(i, v) {
			    			sum_priority += v.priority;
			    		});
			    		//alert(json.List.length + "/" + sum_priority);
			    		
			    		// TODO: 일정 시간 이후 광고 리프레시 기능 구현 필요
			    		
			    		var len = json.List.length ? json.List.length : 0;
			    		if(len) {
			    			var seed = Math.floor(Math.random() * len);// + 1;
			    			var ad = json.List[seed];
								
								// ad.type: IMAGE, SCRIPT, VIDEO, DOUBLECLICK, FLASH
								// alert(ad.type);
								switch(ad.type) {
									case "SCRIPT":
										break;
									case "VIDEO":
										break;
									case "DOUBLECLICK":
									{
										var script = ad.source.src;
										var noscript = ad.source.noscript;
									
										var ord = Math.random();
									  ord = ord*10000000000000000000;  									
									  
									  //alert(jQuery.browser.msie);

									  jQuery.ajax({
							        url: "/proxy?url="+ encodeURIComponent(script.str_replace("[#randnum#]", ord)),
							        success: function(data) {
							        	
  											if( BrowserDetect.browser == "Explorer" && BrowserDetect.version <=8) {
  												noscript = noscript.str_replace("[#randnum#]",ord);
							        		noscript = jQuery(noscript).attr("target", "_blank");
							        		//alert(noscript.html());
  												jQuery(v).empty()
  																 .append(noscript).on("click", function(e) {
								    						 		 //e.preventDefault();
								    						 		 jQuery.ajax({type:"get",url:ad.click,dataType:"jsonp"});							    						 		
									    						 });
					    						jQuery.ajax({type:"get",url:ad.impression,dataType:"jsonp"});
  											}
  											else {
								        	jQuery(v).addClass("p-"+ord);
								        	data = data.str_replace("document.write", "jQuery('.p-"+ord+"').append");
							            //console.log(unescape(data));
	 						            var script  = document.createElement("script");
													script.type = "text/javascript";	
													script.text = unescape(data);
							            jQuery(v).empty()
							            				 .html(script)
							            				 .on("click", function(e) {
								    						 		 //e.preventDefault();
								    						 		 jQuery.ajax({type:"get",url:ad.click,dataType:"jsonp"});							    						 		
									    						 });
					    						jQuery.ajax({type:"get",url:ad.impression,dataType:"jsonp"});
				    						}
				    						
							        }
								    });
										break;
									}
									case "FLASH":
										break;
									case "IMAGE":
									default:
									{
										var imgsrc = ad.source.src;
						    		var img = jQuery("<img/>").attr("src", imgsrc);
						    		//var anchor = jQuery("<a/>").attr("target", "_blank").append(img).addClass("p-pointer");	 
						    		var anchor = jQuery("<a/>").attr("target", "_blank").attr("href", ad.link).append(img);	 			    		
						    		//console.log(json.link);
						    		jQuery(v).empty()
						    						 .append(anchor)
						    						 //.data("ad-code", ad.code)
						    						 //.data("ad-link", ad.link)
						    						 //.data("ad-c", ad.click)
						    						 //.data("ad-i", ad.impression)
						    						 //.click(function (e) {
						    						 .on("click", function(e) {
						    						 		e.preventDefault();
						    						 		//window.open(jQuery(v).data("ad-link"), "_blank", "");		    						 		
						    						 		window.open(ad.link, "_blank", "");
						    						 		jQuery.ajax({type:"get",url:ad.click,dataType:"jsonp"});		    						 		
						    						 });		    						 
						    		jQuery.ajax({type:"get",url:ad.impression,dataType:"jsonp"});
										
										break;
									}
								}
			    		}
		    			
		    			
		    			
		    		}		    		
		    	}
				},
				error: function () {   
				}
					
			});
			
		}
		catch(err) {
		}
			
    
  });
  
});
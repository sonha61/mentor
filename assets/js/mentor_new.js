jQuery(document).ready(function(){

    jQuery(".p-facebook-share").on("click", function(e) {
  		e.preventDefault();
  		var title = jQuery('meta[property="og:title"]').attr('content');
  		var url = jQuery('meta[property="og:url"]').attr('content');
  		snsShowPage("facebook", title, url, "");
  	});

  	jQuery(".p-twitter-tweet").on("click", function(e) {  	
  		e.preventDefault();
  		var title = jQuery('meta[property="og:title"]').attr('content');
  		var url = jQuery('meta[property="og:url"]').attr('content');
  		snsShowPage("twitter", title, url, "");
  	});
	function snsShowPage(type, title, surl, thumnail) {
    //this.getShortUrl(longurl);
    surl = encodeURIComponent(surl);
    title = encodeURIComponent(title);
    switch (type) {
      case 'twitter':
          var link = 'http://twitter.com/home?status=' + title + ':' + surl;
          //popwin = window.open(link,'popwin');
          popwin = window.open(link, 'popwin', 'menubar=yes,toolbar=yes,status=yes,resizable=yes,location=yes,scrollbars=yes');
          if (popwin)
              popwin.focus();
          break;
      case 'facebook':
          var link = 'http://www.facebook.com/sharer.php?u=' + surl + '&t=' + encodeURIComponent(title);
          popwin = window.open(link, 'recom_icon_pop', 'width=600,height=500,scrollbars=no,resizable=no');
          if (popwin)
              popwin.focus();
          break;
    }
	}
  
});
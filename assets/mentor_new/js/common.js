jQuery(document).ready(function(){
	/* 더보기 */
	jQuery('.more_wrap').hide();
	jQuery('.more').on('click', function(){
		jQuery(this).toggleClass('on');
		if(jQuery(this).hasClass('on')){
			jQuery('.more_wrap').show();
		}else{
			jQuery('.more_wrap').hide();
		}
	});
	
	var uploadMain = jQuery('.file_preview li button');
	jQuery('.file_preview li:first-child').addClass('on');
	uploadMain.on('click', function(){
		uploadMain.closest('li').removeClass('on');
		jQuery(this).closest('li').addClass('on');
	});
	jQuery(document).on('click','.s_gnb > .btn', function(){
		jQuery(document).find('.location_con').toggle();
	});
	
	var intval4s_gnb = setInterval(function() {
		if(jQuery.trim(jQuery('.location_con').html())) {
			clearInterval(intval4s_gnb);		
			var widePop = jQuery(document).find('.wide .ct_wrap');
			if(jQuery('.location_con').css('display', 'block')){
				jQuery(".location_con").animate({
					height:"hide"
				},0);
				for(var i=0; i < widePop.length; i+=5){
					widePop.slice(i, i+5).wrapAll("<div class='locate_list'></div>");
				}
			}else{
				jQuery(".location_con").animate({
					height:"show"
				},200);	
				widePop.find('div').unwrap();
			}			 
	
		}
		
	},500);
});
function textCounter(field,field2,maxlimit)
      {
       	var countfield = document.getElementById(field2);
       	if ( field.value.length > maxlimit ) {
        	field.value = field.value.substring( 0, maxlimit );
        return false;
       	} else {
        	countfield.value = field.value.length + field.value.length + "/100";
       	}
      }
$(function () {
	var update_handle_track_pos;
	update_handle_track_pos = function (slider, ui_handle_pos) {
  var handle_track_xoffset, slider_range_inverse_width;
  handle_track_xoffset = -(ui_handle_pos / 1000 * slider.clientWidth);

  $(slider).find('.handle-track').css('left', handle_track_xoffset);
    slider_range_inverse_width = (1000 - ui_handle_pos)/10 + '%';
    return $(slider).find('.slider-range-inverse').css('width', slider_range_inverse_width);
	};

	var waterdrop_slider = $('#waterdrop-slider').slider({
    range: 'min',
    max: 1000,
    value: 1000,
    create: function (event, ui) {
      var slider;
      slider = $(event.target);
      slider.find('.ui-slider-handle').append('<span class="dot"><span class="handle-track"></span></span>');
      slider.prepend('<div class="slider-range-inverse"></div>');
      slider.find('.handle-track').css('width', event.target.clientWidth);
      return update_handle_track_pos(event.target, $(this).slider('value'));
    },
    slide: function (event, ui) {
    	$('#amount').val(ui.value);
      return update_handle_track_pos(event.target, ui.value);
    }
	});

	// Update input value when drag slider
	$('#amount').val( $('#waterdrop-slider').slider('value'));

	// Update color and position of slider when change input value
	$('#amount').on( 'input propertychange', function() {
    $('#waterdrop-slider').slider( 'value', this.value);
    update_handle_track_pos(waterdrop_slider, this.value);
    var handle_track_xoffset = -(this.value / 1000 * waterdrop_slider.width());
  	$(waterdrop_slider).find('.handle-track').css('left', handle_track_xoffset);
  });
}.call(this));

$('input').iCheck({
  checkboxClass: 'icheckbox_square-blue',
  radioClass: 'iradio_square-blue'
});

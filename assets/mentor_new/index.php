 <?php 
  $kaim_path = "."; // common.php 의 상대 경로
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Mentor 2</title>
    <?php include_once($kaim_path."/include/head.html"); ?>
  </head>
  <body>
    <header>
    	<?php include_once($kaim_path."/include/header.html"); ?>
    </header>
    <!-- Best Mentoring -->
		<section>
			<div class="container">
				<div class="best-mentoring-wrap clearfix">
					<h3 class="best-mentoring-header">Best Mentoring <a class="more" href="#">more</a></h3>
					<div class="list-item clearfix">
						<div class="item law-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
							<div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
								<p>
									이거 제가 사기당하는 것인가요
									제가 포트리에서 교통사고를당했습니다.
									법원에서 이거제가 사기당하는것인가요
									포트리 교통사오 법원 
									질문이 있습니다.? 변호사 선임</p>
								<div class="user-info">
									<a href="#">
										<img src="./images/user-profile-img.png" alt="...">
										user name
									</a>
									<span class="date">Jul 14, 2016</span>
								</div>
							</div>
						</div>
						<div class="item banking-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
							<div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
								<p>
									이거 제가 사기당하는 것인가요
									제가 포트리에서 교통사고를당했습니다.
									법원에서 이거제가 사기당하는것인가요
									포트리 교통사오 법원 
									질문이 있습니다.? 변호사 선임</p>
								<div class="user-info">
									<a href="#">
										<img src="./images/user-profile-img.png" alt="...">
										user name
									</a>
									<span class="date">Jul 14, 2016</span>
								</div>
							</div>
						</div>
						<div class="item school-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
							<div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
								<p>
									이거 제가 사기당하는 것인가요
									제가 포트리에서 교통사고를당했습니다.
									법원에서 이거제가 사기당하는것인가요
									포트리 교통사오 법원 
									질문이 있습니다.? 변호사 선임</p>
								<div class="user-info">
									<a href="#">
										<img src="./images/user-profile-img.png" alt="...">
										user name
									</a>
									<span class="date">Jul 14, 2016</span>
								</div>
							</div>
						</div>
						<div class="item travelling-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
							<div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
								<p>
									이거 제가 사기당하는 것인가요
									제가 포트리에서 교통사고를당했습니다.
									법원에서 이거제가 사기당하는것인가요
									포트리 교통사오 법원 
									질문이 있습니다.? 변호사 선임</p>
								<div class="user-info">
									<a href="#">
										<img src="./images/user-profile-img.png" alt="...">
										user name
									</a>
									<span class="date">Jul 14, 2016</span>
								</div>
							</div>
						</div>
						<div class="item visa-category">
							<span class="question-title">
								Q. 이거 제가 사기당하는 것인가요?
							</span>
							<div class="answer-box">
								<span class="question-title">
									Q. 이거 제가 사기당하는 것인가요?
								</span>
								<p>
									이거 제가 사기당하는 것인가요
									제가 포트리에서 교통사고를당했습니다.
									법원에서 이거제가 사기당하는것인가요
									포트리 교통사오 법원 
									질문이 있습니다.? 변호사 선임</p>
								<div class="user-info">
									<a href="#">
										<img src="./images/user-profile-img.png" alt="...">
										user name
									</a>
									<span class="date">Jul 14, 2016</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Best Mentoring -->

		<!-- Category list -->
		<section>
			<div class="container">
				<div class="category-list-wrap clearfix">
					<header>
						<h3>
							Category
							<a class="arrow-up-icon" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
							</a>
						</h3>
					</header>
					<div class="collapse in" id="collapseExample">
						<ul class="category-list">
							<li><a href="">All</a></li>
							<li><a href="">School</a></li>
							<li><a href="">Shopping</a></li>
							<li><a href="">Entertainment</a></li>
							<li><a href="">Travelling</a></li>
							<li><a href="">Low Society & Culture</a></li>
							<li><a href="">Immigration</a></li>
							<li><a href="">Study Abroad</a></li>
							<li><a href="">Tips & Produce</a></li>
							<li><a href="">Perfomance</a></li>
							<li><a href="">Sightseeing</a></li>
							<li><a href="">Money & Banking</a></li>
							<li><a href="">Visa</a></li>
							<li><a href="">Motherhood</a></li>
							<li><a href="">Beauty & Style</a></li>
							<li><a href="">Sports</a></li>
							<li><a href="">Food & Restaurant</a></li>
							<li><a href="">HeyKorean</a></li>
							<li><a href="">Passport</a></li>
							<li><a href="">Schooling</a></li>
							<li><a href="">Life style info</a></li>
							<li><a href="">Health & Medicine</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!-- End Category list -->

		<!-- Main content -->
		<section>
			<div class="container">
				<!-- Left content -->
				<div class="main-content home" id="main">
					<!-- Header: Toolbar -->
					<h3>Category title</h3>
					<div class="toolbar-wrap clearfix">
						<div class="style-group">
							<button>Style 1</button>
							<button>Style 2</button>
						</div>
						<div class="search-wrap">
							<div class="select-btn btn-group">
							  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							    option <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu">
							    <li><a href="#">Option 1</a></li>
							    <li><a href="#">Option 2</a></li>
							    <li><a href="#">Option 3</a></li>
							    <li><a href="#">Option 4</a></li>
							  </ul>
							</div>
							<div class="input-group">
				      	<input type="text" class="form-control" placeholder="Search for...">
					      <span class="input-group-btn">
					        <button class="btn btn-search" type="button"></button>
					      </span>
					    </div>
							<a href="#" class="help-icon"></a>
						</div>
					</div>
					<!-- End Header: Toolbar -->
					<!-- List item -->
					<div class="list-content-wrap clearfix">
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-gray.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-gray.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-gray.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-gray.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-blue.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-gray.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-gray.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-orange.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-blue.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
						<div class="item clearfix">
							<div class="waterdrop">
								<img src="./images/water-orange.png" alt="water-drop">
								<span class="point">30</span>
							</div>
							<div class="item-context">
								<div class="header">
									<a href="#" class="category">Visa</a>
									<a href="#" class="title">해외에서 유학하신 선배님들께 물어봅니다 !</a>
									<span class="comment-counter">3</span>
									<span class="location">US</span>
								</div>
								<p>
									저는 현대 UIUC 산업공학에 다니는 94년생 유학생입니다. 아직 군복무를하지않은상태인데, 군입을 언제할지 고민이 많습
									니다.졸업 후 군대를 가는 것이 바람직한가요? 아니면 1학기라도 남겨놓고서 입대를 하는 것이 바람직한가요?  
									졸업이후현대 군복무 제대어떻게 하락요 궁금합니다  현대 대학교 다니는 94년생 유학생 언제할지 .      
								</p>
							</div>
						</div>
					</div>
					<button class="btn btn-more">더 보기 <img src="./images/arrow-down.png"></button>
					<!-- End List item -->
				</div>
				<!-- End Left content -->
				<!-- Right content -->
				<aside>
					<div class="button-wrap clearfix">
						<button class="btn btn-orange btn-ask"><img src="./images/question-icon-w.png" alt="..."> ask</button>
						<button class="btn btn-blue btn-share"><img src="./images/comment-icon-w.png" alt="..."> share</button>
					</div>
					<!-- Keyword block -->
					<div class="keyword-wrap aside-block clearfix">
						<div class="header">
							<h3>best keyword</h3>
							<div class="control-block">
								<a class="arrow-down-icon" role="button"></a>
								<a class="arrow-up-icon" role="button"></a>
							</div>						
						</div>
						<div class="keyword-list clearfix">
							<ol>
								<li>
									<a href="">
										<span class="pos">1</span>
										마사지
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">2</span>
										플러싱
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">3</span>
										보톡스
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">4</span>
										뉴욕
										<span class="quantity">110</span>
									</a>
								</li>
								<li>
									<a href="">
										<span class="pos">5</span>
										뉴저지 영주권
										<span class="quantity">110</span>
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- End Keyword block -->

					<!-- Ads block -->
					<div class="ads-img clearfix">
						<img src="./images/banner.png" alt="ads">
					</div>
					<!-- End Ads block -->

					<!-- Leaderboard block -->
					<div class="leader-board-wrap aside-block clearfix">
						<div>
						  <!-- Nav tabs -->
						  <ul class="leader-board-header" role="tablist">
						    <li role="presentation" class="active"><a href="#best-mentors" aria-controls="home" role="tab" data-toggle="tab"><img src="./images/crown-icon-b.png"> Best Mentors</a></li>
						    <li role="presentation"><a href="#best-leaders" aria-controls="profile" role="tab" data-toggle="tab">Best Leaders</a></li>
						  </ul>
						  <!-- Tab panes -->
						  <div class="tab-content">
						    <div role="tabpanel" class="tab-pane active leader-board-content" id="best-mentors">
						    	<ol>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">1</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">2</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">3</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">4</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">5</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    	</ol>
						    	<a class="more" href="#">more</a>
						    </div>
						    <div role="tabpanel" class="tab-pane leader-board-content" id="best-leaders">
						    	<ol>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">1</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">2</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">3</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">4</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    		<li>
						    			<a href="#" class="clearfix">
						    				<span class="number">5</span>
						    				<span class="user-img"><img src="./images/user-profile-img.png" alt=""></span>
						    				<span class="user-info">
							    				<span class="user-name">Username</span>
							    				<span class="rank">
							    					<img src="./images/silver-icon.png">
							    					silver
							    				</span>
							    				<span class="point">
							    					<img src="./images/water-gray.png">
							    					12,000
							    				</span>
						    				</span>
						    			</a>
						    		</li>
						    	</ol>
						    	<a class="more" href="#">more</a>
						    </div>
						  </div>
						</div>
					</div>
					<!-- End Leaderboard block -->
					<button class="btn btn-gray btn-help"><img src="./images/question-icon-t.png"> HELP DESK</button>
					<button class="btn btn-dark-blue btn-connect"><img src="./images/hey-icon.png"> CONNECT US</button>
					<div class="sponsor-wrap">
						<h3>
							Sponsor
						</h3>
						<img src="./images/sponsor.png" alt="sponsor">
					</div>
				</aside>
			</div>
		</section>
		<footer>
			<?php include_once($kaim_path."/include/footer.html"); ?>
		</footer>
		<!-- End Main content -->
		<?php include_once($kaim_path."/include/js-include.html"); ?>
  </body>
</html>